package com.medical.medicalfitness;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.medical.medicalfitness.objects.AudioQuestionnaire;
import com.medical.medicalfitness.objects.MedicalFitnessCertificate;
import com.medical.medicalfitness.objects.MedicalHistory;
import com.medical.medicalfitness.objects.MedicalReferral;
import com.medical.medicalfitness.objects.OccupationalHealthRecord;
import com.medical.medicalfitness.objects.OtoscopicExamination;
import com.medical.medicalfitness.objects.PhysicalExamination;
import com.medical.medicalfitness.objects.RespiratoryQuestionnaire;
import com.medical.medicalfitness.objects.SpecialMedicalInvestigation;
import com.medical.medicalfitness.params.Constants;
import com.parse.FindCallback;
import com.parse.LogOutCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private String valueToSend;
    private AppCompatButton id_sendFull,id_go_prev,id_go_next;
    private ViewFlipper flipper;

    private EditText id_9_referred_to,id_9_employement_date,id_9_employement_no,id_9_gender,
            id_9_patientName,id_9_dateReferred,id_9_occupation,id_9_id_number, id_9_Company, id_9_Comments,id_9_occdate,
            id_9_occpractiseNumber, id_9_seenby, id_9_Patient, id_9_medComments, id_9_meddate,id_9_medpractiseNumber;
    private Button id_9_medical_practioner_button, id_9_occupational_health_practioner_button, id_9_save;
    private ImageView id_9_occupational_health_practioner_image,id_9_medical_practioner_image;

    private ParseFile examsign_7_1, examsign_7_2,examsign_7_3,examsign_7_4,medfileDate9,occfileDate9,
            examiner8,employee8,employee6,technician6,employee1,employee2,examiner1,omp1;

    private InputStream bexamsign_7_1, bexamsign_7_2,bexamsign_7_3,bexamsign_7_4,bmedfileDate9,boccfileDate9,
            bexaminer8,bemployee8,bemployee6,btechnician6,bemployee2,bexaminer1,bomp1;
    private InputStream bemployee1;

    private AppCompatEditText id_8_vision_comments,id_8_audio_plh,id_8_audio_shift,id_8_lung,id_8_fvc,id_8_fev,id_8_fevoverfvc,id_8_peakflow,id_8_certified,id_8_datesign;
    private RadioGroup id_8_vision,id_8_farvision,id_8_nearvision,id_8_nightvision,id_8_othervision,id_8_chestray,id_8_rest_ecg,id_8_cholestrol,id_8_mdt,id_8_other;
    private Button id_8_save,id_8_examiner_button,id_8_employee_button;
    private ImageView id_8_employee_image,id_8_examiner_image;



    private AppCompatEditText id_7_audio_date1,id_7_audio_cold_comments1,id_7_audio_noise_comments1,id_7_audio_hobbies_comments1,id_7_audio_hearing_comments1,id_7_audio_trauma_comments1,id_7_audio_tb_comments1,id_7_audio_ears_comments1,id_7_audio_military_comments1,id_7_audio_ppe_comments1,id_7_audio_meds_comments1,
            id_7_audio_date2,id_7_audio_cold_comments2,id_7_audio_noise_comments2,id_7_audio_hobbies_comments2,id_7_audio_hearing_comments2,id_7_audio_trauma_comments2,id_7_audio_tb_comments2,id_7_audio_ears_comments2,id_7_audio_military_comments2,id_7_audio_ppe_comments2,id_7_audio_meds_comments2,
            id_7_audio_date3,id_7_audio_cold_comments3,id_7_audio_noise_comments3,id_7_audio_hobbies_comments3,id_7_audio_hearing_comments3,id_7_audio_trauma_comments3,id_7_audio_tb_comments3,id_7_audio_ears_comments3,id_7_audio_military_comments3,id_7_audio_ppe_comments3,id_7_audio_meds_comments3;
    private RadioGroup id_7_audio_cold1,id_7_audio_noise1,id_7_audio_hobbies1,id_7_audio_hearing1,id_7_audio_trauma1,id_7_audio_tb1,id_7_audio_ears1,id_7_audio_military1,id_7_audio_ppe1,id_7_audio_meds1,
            id_7_audio_cold2,id_7_audio_noise2,id_7_audio_hobbies2,id_7_audio_hearing2,id_7_audio_trauma2,id_7_audio_tb2,id_7_audio_ears2,id_7_audio_military2,id_7_audio_ppe2,id_7_audio_meds2,
            id_7_audio_cold3,id_7_audio_noise3,id_7_audio_hobbies3,id_7_audio_hearing3,id_7_audio_trauma3,id_7_audio_tb3,id_7_audio_ears3,id_7_audio_military3,id_7_audio_ppe3,id_7_audio_meds3;
    private RadioGroup id_7_oto_perforation1,id_7_oto_malfunction1,id_7_oto_otitis1,id_7_oto_wax1,id_7_oto_treatment1,
            id_7_oto_perforation2,id_7_oto_malfunction2,id_7_oto_otitis2,id_7_oto_wax2,id_7_oto_treatment2,
            id_7_oto_perforation3,id_7_oto_malfunction3,id_7_oto_otitis3,id_7_oto_wax3,id_7_oto_treatment3,
            id_7_oto_perforation4,id_7_oto_malfunction4,id_7_oto_otitis4,id_7_oto_wax4,id_7_oto_treatment4;
    private AppCompatEditText id_7_oto_date1,id_7_oto_date2,id_7_oto_date3,id_7_oto_date4;
    private AppCompatButton id_7_oto_sign1,id_7_oto_sign2,id_7_oto_sign3,id_7_oto_sign4,id_7_save;
    private AppCompatImageView id_7_oto_sign_image1,id_7_oto_sign_image2,id_7_oto_sign_image3,id_7_oto_sign_image4;




    private AppCompatEditText id_6_name,id_6_dob,id_6_jobTitle,id_6_weight,id_6_height,id_6_department,id_6_year_exposed,id_6_company_name,id_6_job_title,id_6_hazards_exposed_to;
    private AppCompatButton id_6_employeesign_button,id_6_techniciansign_button,id_6_save;
    private AppCompatImageView id_6_employeesign_image,id_6_techniciansign_image;

    private RadioGroup id_6_chest_problem,id_6_lungcondition,id_6_tb,id_6_cold,id_6_smoking,id_6_ex_smoker,id_6_xray,id_6_ppe,id_6_on_treatment;
    private AppCompatEditText id_6_chest_problem_comment,id_6_lungcondition_comment,id_6_tb_comment,id_6_cold_comment,id_6_smoking_comment,id_6_ex_smoker_comment,id_6_xray_comment,id_6_ppe_comment,id_6_on_treatment_comment;






    private AppCompatButton id_5_save;
    private AppCompatEditText id_5_bp,id_5_pulse,id_5_weight,id_5_bmi,id_5_hgt,id_5_height,id_5_urine_analysis,id_5_ph,id_5_sg,id_5_bld,id_5_gluc,id_5_leuk;
    private RadioGroup id_5_head,id_5_ear,id_5_lungs,id_5_heart,id_5_vascular,id_5_abdomen,id_5_genito,id_5_neurological,id_5_limbs,id_5_spine,id_5_skin,id_5_psychological;
    private AppCompatEditText id_5_head_comment,id_5_ear_comment,id_5_lungs_comment,id_5_heart_comment,id_5_vascular_comment,id_5_abdomen_comment,id_5_genito_comment,id_5_neurological_comment,id_5_limbs_comment,id_5_spine_comment,id_5_skin_comment,id_5_psychological_comment;




    private AppCompatEditText id_3_you_drugs_type,id_3_you_drugs_quantity;
    private AppCompatButton id_3_save;
    private RadioGroup id_3_you_heart_disease,id_3_you_blood_pressure,id_3_you_diabetes,id_3_you_epilepsy,id_3_you_cancer,id_3_you_thyroid,
            id_3_fam_heart_disease,id_3_fam_blood_pressure,id_3_fam_diabetes,id_3_fam_epilepsy,id_3_fam_cancer,id_3_fam_thyroid,id_3_fam_hereditary,id_3_fam_blindness,
            id_3_you_smoking,id_3_you_ex_smoker,id_3_you_skin_disorder,id_3_you_admitted_to_hospital,id_3_you_muscles_disease,id_3_you_other_injury,
            id_3_you_pregnant,id_3_you_gynae,id_3_you_headaches,id_3_you_dizziness,id_3_you_hearing,id_3_you_vision,id_3_you_head_injury,id_3_you_neurological,
            id_3_you_car_accident,id_3_you_any_allergies,id_3_you_asthma,id_3_you_tb,id_3_you_pneumonia,id_3_you_chest_discomfort,id_3_you_heartburn,
            id_3_you_ulcer,id_3_you_rectal_bleading,id_3_you_weightloss,id_3_you_depression,id_3_you_height_fear,id_3_you_play_sports,id_3_you_gp_address;
    private AppCompatEditText id_3_you_heart_disease_comment,id_3_you_blood_pressure_comment,id_3_you_diabetes_comment,id_3_you_epilepsy_comment,
            id_3_you_cancer_comment,id_3_you_thyroid_comment,
            id_3_fam_heart_disease_comment,id_3_fam_blood_pressure_comment,id_3_fam_diabetes_comment,id_3_fam_epilepsy_comment,
            id_3_fam_cancer_comment,id_3_fam_thyroid_comment,id_3_fam_hereditary_comment,id_3_fam_blindness_comment,
            id_3_you_smoking_comment,id_3_you_ex_smoker_comment,id_3_you_skin_disorder_comment,id_3_you_admitted_to_hospital_comment,
            id_3_you_muscles_disease_comment,id_3_you_other_injury_comment,
            id_3_you_pregnant_comment,id_3_you_gynae_comment,id_3_you_headaches_comment,id_3_you_dizziness_comment,id_3_you_hearing_comment,
            id_3_you_vision_comment,id_3_you_head_injury_comment,id_3_you_neurological_comment,
            id_3_you_car_accident_comment,id_3_you_any_allergies_comment,id_3_you_asthma_comment,id_3_you_tb_comment,id_3_you_pneumonia_comment,
            id_3_you_chest_discomfort_comment,id_3_you_heartburn_comment,
            id_3_you_ulcer_comment,id_3_you_rectal_bleading_comment,id_3_you_weightloss_comment,id_3_you_depression_comment,id_3_you_height_fear_comment,
            id_3_you_play_sports_comment,id_3_you_gp_address_comment;


    private AppCompatButton id_1_save,id_2_save,id_7_oto_save;






    private AppCompatButton id_2_employee_button;
    private AppCompatImageView id_2_employee_image;
    private AppCompatEditText id_2_name_surname,id_2_id_number,id_2_gender,id_2_marital_status,
            id_2_home_address,id_2_town,id_2_province,id_2_contact,
            id_2_medical,id_2_company,id_2_job_title,id_2_employee_no,id_2_contractbase,
            id_2_shift_worker,id_2_starting_date,id_2_department,id_2_supervisor,
            id_2_contact_office,
            id_2_company1,id_2_years1,id_2_jobtitle1,id_2_hazard1,
            id_2_company2,id_2_years2,id_2_jobtitle2,id_2_hazard2,
            id_2_company3,id_2_years3,id_2_jobtitle3,id_2_hazard3,
            id_2_company4,id_2_years4,id_2_jobtitle4,id_2_hazard4,id_2_consent,id_2_datesign;
    private AppCompatEditText id_1_company_name,id_1_name,id_1_sur_name,id_1_id_number,id_1_employee_number,id_1_job_title,id_1_department,id_1_address,id_1_town,id_1_region,
            id_1_exam_type_defined,id_1_acknowledged_name,id_1_datesign,id_1_name_surname,id_1_datesign_omp,id_1_date_expiry;
    private RadioGroup id_1_exam_type,id_1_person_record;
    private AppCompatButton id_1_employee_button,id_1_examiner_button,id_1_omp_button;
    private AppCompatImageView id_1_employee_image,id_1_examiner_image,id_1_omp_image;
    private View startView;
    private ProgressDialog dialog;
    public static String tempDir;
    public String currentfilename = null;
    private String uniqueId;
    private PdfWriter writer=null;
    private Document doc=null;
    private File currentfile;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startView= LayoutInflater.from(this).inflate(R.layout.activity_main,null);
        setContentView(startView);
        if (ParseUser.getCurrentUser() == null) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tempDir = Environment.getExternalStorageDirectory() + "/" + Constants.DIRECTORY1 + "/";
        //ContextWrapper cw = new ContextWrapper(getApplicationContext());
        //File directory = cw.getDir(Constants.DIRECTORY, Context.MODE_PRIVATE);
        Constants.prepareDirectory(this, tempDir);


        flipper=(ViewFlipper)startView.findViewById(R.id.viewFlipper2);

        id_sendFull=(AppCompatButton)startView.findViewById(R.id.id_sendFull);
        id_go_next=(AppCompatButton)startView.findViewById(R.id.id_go_next);
        id_go_prev=(AppCompatButton)startView.findViewById(R.id.id_go_prev);
        id_sendFull.setOnClickListener(this);
        id_go_next.setOnClickListener(this);
        id_go_prev.setOnClickListener(this);
        ChangeTitle(0);
        dialog=Constants.getDialog(MainActivity.this);
        Initialize9Screen();
        Initialize8Screen();
        Initialize7Screen();
        Initialize6Screen();
        Initialize5Screen();
        Initialize3Screen();
        Initialize2Screen();
        Initialize1Screen();
        EnableIndividualSending();
    }
    private void EnableIndividualSending()
    {
        id_1_save.setVisibility(View.GONE);
        id_2_save.setVisibility(View.GONE);
        id_3_save.setVisibility(View.GONE);
        id_5_save.setVisibility(View.GONE);
        id_6_save.setVisibility(View.GONE);
        id_7_save.setVisibility(View.GONE);
        id_7_oto_save.setVisibility(View.GONE);
        id_8_save.setVisibility(View.GONE);
        id_9_save.setVisibility(View.GONE);
    }



    private void SaveSign(int s)
    {
        Intent intent = new Intent(getApplicationContext(), CaptureSignature.class);
        startActivityForResult(intent, s);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String sss="";
        switch(requestCode) {
            case Constants.SIGNATURE_EMPLOYEE2:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        employee2=Constants.ConvertToParseFile(sss);
                        try {
                            bemployee2=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_2_employee_image.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_2_employee_image.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_EMPLOYEE6:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        employee6=Constants.ConvertToParseFile(sss);
                        try {
                            bemployee6=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_6_employeesign_image.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_6_employeesign_image.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_TECHNICIAN6:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        technician6=Constants.ConvertToParseFile(sss);
                        try {
                            btechnician6=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_6_techniciansign_image.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_6_techniciansign_image.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_EXAMINER_7_1:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        examsign_7_1=Constants.ConvertToParseFile(sss);
                        try {
                            bexamsign_7_1=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_7_oto_sign_image1.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_7_oto_sign_image1.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_EXAMINER_7_2:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        examsign_7_2=Constants.ConvertToParseFile(sss);
                        try {
                            bexamsign_7_2=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_7_oto_sign_image2.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_7_oto_sign_image2.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_EXAMINER_7_3:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        examsign_7_3=Constants.ConvertToParseFile(sss);
                        try {
                            bexamsign_7_3=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_7_oto_sign_image3.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_7_oto_sign_image3.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_EXAMINER_7_4:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        examsign_7_4=Constants.ConvertToParseFile(sss);
                        try {
                            bexamsign_7_4=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_7_oto_sign_image4.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_7_oto_sign_image4.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_MEDICAL9:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        medfileDate9=Constants.ConvertToParseFile(sss);
                        try {
                            bmedfileDate9=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_9_medical_practioner_image.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_9_medical_practioner_image.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_OCCUPATION9:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        occfileDate9=Constants.ConvertToParseFile(sss);
                        try {
                            boccfileDate9=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_9_occupational_health_practioner_image.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_9_occupational_health_practioner_image.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_EXAMINER8:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        examiner8=Constants.ConvertToParseFile(sss);
                        try {
                            bexaminer8=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_8_examiner_image.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_8_examiner_image.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_EMPLOYEE8:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        employee8=Constants.ConvertToParseFile(sss);
                        try {
                            bemployee8=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_8_employee_image.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_8_employee_image.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_EMPLOYEE1:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        employee1=Constants.ConvertToParseFile(sss);
                        try {
                            bemployee1=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        //bemployee1=Constants.ConvertToByteArray(sss);
                        id_1_employee_image.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_1_employee_image.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_EXAMINER1:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        examiner1=Constants.ConvertToParseFile(sss);
                        try {
                            bexaminer1=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_1_examiner_image.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_1_examiner_image.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case Constants.SIGNATURE_OMP1:
                if (resultCode == RESULT_OK) {
                    Bundle bundle = data.getExtras();
                    String status  = bundle.getString(Constants.STATUS);
                    if(status.equalsIgnoreCase(Constants.DONE)){
                        sss=bundle.getString(Constants.FILEDATA);
                        omp1=Constants.ConvertToParseFile(sss);
                        try {
                            bomp1=new FileInputStream(new File(Constants.getRealPathFromURI(this,Uri.parse(bundle.getString(Constants.FILE)))));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        id_1_omp_image.setImageBitmap(Constants.ConvertToBitmap(sss));
                        id_1_omp_image.setVisibility(View.VISIBLE);
                    }
                }
                break;
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.id_6_employeesign_button:
                SaveSign(Constants.SIGNATURE_EMPLOYEE6);
                break;
            case R.id.id_6_techniciansign_button:
                SaveSign(Constants.SIGNATURE_TECHNICIAN6);
                break;
            case R.id.id_7_oto_sign1:
                SaveSign(Constants.SIGNATURE_EXAMINER_7_1);
                break;
            case R.id.id_7_oto_sign2:
                SaveSign(Constants.SIGNATURE_EXAMINER_7_2);
                break;
            case R.id.id_7_oto_sign3:
                SaveSign(Constants.SIGNATURE_EXAMINER_7_3);
                break;
            case R.id.id_7_oto_sign4:
                SaveSign(Constants.SIGNATURE_EXAMINER_7_4);
                break;
            case R.id.id_9_medical_practioner_button:
                SaveSign(Constants.SIGNATURE_MEDICAL9);
                break;
            case  R.id.id_9_occupational_health_practioner_button:
                SaveSign(Constants.SIGNATURE_OCCUPATION9);
                break;
            case  R.id.id_8_examiner_button:
                SaveSign(Constants.SIGNATURE_EXAMINER8);
                break;
            case  R.id.id_8_employee_button:
                SaveSign(Constants.SIGNATURE_EMPLOYEE8);
                break;
            case  R.id.id_1_employee_button:
                SaveSign(Constants.SIGNATURE_EMPLOYEE1);
                break;
            case  R.id.id_1_examiner_button:
                SaveSign(Constants.SIGNATURE_EXAMINER1);
                break;
            case  R.id.id_1_omp_button:
                SaveSign(Constants.SIGNATURE_OMP1);
                break;
            case  R.id.id_2_employee_button:
                SaveSign(Constants.SIGNATURE_EMPLOYEE2);
                break;
            case  R.id.id_go_next:
                flipper.showNext();
                ChangeTitle(flipper.getDisplayedChild());
                break;
            case  R.id.id_go_prev:
                flipper.showPrevious();
                ChangeTitle(flipper.getDisplayedChild());
                break;
            default:
                valueToSend="";
                switch (v.getId())
                {
                    case R.id.id_9_save:
                        //SaveData9();
                        break;
                    case R.id.id_8_save:
                        //SaveData8();
                        break;
                    case R.id.id_7_save:
                        //SaveData7Audio();
                        break;
                    case R.id.id_7_oto_save:
                        //SaveData7Oto();
                        break;
                    case R.id.id_6_save:
                        //SaveData6();
                        break;
                    case R.id.id_5_save:
                        //SaveData5();
                        break;
                    case R.id.id_3_save:
                        //SaveData3();
                        break;
                    case R.id.id_2_save:
                        //SaveData2();
                        break;
                    case R.id.id_1_save:
                        //SaveData1();
                        break;
                    case R.id.id_sendFull:
                        dialog.show();
                        uniqueId = Constants.GetUniqueID();
                        currentfilename = tempDir+uniqueId + ".pdf";
                        currentfile = new File(currentfilename);
                        doc = new Document(PageSize.A4);
                        try {
                            currentfile.createNewFile();
                            FileOutputStream fOut = new FileOutputStream(currentfile);
                            writer= PdfWriter.getInstance(doc, fOut);
                        } catch (FileNotFoundException|DocumentException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        doc.open();
                        SavePDFData1(doc);
                        break;
                }
                break;
        }
    }

















    private void SavePDFData9(Document doc) {
        final MedicalReferral medicalReferral = new MedicalReferral(ParseUser.getCurrentUser());
        medicalReferral.setId_9_referred_to(id_9_referred_to.getText().toString());
        medicalReferral.setId_9_employement_date(id_9_employement_date.getText().toString());
        medicalReferral.setId_9_employement_no(id_9_employement_no.getText().toString());
        medicalReferral.setId_9_gender(id_9_gender.getText().toString());
        medicalReferral.setId_9_patientName(id_9_patientName.getText().toString());
        medicalReferral.setId_9_dateReferred(id_9_dateReferred.getText().toString());
        medicalReferral.setId_9_occupation(id_9_occupation.getText().toString());
        medicalReferral.setId_9_id_number(id_9_id_number.getText().toString());
        medicalReferral.setId_9_Company(id_9_Company.getText().toString());
        medicalReferral.setId_9_Comments(id_9_Comments.getText().toString());
        medicalReferral.setBoccfileDate9(boccfileDate9);
        medicalReferral.setId_9_occdate(id_9_occdate.getText().toString());
        medicalReferral.setId_9_occpractiseNumber(id_9_occpractiseNumber.getText().toString());
        medicalReferral.setId_9_seenby(id_9_seenby.getText().toString());
        medicalReferral.setId_9_Patient(id_9_Patient.getText().toString());
        medicalReferral.setId_9_medComments(id_9_medComments.getText().toString());
        medicalReferral.setBmedfileDate9(bmedfileDate9);
        medicalReferral.setId_9_meddate(id_9_meddate.getText().toString());
        medicalReferral.setId_9_medpractiseNumber(id_9_medpractiseNumber.getText().toString());
        try {
            medicalReferral.getinPDF(doc);
            doc.close();
            dialog.dismiss();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        Constants.SendEmail(this, "Send Mail", currentfile);
    }
    private void SavePDFData8(Document doc) {
        final SpecialMedicalInvestigation specialMedicalInvestigation = new SpecialMedicalInvestigation(ParseUser.getCurrentUser());
        specialMedicalInvestigation.setId_8_vision(Constants.GetRadioText(this, id_8_vision));
        specialMedicalInvestigation.setId_8_farvision(Constants.GetRadioText(this, id_8_farvision));
        specialMedicalInvestigation.setId_8_nearvision(Constants.GetRadioText(this, id_8_nearvision));
        specialMedicalInvestigation.setId_8_nightvision(Constants.GetRadioText(this, id_8_nightvision));
        specialMedicalInvestigation.setId_8_othervision(Constants.GetRadioText(this, id_8_othervision));
        specialMedicalInvestigation.setId_8_vision_comments(id_8_vision_comments.getText().toString());
        specialMedicalInvestigation.setId_8_audio_plh(id_8_audio_plh.getText().toString());
        specialMedicalInvestigation.setId_8_audio_shift(id_8_audio_shift.getText().toString());
        specialMedicalInvestigation.setId_8_lung(id_8_lung.getText().toString());
        specialMedicalInvestigation.setId_8_fvc(id_8_fvc.getText().toString());
        specialMedicalInvestigation.setId_8_fev(id_8_fev.getText().toString());
        specialMedicalInvestigation.setId_8_fevoverfvc(id_8_fevoverfvc.getText().toString());
        specialMedicalInvestigation.setId_8_peakflow(id_8_peakflow.getText().toString());
        specialMedicalInvestigation.setId_8_chestray(Constants.GetRadioText(this, id_8_chestray));
        specialMedicalInvestigation.setId_8_rest_ecg(Constants.GetRadioText(this, id_8_rest_ecg));
        specialMedicalInvestigation.setId_8_cholestrol(Constants.GetRadioText(this, id_8_cholestrol));
        specialMedicalInvestigation.setId_8_mdt(Constants.GetRadioText(this, id_8_mdt));
        specialMedicalInvestigation.setId_8_other(Constants.GetRadioText(this, id_8_other));
        specialMedicalInvestigation.setId_8_certified(id_8_certified.getText().toString());
        specialMedicalInvestigation.setBemployee8(bemployee8);
        specialMedicalInvestigation.setBexaminer8(bexaminer8);
        specialMedicalInvestigation.setId_8_datesign(id_8_datesign.getText().toString());
        try {
            SavePDFData9(specialMedicalInvestigation.getinPDF(doc));
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
    private void SavePDFData7Oto(Document doc) {
        final OtoscopicExamination otoscopicExamination = new OtoscopicExamination(ParseUser.getCurrentUser());

        otoscopicExamination.setId_7_oto_date1(id_7_oto_date1.getText().toString());
        otoscopicExamination.setBexamsign_7_1(bexamsign_7_1);
        otoscopicExamination.setId_7_oto_perforation1(Constants.GetRadioText(this, id_7_oto_perforation1));
        otoscopicExamination.setId_7_oto_malfunction1(Constants.GetRadioText(this, id_7_oto_malfunction1));
        otoscopicExamination.setId_7_oto_otitis1(Constants.GetRadioText(this, id_7_oto_otitis1));
        otoscopicExamination.setId_7_oto_wax1(Constants.GetRadioText(this, id_7_oto_wax1));
        otoscopicExamination.setId_7_oto_treatment1(Constants.GetRadioText(this, id_7_oto_treatment1));

        otoscopicExamination.setId_7_oto_date2(id_7_oto_date2.getText().toString());
        otoscopicExamination.setBexamsign_7_2(bexamsign_7_2);
        otoscopicExamination.setId_7_oto_perforation2(Constants.GetRadioText(this, id_7_oto_perforation2));
        otoscopicExamination.setId_7_oto_malfunction2(Constants.GetRadioText(this, id_7_oto_malfunction2));
        otoscopicExamination.setId_7_oto_otitis2(Constants.GetRadioText(this, id_7_oto_otitis2));
        otoscopicExamination.setId_7_oto_wax2(Constants.GetRadioText(this, id_7_oto_wax2));
        otoscopicExamination.setId_7_oto_treatment2(Constants.GetRadioText(this, id_7_oto_treatment2));

        otoscopicExamination.setId_7_oto_date3(id_7_oto_date3.getText().toString());
        otoscopicExamination.setBexamsign_7_3(bexamsign_7_3);
        otoscopicExamination.setId_7_oto_perforation3(Constants.GetRadioText(this, id_7_oto_perforation3));
        otoscopicExamination.setId_7_oto_malfunction3(Constants.GetRadioText(this, id_7_oto_malfunction3));
        otoscopicExamination.setId_7_oto_otitis3(Constants.GetRadioText(this, id_7_oto_otitis3));
        otoscopicExamination.setId_7_oto_wax3(Constants.GetRadioText(this, id_7_oto_wax3));
        otoscopicExamination.setId_7_oto_treatment3(Constants.GetRadioText(this, id_7_oto_treatment3));

        otoscopicExamination.setId_7_oto_date4(id_7_oto_date4.getText().toString());
        otoscopicExamination.setBexamsign_7_4(bexamsign_7_4);
        otoscopicExamination.setId_7_oto_perforation4(Constants.GetRadioText(this, id_7_oto_perforation4));
        otoscopicExamination.setId_7_oto_malfunction4(Constants.GetRadioText(this, id_7_oto_malfunction4));
        otoscopicExamination.setId_7_oto_otitis4(Constants.GetRadioText(this, id_7_oto_otitis4));
        otoscopicExamination.setId_7_oto_wax4(Constants.GetRadioText(this, id_7_oto_wax4));
        otoscopicExamination.setId_7_oto_treatment4(Constants.GetRadioText(this, id_7_oto_treatment4));
        try {
            SavePDFData8(otoscopicExamination.getinPDF(doc));
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }
    private void SavePDFData7Audio(Document doc) {
        final AudioQuestionnaire audioQuestionnaire = new AudioQuestionnaire(ParseUser.getCurrentUser());
        audioQuestionnaire.setId_7_audio_date1(id_7_audio_date1.getText().toString());
        audioQuestionnaire.setId_7_audio_cold1(Constants.GetRadioText(this, id_7_audio_cold1));
        audioQuestionnaire.setId_7_audio_cold_comments1(id_7_audio_cold_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_noise1(Constants.GetRadioText(this, id_7_audio_noise1));
        audioQuestionnaire.setId_7_audio_noise_comments1(id_7_audio_noise_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_hobbies1(Constants.GetRadioText(this, id_7_audio_hobbies1));
        audioQuestionnaire.setId_7_audio_hobbies_comments1(id_7_audio_hobbies_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_hearing1(Constants.GetRadioText(this, id_7_audio_hearing1));
        audioQuestionnaire.setId_7_audio_hearing_comments1(id_7_audio_hearing_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_trauma1(Constants.GetRadioText(this, id_7_audio_trauma1));
        audioQuestionnaire.setId_7_audio_trauma_comments1(id_7_audio_trauma_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_tb1(Constants.GetRadioText(this, id_7_audio_tb1));
        audioQuestionnaire.setId_7_audio_tb_comments1(id_7_audio_tb_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_ears1(Constants.GetRadioText(this, id_7_audio_ears1));
        audioQuestionnaire.setId_7_audio_ears_comments1(id_7_audio_ears_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_military1(Constants.GetRadioText(this, id_7_audio_military1));
        audioQuestionnaire.setId_7_audio_military_comments1(id_7_audio_military_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_ppe1(Constants.GetRadioText(this, id_7_audio_ppe1));
        audioQuestionnaire.setId_7_audio_ppe_comments1(id_7_audio_ppe_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_meds1(Constants.GetRadioText(this, id_7_audio_meds1));
        audioQuestionnaire.setId_7_audio_meds_comments1(id_7_audio_meds_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_date2(id_7_audio_date2.getText().toString());
        audioQuestionnaire.setId_7_audio_cold2(Constants.GetRadioText(this, id_7_audio_cold2));
        audioQuestionnaire.setId_7_audio_cold_comments2(id_7_audio_cold_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_noise2(Constants.GetRadioText(this, id_7_audio_noise2));
        audioQuestionnaire.setId_7_audio_noise_comments2(id_7_audio_noise_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_hobbies2(Constants.GetRadioText(this, id_7_audio_hobbies2));
        audioQuestionnaire.setId_7_audio_hobbies_comments2(id_7_audio_hobbies_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_hearing2(Constants.GetRadioText(this, id_7_audio_hearing2));
        audioQuestionnaire.setId_7_audio_hearing_comments2(id_7_audio_hearing_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_trauma2(Constants.GetRadioText(this, id_7_audio_trauma2));
        audioQuestionnaire.setId_7_audio_trauma_comments2(id_7_audio_trauma_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_tb2(Constants.GetRadioText(this, id_7_audio_tb2));
        audioQuestionnaire.setId_7_audio_tb_comments2(id_7_audio_tb_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_ears2(Constants.GetRadioText(this, id_7_audio_ears2));
        audioQuestionnaire.setId_7_audio_ears_comments2(id_7_audio_ears_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_military2(Constants.GetRadioText(this, id_7_audio_military2));
        audioQuestionnaire.setId_7_audio_military_comments2(id_7_audio_military_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_ppe2(Constants.GetRadioText(this, id_7_audio_ppe2));
        audioQuestionnaire.setId_7_audio_ppe_comments2(id_7_audio_ppe_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_meds2(Constants.GetRadioText(this, id_7_audio_meds2));
        audioQuestionnaire.setId_7_audio_meds_comments2(id_7_audio_meds_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_date3(id_7_audio_date3.getText().toString());
        audioQuestionnaire.setId_7_audio_cold3(Constants.GetRadioText(this, id_7_audio_cold3));
        audioQuestionnaire.setId_7_audio_cold_comments3(id_7_audio_cold_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_noise3(Constants.GetRadioText(this, id_7_audio_noise3));
        audioQuestionnaire.setId_7_audio_noise_comments3(id_7_audio_noise_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_hobbies3(Constants.GetRadioText(this, id_7_audio_hobbies3));
        audioQuestionnaire.setId_7_audio_hobbies_comments3(id_7_audio_hobbies_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_hearing3(Constants.GetRadioText(this, id_7_audio_hearing3));
        audioQuestionnaire.setId_7_audio_hearing_comments3(id_7_audio_hearing_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_trauma3(Constants.GetRadioText(this, id_7_audio_trauma3));
        audioQuestionnaire.setId_7_audio_trauma_comments3(id_7_audio_trauma_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_tb3(Constants.GetRadioText(this, id_7_audio_tb3));
        audioQuestionnaire.setId_7_audio_tb_comments3(id_7_audio_tb_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_ears3(Constants.GetRadioText(this, id_7_audio_ears3));
        audioQuestionnaire.setId_7_audio_ears_comments3(id_7_audio_ears_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_military3(Constants.GetRadioText(this, id_7_audio_military3));
        audioQuestionnaire.setId_7_audio_military_comments3(id_7_audio_military_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_ppe3(Constants.GetRadioText(this, id_7_audio_ppe3));
        audioQuestionnaire.setId_7_audio_ppe_comments3(id_7_audio_ppe_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_meds3(Constants.GetRadioText(this, id_7_audio_meds3));
        audioQuestionnaire.setId_7_audio_meds_comments3(id_7_audio_meds_comments3.getText().toString());
        try {
            SavePDFData7Oto(audioQuestionnaire.getinPDF(doc));
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }
    private void SavePDFData6(Document doc) {
        final RespiratoryQuestionnaire respiratoryQuestionnaire = new RespiratoryQuestionnaire(ParseUser.getCurrentUser());
        respiratoryQuestionnaire.setId_6_name(id_6_name.getText().toString());
        respiratoryQuestionnaire.setId_6_dob(id_6_dob.getText().toString());
        respiratoryQuestionnaire.setId_6_jobTitle(id_6_jobTitle.getText().toString());
        respiratoryQuestionnaire.setId_6_weight(id_6_weight.getText().toString());
        respiratoryQuestionnaire.setId_6_height(id_6_height.getText().toString());
        respiratoryQuestionnaire.setId_6_department(id_6_department.getText().toString());
        respiratoryQuestionnaire.setId_6_year_exposed(id_6_year_exposed.getText().toString());
        respiratoryQuestionnaire.setId_6_company_name(id_6_company_name.getText().toString());
        respiratoryQuestionnaire.setId_6_job_title(id_6_job_title.getText().toString());
        respiratoryQuestionnaire.setId_6_hazards_exposed_to(id_6_hazards_exposed_to.getText().toString());
        respiratoryQuestionnaire.setId_6_chest_problem(Constants.GetRadioText(this, id_6_chest_problem));
        respiratoryQuestionnaire.setId_6_chest_problem_comment(id_6_chest_problem_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_lungcondition(Constants.GetRadioText(this, id_6_lungcondition));
        respiratoryQuestionnaire.setId_6_lungcondition_comment(id_6_lungcondition_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_tb(Constants.GetRadioText(this, id_6_tb));
        respiratoryQuestionnaire.setId_6_tb_comment(id_6_tb_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_cold(Constants.GetRadioText(this, id_6_cold));
        respiratoryQuestionnaire.setId_6_cold_comment(id_6_cold_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_smoking(Constants.GetRadioText(this, id_6_smoking));
        respiratoryQuestionnaire.setId_6_smoking_comment(id_6_smoking_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_ex_smoker(Constants.GetRadioText(this, id_6_ex_smoker));
        respiratoryQuestionnaire.setId_6_ex_smoker_comment(id_6_ex_smoker_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_xray(Constants.GetRadioText(this, id_6_xray));
        respiratoryQuestionnaire.setId_6_xray_comment(id_6_xray_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_ppe(Constants.GetRadioText(this, id_6_ppe));
        respiratoryQuestionnaire.setId_6_ppe_comment(id_6_ppe_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_on_treatment(Constants.GetRadioText(this, id_6_on_treatment));
        respiratoryQuestionnaire.setId_6_on_treatment_comment(id_6_on_treatment_comment.getText().toString());
        respiratoryQuestionnaire.setBtechnician6(btechnician6);
        respiratoryQuestionnaire.setBemployee6(bemployee6);
        try {
            SavePDFData7Audio(respiratoryQuestionnaire.getinPDF(doc));
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
    private void SavePDFData5(Document doc) {
        final PhysicalExamination physicalExamination = new PhysicalExamination(ParseUser.getCurrentUser());
        physicalExamination.setId_5_bp(id_5_bp.getText().toString());
        physicalExamination.setId_5_pulse(id_5_pulse.getText().toString());
        physicalExamination.setId_5_weight(id_5_weight.getText().toString());
        physicalExamination.setId_5_bmi(id_5_bmi.getText().toString());
        physicalExamination.setId_5_hgt(id_5_hgt.getText().toString());
        physicalExamination.setId_5_height(id_5_height.getText().toString());
        physicalExamination.setId_5_urine_analysis(id_5_urine_analysis.getText().toString());
        physicalExamination.setId_5_ph(id_5_ph.getText().toString());
        physicalExamination.setId_5_sg(id_5_sg.getText().toString());
        physicalExamination.setId_5_bld(id_5_bld.getText().toString());
        physicalExamination.setId_5_gluc(id_5_gluc.getText().toString());
        physicalExamination.setId_5_leuk(id_5_leuk.getText().toString());
        physicalExamination.setId_5_head(Constants.GetRadioText(this, id_5_head));
        physicalExamination.setId_5_head_comment(id_5_head_comment.getText().toString());
        physicalExamination.setId_5_ear(Constants.GetRadioText(this, id_5_ear));
        physicalExamination.setId_5_ear_comment(id_5_ear_comment.getText().toString());
        physicalExamination.setId_5_lungs(Constants.GetRadioText(this, id_5_lungs));
        physicalExamination.setId_5_lungs_comment(id_5_lungs_comment.getText().toString());
        physicalExamination.setId_5_heart(Constants.GetRadioText(this, id_5_heart));
        physicalExamination.setId_5_heart_comment(id_5_heart_comment.getText().toString());
        physicalExamination.setId_5_vascular(Constants.GetRadioText(this, id_5_vascular));
        physicalExamination.setId_5_vascular_comment(id_5_vascular_comment.getText().toString());
        physicalExamination.setId_5_abdomen(Constants.GetRadioText(this, id_5_abdomen));
        physicalExamination.setId_5_abdomen_comment(id_5_abdomen_comment.getText().toString());
        physicalExamination.setId_5_genito(Constants.GetRadioText(this, id_5_genito));
        physicalExamination.setId_5_genito_comment(id_5_genito_comment.getText().toString());
        physicalExamination.setId_5_neurological(Constants.GetRadioText(this, id_5_neurological));
        physicalExamination.setId_5_neurological_comment(id_5_neurological_comment.getText().toString());
        physicalExamination.setId_5_limbs(Constants.GetRadioText(this, id_5_limbs));
        physicalExamination.setId_5_limbs_comment(id_5_limbs_comment.getText().toString());
        physicalExamination.setId_5_spine(Constants.GetRadioText(this, id_5_spine));
        physicalExamination.setId_5_spine_comment(id_5_spine_comment.getText().toString());
        physicalExamination.setId_5_skin(Constants.GetRadioText(this, id_5_skin));
        physicalExamination.setId_5_skin_comment(id_5_skin_comment.getText().toString());
        physicalExamination.setId_5_psychological(Constants.GetRadioText(this, id_5_psychological));
        physicalExamination.setId_5_psychological_comment(id_5_psychological_comment.getText().toString());
        try {
            SavePDFData6(physicalExamination.getinPDF(doc));
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }
    private void SavePDFData3(Document doc) {
        final MedicalHistory medicalHistory = new MedicalHistory(ParseUser.getCurrentUser());
        medicalHistory.setId_3_fam_blindness(Constants.GetRadioText(this, id_3_fam_blindness));
        medicalHistory.setId_3_fam_blood_pressure(Constants.GetRadioText(this, id_3_fam_blood_pressure));
        medicalHistory.setId_3_fam_cancer(Constants.GetRadioText(this, id_3_fam_cancer));
        medicalHistory.setId_3_fam_diabetes(Constants.GetRadioText(this, id_3_fam_diabetes));
        medicalHistory.setId_3_fam_epilepsy(Constants.GetRadioText(this, id_3_fam_epilepsy));
        medicalHistory.setId_3_fam_heart_disease(Constants.GetRadioText(this, id_3_fam_heart_disease));
        medicalHistory.setId_3_fam_hereditary(Constants.GetRadioText(this, id_3_fam_hereditary));
        medicalHistory.setId_3_fam_thyroid(Constants.GetRadioText(this, id_3_fam_thyroid));
        medicalHistory.setId_3_fam_blindness_comment(id_3_fam_blindness_comment.getText().toString());
        medicalHistory.setId_3_fam_blood_pressure_comment(id_3_fam_blood_pressure_comment.getText().toString());
        medicalHistory.setId_3_fam_cancer_comment(id_3_fam_cancer_comment.getText().toString());
        medicalHistory.setId_3_fam_diabetes_comment(id_3_fam_diabetes_comment.getText().toString());
        medicalHistory.setId_3_fam_epilepsy_comment(id_3_fam_epilepsy_comment.getText().toString());
        medicalHistory.setId_3_fam_heart_disease_comment(id_3_fam_heart_disease_comment.getText().toString());
        medicalHistory.setId_3_fam_hereditary_comment(id_3_fam_hereditary_comment.getText().toString());
        medicalHistory.setId_3_fam_thyroid_comment(id_3_fam_thyroid_comment.getText().toString());
        medicalHistory.setId_3_you_heart_disease(Constants.GetRadioText(this, id_3_you_heart_disease));
        medicalHistory.setId_3_you_heart_disease_comment(id_3_you_heart_disease_comment.getText().toString());
        medicalHistory.setId_3_you_blood_pressure(Constants.GetRadioText(this, id_3_you_blood_pressure));
        medicalHistory.setId_3_you_blood_pressure_comment(id_3_you_blood_pressure_comment.getText().toString());
        medicalHistory.setId_3_you_diabetes(Constants.GetRadioText(this, id_3_you_diabetes));
        medicalHistory.setId_3_you_diabetes_comment(id_3_you_diabetes_comment.getText().toString());
        medicalHistory.setId_3_you_epilepsy(Constants.GetRadioText(this, id_3_you_epilepsy));
        medicalHistory.setId_3_you_epilepsy_comment(id_3_you_epilepsy_comment.getText().toString());
        medicalHistory.setId_3_you_cancer(Constants.GetRadioText(this, id_3_you_cancer));
        medicalHistory.setId_3_you_cancer_comment(id_3_you_cancer_comment.getText().toString());
        medicalHistory.setId_3_you_thyroid(Constants.GetRadioText(this, id_3_you_thyroid));
        medicalHistory.setId_3_you_thyroid_comment(id_3_you_thyroid_comment.getText().toString());
        medicalHistory.setId_3_you_drugs_type(id_3_you_drugs_type.getText().toString());
        medicalHistory.setId_3_you_drugs_quantity(id_3_you_drugs_quantity.getText().toString());
        medicalHistory.setId_3_you_smoking(Constants.GetRadioText(this, id_3_you_smoking));
        medicalHistory.setId_3_you_smoking_comment(id_3_you_smoking_comment.getText().toString());
        medicalHistory.setId_3_you_ex_smoker(Constants.GetRadioText(this, id_3_you_ex_smoker));
        medicalHistory.setId_3_you_ex_smoker_comment(id_3_you_ex_smoker_comment.getText().toString());
        medicalHistory.setId_3_you_skin_disorder(Constants.GetRadioText(this, id_3_you_skin_disorder));
        medicalHistory.setId_3_you_skin_disorder_comment(id_3_you_skin_disorder_comment.getText().toString());
        medicalHistory.setId_3_you_admitted_to_hospital(Constants.GetRadioText(this, id_3_you_admitted_to_hospital));
        medicalHistory.setId_3_you_admitted_to_hospital_comment(id_3_you_admitted_to_hospital_comment.getText().toString());
        medicalHistory.setId_3_you_muscles_disease(Constants.GetRadioText(this, id_3_you_muscles_disease));
        medicalHistory.setId_3_you_muscles_disease_comment(id_3_you_muscles_disease_comment.getText().toString());
        medicalHistory.setId_3_you_other_injury(Constants.GetRadioText(this, id_3_you_other_injury));
        medicalHistory.setId_3_you_other_injury_comment(id_3_you_other_injury_comment.getText().toString());
        medicalHistory.setId_3_you_pregnant(Constants.GetRadioText(this, id_3_you_pregnant));
        medicalHistory.setId_3_you_pregnant_comment(id_3_you_pregnant_comment.getText().toString());
        medicalHistory.setId_3_you_gynae(Constants.GetRadioText(this, id_3_you_gynae));
        medicalHistory.setId_3_you_gynae_comment(id_3_you_gynae_comment.getText().toString());
        medicalHistory.setId_3_you_headaches(Constants.GetRadioText(this, id_3_you_headaches));
        medicalHistory.setId_3_you_headaches_comment(id_3_you_headaches_comment.getText().toString());
        medicalHistory.setId_3_you_dizziness(Constants.GetRadioText(this, id_3_you_dizziness));
        medicalHistory.setId_3_you_dizziness_comment(id_3_you_dizziness_comment.getText().toString());
        medicalHistory.setId_3_you_hearing(Constants.GetRadioText(this, id_3_you_hearing));
        medicalHistory.setId_3_you_hearing_comment(id_3_you_hearing_comment.getText().toString());
        medicalHistory.setId_3_you_vision(Constants.GetRadioText(this, id_3_you_vision));
        medicalHistory.setId_3_you_vision_comment(id_3_you_vision_comment.getText().toString());
        medicalHistory.setId_3_you_head_injury(Constants.GetRadioText(this, id_3_you_head_injury));
        medicalHistory.setId_3_you_head_injury_comment(id_3_you_head_injury_comment.getText().toString());
        medicalHistory.setId_3_you_neurological(Constants.GetRadioText(this, id_3_you_neurological));
        medicalHistory.setId_3_you_neurological_comment(id_3_you_neurological_comment.getText().toString());
        medicalHistory.setId_3_you_car_accident(Constants.GetRadioText(this, id_3_you_car_accident));
        medicalHistory.setId_3_you_car_accident_comment(id_3_you_car_accident_comment.getText().toString());
        medicalHistory.setId_3_you_any_allergies(Constants.GetRadioText(this, id_3_you_any_allergies));
        medicalHistory.setId_3_you_any_allergies_comment(id_3_you_any_allergies_comment.getText().toString());
        medicalHistory.setId_3_you_asthma(Constants.GetRadioText(this, id_3_you_asthma));
        medicalHistory.setId_3_you_asthma_comment(id_3_you_asthma_comment.getText().toString());
        medicalHistory.setId_3_you_tb(Constants.GetRadioText(this, id_3_you_tb));
        medicalHistory.setId_3_you_tb_comment(id_3_you_tb_comment.getText().toString());
        medicalHistory.setId_3_you_pneumonia(Constants.GetRadioText(this, id_3_you_pneumonia));
        medicalHistory.setId_3_you_pneumonia_comment(id_3_you_pneumonia_comment.getText().toString());
        medicalHistory.setId_3_you_chest_discomfort(Constants.GetRadioText(this, id_3_you_chest_discomfort));
        medicalHistory.setId_3_you_chest_discomfort_comment(id_3_you_chest_discomfort_comment.getText().toString());
        medicalHistory.setId_3_you_heartburn(Constants.GetRadioText(this, id_3_you_heartburn));
        medicalHistory.setId_3_you_heartburn_comment(id_3_you_heartburn_comment.getText().toString());
        medicalHistory.setId_3_you_ulcer(Constants.GetRadioText(this, id_3_you_ulcer));
        medicalHistory.setId_3_you_ulcer_comment(id_3_you_ulcer_comment.getText().toString());
        medicalHistory.setId_3_you_rectal_bleading(Constants.GetRadioText(this, id_3_you_rectal_bleading));
        medicalHistory.setId_3_you_rectal_bleading_comment(id_3_you_rectal_bleading_comment.getText().toString());
        medicalHistory.setId_3_you_weightloss(Constants.GetRadioText(this, id_3_you_weightloss));
        medicalHistory.setId_3_you_weightloss_comment(id_3_you_weightloss_comment.getText().toString());
        medicalHistory.setId_3_you_depression(Constants.GetRadioText(this, id_3_you_depression));
        medicalHistory.setId_3_you_depression_comment(id_3_you_depression_comment.getText().toString());
        medicalHistory.setId_3_you_height_fear(Constants.GetRadioText(this, id_3_you_height_fear));
        medicalHistory.setId_3_you_height_fear_comment(id_3_you_height_fear_comment.getText().toString());
        medicalHistory.setId_3_you_play_sports(Constants.GetRadioText(this, id_3_you_play_sports));
        medicalHistory.setId_3_you_play_sports_comment(id_3_you_play_sports_comment.getText().toString());
        medicalHistory.setId_3_you_gp_address(Constants.GetRadioText(this, id_3_you_gp_address));
        medicalHistory.setId_3_you_gp_address_comment(id_3_you_gp_address_comment.getText().toString());
        try {
            SavePDFData5(medicalHistory.getinPDF(doc));
        } catch (DocumentException e) {
            e.printStackTrace();
        }

    }
    private void SavePDFData2(Document doc) {
        final OccupationalHealthRecord occupationalHealthRecord=new OccupationalHealthRecord(ParseUser.getCurrentUser());
        occupationalHealthRecord.setId_2_years4(id_2_years4.getText().toString());
        occupationalHealthRecord.setId_2_years3(id_2_years3.getText().toString());
        occupationalHealthRecord.setId_2_years2(id_2_years2.getText().toString());
        occupationalHealthRecord.setId_2_years1(id_2_years1.getText().toString());
        occupationalHealthRecord.setId_2_town(id_2_town.getText().toString());
        occupationalHealthRecord.setId_2_company(id_2_company.getText().toString());
        occupationalHealthRecord.setId_2_contact(id_2_contact.getText().toString());
        occupationalHealthRecord.setId_2_company1(id_2_company1.getText().toString());
        occupationalHealthRecord.setId_2_company2(id_2_company2.getText().toString());
        occupationalHealthRecord.setId_2_company3(id_2_company3.getText().toString());
        occupationalHealthRecord.setId_2_company4(id_2_company4.getText().toString());
        occupationalHealthRecord.setId_2_consent(id_2_consent.getText().toString());
        occupationalHealthRecord.setId_2_contact_office(id_2_contact_office.getText().toString());
        occupationalHealthRecord.setId_2_contractbase(id_2_contractbase.getText().toString());
        occupationalHealthRecord.setId_2_datesign(id_2_datesign.getText().toString());
        occupationalHealthRecord.setId_2_department(id_2_department.getText().toString());
        occupationalHealthRecord.setId_2_employee_no(id_2_employee_no.getText().toString());
        occupationalHealthRecord.setId_2_gender(id_2_gender.getText().toString());
        occupationalHealthRecord.setId_2_hazard1(id_2_hazard1.getText().toString());
        occupationalHealthRecord.setId_2_hazard2(id_2_hazard2.getText().toString());
        occupationalHealthRecord.setId_2_hazard3(id_2_hazard3.getText().toString());
        occupationalHealthRecord.setId_2_hazard4(id_2_hazard4.getText().toString());
        occupationalHealthRecord.setId_2_home_address(id_2_home_address.getText().toString());
        occupationalHealthRecord.setId_2_id_number(id_2_id_number.getText().toString());
        occupationalHealthRecord.setId_2_job_title(id_2_job_title.getText().toString());
        occupationalHealthRecord.setId_2_jobtitle1(id_2_jobtitle1.getText().toString());
        occupationalHealthRecord.setId_2_jobtitle2(id_2_jobtitle2.getText().toString());
        occupationalHealthRecord.setId_2_jobtitle3(id_2_jobtitle3.getText().toString());
        occupationalHealthRecord.setId_2_jobtitle4(id_2_jobtitle4.getText().toString());
        occupationalHealthRecord.setId_2_marital_status(id_2_marital_status.getText().toString());
        occupationalHealthRecord.setId_2_medical(id_2_medical.getText().toString());
        occupationalHealthRecord.setId_2_name_surname(id_2_name_surname.getText().toString());
        occupationalHealthRecord.setId_2_province(id_2_province.getText().toString());
        occupationalHealthRecord.setId_2_shift_worker(id_2_shift_worker.getText().toString());
        occupationalHealthRecord.setId_2_starting_date(id_2_starting_date.getText().toString());
        occupationalHealthRecord.setId_2_supervisor(id_2_supervisor.getText().toString());
        occupationalHealthRecord.setBemployee2(bemployee2);
        try {
            SavePDFData3(occupationalHealthRecord.getinPDF(doc));
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
    private void SavePDFData1(Document doc){
        final MedicalFitnessCertificate medicalFitnessCertificate=new MedicalFitnessCertificate(ParseUser.getCurrentUser());
        medicalFitnessCertificate.setId_1_company_name(id_1_company_name.getText().toString());
        medicalFitnessCertificate.setId_1_acknowledged_name(id_1_acknowledged_name.getText().toString());
        medicalFitnessCertificate.setId_1_address(id_1_address.getText().toString());
        medicalFitnessCertificate.setId_1_date_expiry(id_1_date_expiry.getText().toString());
        medicalFitnessCertificate.setId_1_datesign(id_1_datesign.getText().toString());
        medicalFitnessCertificate.setId_1_datesign_omp(id_1_datesign_omp.getText().toString());
        medicalFitnessCertificate.setId_1_department(id_1_department.getText().toString());
        medicalFitnessCertificate.setId_1_employee_number(id_1_employee_number.getText().toString());
        medicalFitnessCertificate.setId_1_exam_type_defined(id_1_exam_type_defined.getText().toString());
        medicalFitnessCertificate.setId_1_id_number(id_1_id_number.getText().toString());
        medicalFitnessCertificate.setId_1_job_title(id_1_job_title.getText().toString());
        medicalFitnessCertificate.setId_1_name(id_1_name.getText().toString());
        medicalFitnessCertificate.setId_1_name_surname(id_1_name_surname.getText().toString());
        medicalFitnessCertificate.setId_1_sur_name(id_1_sur_name.getText().toString());
        medicalFitnessCertificate.setId_1_town(id_1_town.getText().toString());
        medicalFitnessCertificate.setId_1_region(id_1_region.getText().toString());
        medicalFitnessCertificate.setId_1_exam_type(Constants.GetRadioText(this, id_1_exam_type));
        medicalFitnessCertificate.setId_1_person_record(Constants.GetRadioText(this, id_1_person_record));
        medicalFitnessCertificate.setBemployee1(bemployee1);
        medicalFitnessCertificate.setBexaminer1(bexaminer1);
        medicalFitnessCertificate.setBomp1(bomp1);
        try {
            SavePDFData2(medicalFitnessCertificate.getinPDF(doc));
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }







    /*private void SaveData9() {
        final MedicalReferral medicalReferral = new MedicalReferral(ParseUser.getCurrentUser());
        medicalReferral.setId_9_referred_to(id_9_referred_to.getText().toString());
        medicalReferral.setId_9_employement_date(id_9_employement_date.getText().toString());
        medicalReferral.setId_9_employement_no(id_9_employement_no.getText().toString());
        medicalReferral.setId_9_gender(id_9_gender.getText().toString());
        medicalReferral.setId_9_patientName(id_9_patientName.getText().toString());
        medicalReferral.setId_9_dateReferred(id_9_dateReferred.getText().toString());
        medicalReferral.setId_9_occupation(id_9_occupation.getText().toString());
        medicalReferral.setId_9_id_number(id_9_id_number.getText().toString());
        medicalReferral.setId_9_Company(id_9_Company.getText().toString());
        medicalReferral.setId_9_Comments(id_9_Comments.getText().toString());
        medicalReferral.setOccfileDate9(occfileDate9);
        medicalReferral.setId_9_occdate(id_9_occdate.getText().toString());
        medicalReferral.setId_9_occpractiseNumber(id_9_occpractiseNumber.getText().toString());
        medicalReferral.setId_9_seenby(id_9_seenby.getText().toString());
        medicalReferral.setId_9_Patient(id_9_Patient.getText().toString());
        medicalReferral.setId_9_medComments(id_9_medComments.getText().toString());
        medicalReferral.setMedfileDate9(medfileDate9);
        medicalReferral.setId_9_meddate(id_9_meddate.getText().toString());
        medicalReferral.setId_9_medpractiseNumber(id_9_medpractiseNumber.getText().toString());
        ParseQuery<MedicalReferral> query=ParseQuery.getQuery("MedicalReferral");
        query= query.whereEqualTo("user",ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<MedicalReferral>() {
            @Override
            public void done(List<MedicalReferral> objects, ParseException e) {
                if (e != null) {
                    if (objects.size() > 0) {
                        medicalReferral.setObjectId(objects.get(0).getObjectId());

                    }
                    medicalReferral.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            dialog.dismiss();
                            Toast.makeText(MainActivity.this, "Saved", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

    }
    private void SaveData8() {
        final SpecialMedicalInvestigation specialMedicalInvestigation = new SpecialMedicalInvestigation(ParseUser.getCurrentUser());
        specialMedicalInvestigation.setId_8_vision(Constants.GetRadioText(this, id_8_vision));
        specialMedicalInvestigation.setId_8_farvision(Constants.GetRadioText(this, id_8_farvision));
        specialMedicalInvestigation.setId_8_nearvision(Constants.GetRadioText(this, id_8_nearvision));
        specialMedicalInvestigation.setId_8_nightvision(Constants.GetRadioText(this, id_8_nightvision));
        specialMedicalInvestigation.setId_8_othervision(Constants.GetRadioText(this, id_8_othervision));
        specialMedicalInvestigation.setId_8_vision_comments(id_8_vision_comments.getText().toString());
        specialMedicalInvestigation.setId_8_audio_plh(id_8_audio_plh.getText().toString());
        specialMedicalInvestigation.setId_8_audio_shift(id_8_audio_shift.getText().toString());
        specialMedicalInvestigation.setId_8_lung(id_8_lung.getText().toString());
        specialMedicalInvestigation.setId_8_fvc(id_8_fvc.getText().toString());
        specialMedicalInvestigation.setId_8_fev(id_8_fev.getText().toString());
        specialMedicalInvestigation.setId_8_fevoverfvc(id_8_fevoverfvc.getText().toString());
        specialMedicalInvestigation.setId_8_peakflow(id_8_peakflow.getText().toString());
        specialMedicalInvestigation.setId_8_chestray(Constants.GetRadioText(this, id_8_chestray));
        specialMedicalInvestigation.setId_8_rest_ecg(Constants.GetRadioText(this, id_8_rest_ecg));
        specialMedicalInvestigation.setId_8_cholestrol(Constants.GetRadioText(this, id_8_cholestrol));
        specialMedicalInvestigation.setId_8_mdt(Constants.GetRadioText(this, id_8_mdt));
        specialMedicalInvestigation.setId_8_other(Constants.GetRadioText(this, id_8_other));
        specialMedicalInvestigation.setId_8_certified(id_8_certified.getText().toString());
        specialMedicalInvestigation.setEmployee8(employee8);
        specialMedicalInvestigation.setExaminer8(examiner8);
        specialMedicalInvestigation.setId_8_datesign(id_8_datesign.getText().toString());
        ParseQuery<SpecialMedicalInvestigation> query=ParseQuery.getQuery("SpecialMedicalInvestigation");
        query= query.whereEqualTo("user",ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<SpecialMedicalInvestigation>() {
            @Override
            public void done(List<SpecialMedicalInvestigation> objects, ParseException e) {
                if (e != null) {
                    if (objects.size() > 0) {
                        specialMedicalInvestigation.setObjectId(objects.get(0).getObjectId());

                    }
                    specialMedicalInvestigation.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            SaveData9();
                        }
                    });
                }
            }
        });

    }
    private void SaveData7Oto() {
        final OtoscopicExamination otoscopicExamination = new OtoscopicExamination(ParseUser.getCurrentUser());

        otoscopicExamination.setId_7_oto_date1(id_7_oto_date1.getText().toString());
        otoscopicExamination.setExamsign_7_1(examsign_7_1);
        otoscopicExamination.setId_7_oto_perforation1(Constants.GetRadioText(this, id_7_oto_perforation1));
        otoscopicExamination.setId_7_oto_malfunction1(Constants.GetRadioText(this, id_7_oto_malfunction1));
        otoscopicExamination.setId_7_oto_otitis1(Constants.GetRadioText(this, id_7_oto_otitis1));
        otoscopicExamination.setId_7_oto_wax1(Constants.GetRadioText(this, id_7_oto_wax1));
        otoscopicExamination.setId_7_oto_treatment1(Constants.GetRadioText(this, id_7_oto_treatment1));

        otoscopicExamination.setId_7_oto_date2(id_7_oto_date2.getText().toString());
        otoscopicExamination.setExamsign_7_2(examsign_7_2);
        otoscopicExamination.setId_7_oto_perforation2(Constants.GetRadioText(this, id_7_oto_perforation2));
        otoscopicExamination.setId_7_oto_malfunction2(Constants.GetRadioText(this, id_7_oto_malfunction2));
        otoscopicExamination.setId_7_oto_otitis2(Constants.GetRadioText(this, id_7_oto_otitis2));
        otoscopicExamination.setId_7_oto_wax2(Constants.GetRadioText(this, id_7_oto_wax2));
        otoscopicExamination.setId_7_oto_treatment2(Constants.GetRadioText(this, id_7_oto_treatment2));

        otoscopicExamination.setId_7_oto_date3(id_7_oto_date3.getText().toString());
        otoscopicExamination.setExamsign_7_3(examsign_7_3);
        otoscopicExamination.setId_7_oto_perforation3(Constants.GetRadioText(this, id_7_oto_perforation3));
        otoscopicExamination.setId_7_oto_malfunction3(Constants.GetRadioText(this, id_7_oto_malfunction3));
        otoscopicExamination.setId_7_oto_otitis3(Constants.GetRadioText(this, id_7_oto_otitis3));
        otoscopicExamination.setId_7_oto_wax3(Constants.GetRadioText(this, id_7_oto_wax3));
        otoscopicExamination.setId_7_oto_treatment3(Constants.GetRadioText(this, id_7_oto_treatment3));

        otoscopicExamination.setId_7_oto_date4(id_7_oto_date4.getText().toString());
        otoscopicExamination.setExamsign_7_4(examsign_7_4);
        otoscopicExamination.setId_7_oto_perforation4(Constants.GetRadioText(this, id_7_oto_perforation4));
        otoscopicExamination.setId_7_oto_malfunction4(Constants.GetRadioText(this, id_7_oto_malfunction4));
        otoscopicExamination.setId_7_oto_otitis4(Constants.GetRadioText(this, id_7_oto_otitis4));
        otoscopicExamination.setId_7_oto_wax4(Constants.GetRadioText(this, id_7_oto_wax4));
        otoscopicExamination.setId_7_oto_treatment4(Constants.GetRadioText(this, id_7_oto_treatment4));
        ParseQuery<OtoscopicExamination> query=ParseQuery.getQuery("OtoscopicExamination");
        query= query.whereEqualTo("user",ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<OtoscopicExamination>() {
            @Override
            public void done(List<OtoscopicExamination> objects, ParseException e) {
                if (e != null) {
                    if (objects.size() > 0) {
                        otoscopicExamination.setObjectId(objects.get(0).getObjectId());

                    }
                    otoscopicExamination.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            SaveData8();
                        }
                    });
                }
            }
        });

    }
    private void SaveData7Audio() {
        final AudioQuestionnaire audioQuestionnaire = new AudioQuestionnaire(ParseUser.getCurrentUser());
        audioQuestionnaire.setId_7_audio_date1(id_7_audio_date1.getText().toString());
        audioQuestionnaire.setId_7_audio_cold1(Constants.GetRadioText(this, id_7_audio_cold1));
        audioQuestionnaire.setId_7_audio_cold_comments1(id_7_audio_cold_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_noise1(Constants.GetRadioText(this, id_7_audio_noise1));
        audioQuestionnaire.setId_7_audio_noise_comments1(id_7_audio_noise_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_hobbies1(Constants.GetRadioText(this, id_7_audio_hobbies1));
        audioQuestionnaire.setId_7_audio_hobbies_comments1(id_7_audio_hobbies_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_hearing1(Constants.GetRadioText(this, id_7_audio_hearing1));
        audioQuestionnaire.setId_7_audio_hearing_comments1(id_7_audio_hearing_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_trauma1(Constants.GetRadioText(this, id_7_audio_trauma1));
        audioQuestionnaire.setId_7_audio_trauma_comments1(id_7_audio_trauma_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_tb1(Constants.GetRadioText(this, id_7_audio_tb1));
        audioQuestionnaire.setId_7_audio_tb_comments1(id_7_audio_tb_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_ears1(Constants.GetRadioText(this, id_7_audio_ears1));
        audioQuestionnaire.setId_7_audio_ears_comments1(id_7_audio_ears_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_military1(Constants.GetRadioText(this, id_7_audio_military1));
        audioQuestionnaire.setId_7_audio_military_comments1(id_7_audio_military_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_ppe1(Constants.GetRadioText(this, id_7_audio_ppe1));
        audioQuestionnaire.setId_7_audio_ppe_comments1(id_7_audio_ppe_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_meds1(Constants.GetRadioText(this, id_7_audio_meds1));
        audioQuestionnaire.setId_7_audio_meds_comments1(id_7_audio_meds_comments1.getText().toString());
        audioQuestionnaire.setId_7_audio_date2(id_7_audio_date2.getText().toString());
        audioQuestionnaire.setId_7_audio_cold2(Constants.GetRadioText(this, id_7_audio_cold2));
        audioQuestionnaire.setId_7_audio_cold_comments2(id_7_audio_cold_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_noise2(Constants.GetRadioText(this, id_7_audio_noise2));
        audioQuestionnaire.setId_7_audio_noise_comments2(id_7_audio_noise_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_hobbies2(Constants.GetRadioText(this, id_7_audio_hobbies2));
        audioQuestionnaire.setId_7_audio_hobbies_comments2(id_7_audio_hobbies_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_hearing2(Constants.GetRadioText(this, id_7_audio_hearing2));
        audioQuestionnaire.setId_7_audio_hearing_comments2(id_7_audio_hearing_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_trauma2(Constants.GetRadioText(this, id_7_audio_trauma2));
        audioQuestionnaire.setId_7_audio_trauma_comments2(id_7_audio_trauma_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_tb2(Constants.GetRadioText(this, id_7_audio_tb2));
        audioQuestionnaire.setId_7_audio_tb_comments2(id_7_audio_tb_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_ears2(Constants.GetRadioText(this, id_7_audio_ears2));
        audioQuestionnaire.setId_7_audio_ears_comments2(id_7_audio_ears_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_military2(Constants.GetRadioText(this, id_7_audio_military2));
        audioQuestionnaire.setId_7_audio_military_comments2(id_7_audio_military_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_ppe2(Constants.GetRadioText(this, id_7_audio_ppe2));
        audioQuestionnaire.setId_7_audio_ppe_comments2(id_7_audio_ppe_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_meds2(Constants.GetRadioText(this, id_7_audio_meds2));
        audioQuestionnaire.setId_7_audio_meds_comments2(id_7_audio_meds_comments2.getText().toString());
        audioQuestionnaire.setId_7_audio_date3(id_7_audio_date3.getText().toString());
        audioQuestionnaire.setId_7_audio_cold3(Constants.GetRadioText(this, id_7_audio_cold3));
        audioQuestionnaire.setId_7_audio_cold_comments3(id_7_audio_cold_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_noise3(Constants.GetRadioText(this, id_7_audio_noise3));
        audioQuestionnaire.setId_7_audio_noise_comments3(id_7_audio_noise_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_hobbies3(Constants.GetRadioText(this, id_7_audio_hobbies3));
        audioQuestionnaire.setId_7_audio_hobbies_comments3(id_7_audio_hobbies_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_hearing3(Constants.GetRadioText(this, id_7_audio_hearing3));
        audioQuestionnaire.setId_7_audio_hearing_comments3(id_7_audio_hearing_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_trauma3(Constants.GetRadioText(this, id_7_audio_trauma3));
        audioQuestionnaire.setId_7_audio_trauma_comments3(id_7_audio_trauma_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_tb3(Constants.GetRadioText(this, id_7_audio_tb3));
        audioQuestionnaire.setId_7_audio_tb_comments3(id_7_audio_tb_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_ears3(Constants.GetRadioText(this, id_7_audio_ears3));
        audioQuestionnaire.setId_7_audio_ears_comments3(id_7_audio_ears_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_military3(Constants.GetRadioText(this, id_7_audio_military3));
        audioQuestionnaire.setId_7_audio_military_comments3(id_7_audio_military_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_ppe3(Constants.GetRadioText(this, id_7_audio_ppe3));
        audioQuestionnaire.setId_7_audio_ppe_comments3(id_7_audio_ppe_comments3.getText().toString());
        audioQuestionnaire.setId_7_audio_meds3(Constants.GetRadioText(this, id_7_audio_meds3));
        audioQuestionnaire.setId_7_audio_meds_comments3(id_7_audio_meds_comments3.getText().toString());
        ParseQuery<AudioQuestionnaire> query=ParseQuery.getQuery("AudioQuestionnaire");
        query= query.whereEqualTo("user",ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<AudioQuestionnaire>() {
            @Override
            public void done(List<AudioQuestionnaire> objects, ParseException e) {
                if (e != null) {
                    if (objects.size() > 0) {
                        audioQuestionnaire.setObjectId(objects.get(0).getObjectId());

                    }
                    audioQuestionnaire.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            SaveData7Oto();
                        }
                    });
                }
            }
        });

    }
    private void SaveData6() {
        final RespiratoryQuestionnaire respiratoryQuestionnaire = new RespiratoryQuestionnaire(ParseUser.getCurrentUser());
        respiratoryQuestionnaire.setId_6_name(id_6_name.getText().toString());
        respiratoryQuestionnaire.setId_6_dob(id_6_dob.getText().toString());
        respiratoryQuestionnaire.setId_6_jobTitle(id_6_jobTitle.getText().toString());
        respiratoryQuestionnaire.setId_6_weight(id_6_weight.getText().toString());
        respiratoryQuestionnaire.setId_6_height(id_6_height.getText().toString());
        respiratoryQuestionnaire.setId_6_department(id_6_department.getText().toString());
        respiratoryQuestionnaire.setId_6_year_exposed(id_6_year_exposed.getText().toString());
        respiratoryQuestionnaire.setId_6_company_name(id_6_company_name.getText().toString());
        respiratoryQuestionnaire.setId_6_job_title(id_6_job_title.getText().toString());
        respiratoryQuestionnaire.setId_6_hazards_exposed_to(id_6_hazards_exposed_to.getText().toString());
        respiratoryQuestionnaire.setId_6_chest_problem(Constants.GetRadioText(this, id_6_chest_problem));
        respiratoryQuestionnaire.setId_6_chest_problem_comment(id_6_chest_problem_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_lungcondition(Constants.GetRadioText(this, id_6_lungcondition));
        respiratoryQuestionnaire.setId_6_lungcondition_comment(id_6_lungcondition_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_tb(Constants.GetRadioText(this, id_6_tb));
        respiratoryQuestionnaire.setId_6_tb_comment(id_6_tb_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_cold(Constants.GetRadioText(this, id_6_cold));
        respiratoryQuestionnaire.setId_6_cold_comment(id_6_cold_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_smoking(Constants.GetRadioText(this, id_6_smoking));
        respiratoryQuestionnaire.setId_6_smoking_comment(id_6_smoking_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_ex_smoker(Constants.GetRadioText(this, id_6_ex_smoker));
        respiratoryQuestionnaire.setId_6_ex_smoker_comment(id_6_ex_smoker_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_xray(Constants.GetRadioText(this, id_6_xray));
        respiratoryQuestionnaire.setId_6_xray_comment(id_6_xray_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_ppe(Constants.GetRadioText(this, id_6_ppe));
        respiratoryQuestionnaire.setId_6_ppe_comment(id_6_ppe_comment.getText().toString());
        respiratoryQuestionnaire.setId_6_on_treatment(Constants.GetRadioText(this, id_6_on_treatment));
        respiratoryQuestionnaire.setId_6_on_treatment_comment(id_6_on_treatment_comment.getText().toString());
        respiratoryQuestionnaire.setTechnician6(technician6);
        respiratoryQuestionnaire.setEmployee6(employee6);
        ParseQuery<RespiratoryQuestionnaire> query=ParseQuery.getQuery("RespiratoryQuestionnaire");
        query= query.whereEqualTo("user",ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<RespiratoryQuestionnaire>() {
            @Override
            public void done(List<RespiratoryQuestionnaire> objects, ParseException e) {
                if (e != null) {
                    if (objects.size() > 0) {
                        respiratoryQuestionnaire.setObjectId(objects.get(0).getObjectId());

                    }
                    respiratoryQuestionnaire.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            SaveData7Audio();
                        }
                    });
                }
            }
        });

    }
    private void SaveData5() {
        final PhysicalExamination physicalExamination = new PhysicalExamination(ParseUser.getCurrentUser());
        physicalExamination.setId_5_bp(id_5_bp.getText().toString());
        physicalExamination.setId_5_pulse(id_5_pulse.getText().toString());
        physicalExamination.setId_5_weight(id_5_weight.getText().toString());
        physicalExamination.setId_5_bmi(id_5_bmi.getText().toString());
        physicalExamination.setId_5_hgt(id_5_hgt.getText().toString());
        physicalExamination.setId_5_height(id_5_height.getText().toString());
        physicalExamination.setId_5_urine_analysis(id_5_urine_analysis.getText().toString());
        physicalExamination.setId_5_ph(id_5_ph.getText().toString());
        physicalExamination.setId_5_sg(id_5_sg.getText().toString());
        physicalExamination.setId_5_bld(id_5_bld.getText().toString());
        physicalExamination.setId_5_gluc(id_5_gluc.getText().toString());
        physicalExamination.setId_5_leuk(id_5_leuk.getText().toString());
        physicalExamination.setId_5_head(Constants.GetRadioText(this, id_5_head));
        physicalExamination.setId_5_head_comment(id_5_head_comment.getText().toString());
        physicalExamination.setId_5_ear(Constants.GetRadioText(this, id_5_ear));
        physicalExamination.setId_5_ear_comment(id_5_ear_comment.getText().toString());
        physicalExamination.setId_5_lungs(Constants.GetRadioText(this, id_5_lungs));
        physicalExamination.setId_5_lungs_comment(id_5_lungs_comment.getText().toString());
        physicalExamination.setId_5_heart(Constants.GetRadioText(this, id_5_heart));
        physicalExamination.setId_5_heart_comment(id_5_heart_comment.getText().toString());
        physicalExamination.setId_5_vascular(Constants.GetRadioText(this, id_5_vascular));
        physicalExamination.setId_5_vascular_comment(id_5_vascular_comment.getText().toString());
        physicalExamination.setId_5_abdomen(Constants.GetRadioText(this, id_5_abdomen));
        physicalExamination.setId_5_abdomen_comment(id_5_abdomen_comment.getText().toString());
        physicalExamination.setId_5_genito(Constants.GetRadioText(this, id_5_genito));
        physicalExamination.setId_5_genito_comment(id_5_genito_comment.getText().toString());
        physicalExamination.setId_5_neurological(Constants.GetRadioText(this, id_5_neurological));
        physicalExamination.setId_5_neurological_comment(id_5_neurological_comment.getText().toString());
        physicalExamination.setId_5_limbs(Constants.GetRadioText(this, id_5_limbs));
        physicalExamination.setId_5_limbs_comment(id_5_limbs_comment.getText().toString());
        physicalExamination.setId_5_spine(Constants.GetRadioText(this, id_5_spine));
        physicalExamination.setId_5_spine_comment(id_5_spine_comment.getText().toString());
        physicalExamination.setId_5_skin(Constants.GetRadioText(this, id_5_skin));
        physicalExamination.setId_5_skin_comment(id_5_skin_comment.getText().toString());
        physicalExamination.setId_5_psychological(Constants.GetRadioText(this, id_5_psychological));
        physicalExamination.setId_5_psychological_comment(id_5_psychological_comment.getText().toString());
        ParseQuery<PhysicalExamination> query=ParseQuery.getQuery("PhysicalExamination");
        query= query.whereEqualTo("user",ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<PhysicalExamination>() {
            @Override
            public void done(List<PhysicalExamination> objects, ParseException e) {
                if (e != null) {
                    if (objects.size() > 0) {
                        physicalExamination.setObjectId(objects.get(0).getObjectId());

                    }
                    physicalExamination.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            SaveData6();
                        }
                    });
                }
            }
        });

    }
    private void SaveData3() {
        final MedicalHistory medicalHistory = new MedicalHistory(ParseUser.getCurrentUser());
        medicalHistory.setId_3_fam_blindness(Constants.GetRadioText(this, id_3_fam_blindness));
        medicalHistory.setId_3_fam_blood_pressure(Constants.GetRadioText(this, id_3_fam_blood_pressure));
        medicalHistory.setId_3_fam_cancer(Constants.GetRadioText(this, id_3_fam_cancer));
        medicalHistory.setId_3_fam_diabetes(Constants.GetRadioText(this, id_3_fam_diabetes));
        medicalHistory.setId_3_fam_epilepsy(Constants.GetRadioText(this, id_3_fam_epilepsy));
        medicalHistory.setId_3_fam_heart_disease(Constants.GetRadioText(this, id_3_fam_heart_disease));
        medicalHistory.setId_3_fam_hereditary(Constants.GetRadioText(this, id_3_fam_hereditary));
        medicalHistory.setId_3_fam_thyroid(Constants.GetRadioText(this, id_3_fam_thyroid));
        medicalHistory.setId_3_fam_blindness_comment(id_3_fam_blindness_comment.getText().toString());
        medicalHistory.setId_3_fam_blood_pressure_comment(id_3_fam_blood_pressure_comment.getText().toString());
        medicalHistory.setId_3_fam_cancer_comment(id_3_fam_cancer_comment.getText().toString());
        medicalHistory.setId_3_fam_diabetes_comment(id_3_fam_diabetes_comment.getText().toString());
        medicalHistory.setId_3_fam_epilepsy_comment(id_3_fam_epilepsy_comment.getText().toString());
        medicalHistory.setId_3_fam_heart_disease_comment(id_3_fam_heart_disease_comment.getText().toString());
        medicalHistory.setId_3_fam_hereditary_comment(id_3_fam_hereditary_comment.getText().toString());
        medicalHistory.setId_3_fam_thyroid_comment(id_3_fam_thyroid_comment.getText().toString());
        medicalHistory.setId_3_you_heart_disease(Constants.GetRadioText(this, id_3_you_heart_disease));
        medicalHistory.setId_3_you_heart_disease_comment(id_3_you_heart_disease_comment.getText().toString());
        medicalHistory.setId_3_you_blood_pressure(Constants.GetRadioText(this, id_3_you_blood_pressure));
        medicalHistory.setId_3_you_blood_pressure_comment(id_3_you_blood_pressure_comment.getText().toString());
        medicalHistory.setId_3_you_diabetes(Constants.GetRadioText(this, id_3_you_diabetes));
        medicalHistory.setId_3_you_diabetes_comment(id_3_you_diabetes_comment.getText().toString());
        medicalHistory.setId_3_you_epilepsy(Constants.GetRadioText(this, id_3_you_epilepsy));
        medicalHistory.setId_3_you_epilepsy_comment(id_3_you_epilepsy_comment.getText().toString());
        medicalHistory.setId_3_you_cancer(Constants.GetRadioText(this, id_3_you_cancer));
        medicalHistory.setId_3_you_cancer_comment(id_3_you_cancer_comment.getText().toString());
        medicalHistory.setId_3_you_thyroid(Constants.GetRadioText(this, id_3_you_thyroid));
        medicalHistory.setId_3_you_thyroid_comment(id_3_you_thyroid_comment.getText().toString());
        medicalHistory.setId_3_you_drugs_type(id_3_you_drugs_type.getText().toString());
        medicalHistory.setId_3_you_drugs_quantity(id_3_you_drugs_quantity.getText().toString());
        medicalHistory.setId_3_you_smoking(Constants.GetRadioText(this, id_3_you_smoking));
        medicalHistory.setId_3_you_smoking_comment(id_3_you_smoking_comment.getText().toString());
        medicalHistory.setId_3_you_ex_smoker(Constants.GetRadioText(this, id_3_you_ex_smoker));
        medicalHistory.setId_3_you_ex_smoker_comment(id_3_you_ex_smoker_comment.getText().toString());
        medicalHistory.setId_3_you_skin_disorder(Constants.GetRadioText(this, id_3_you_skin_disorder));
        medicalHistory.setId_3_you_skin_disorder_comment(id_3_you_skin_disorder_comment.getText().toString());
        medicalHistory.setId_3_you_admitted_to_hospital(Constants.GetRadioText(this, id_3_you_admitted_to_hospital));
        medicalHistory.setId_3_you_admitted_to_hospital_comment(id_3_you_admitted_to_hospital_comment.getText().toString());
        medicalHistory.setId_3_you_muscles_disease(Constants.GetRadioText(this, id_3_you_muscles_disease));
        medicalHistory.setId_3_you_muscles_disease_comment(id_3_you_muscles_disease_comment.getText().toString());
        medicalHistory.setId_3_you_other_injury(Constants.GetRadioText(this, id_3_you_other_injury));
        medicalHistory.setId_3_you_other_injury_comment(id_3_you_other_injury_comment.getText().toString());
        medicalHistory.setId_3_you_pregnant(Constants.GetRadioText(this, id_3_you_pregnant));
        medicalHistory.setId_3_you_pregnant_comment(id_3_you_pregnant_comment.getText().toString());
        medicalHistory.setId_3_you_gynae(Constants.GetRadioText(this, id_3_you_gynae));
        medicalHistory.setId_3_you_gynae_comment(id_3_you_gynae_comment.getText().toString());
        medicalHistory.setId_3_you_headaches(Constants.GetRadioText(this, id_3_you_headaches));
        medicalHistory.setId_3_you_headaches_comment(id_3_you_headaches_comment.getText().toString());
        medicalHistory.setId_3_you_dizziness(Constants.GetRadioText(this, id_3_you_dizziness));
        medicalHistory.setId_3_you_dizziness_comment(id_3_you_dizziness_comment.getText().toString());
        medicalHistory.setId_3_you_hearing(Constants.GetRadioText(this, id_3_you_hearing));
        medicalHistory.setId_3_you_hearing_comment(id_3_you_hearing_comment.getText().toString());
        medicalHistory.setId_3_you_vision(Constants.GetRadioText(this, id_3_you_vision));
        medicalHistory.setId_3_you_vision_comment(id_3_you_vision_comment.getText().toString());
        medicalHistory.setId_3_you_head_injury(Constants.GetRadioText(this, id_3_you_head_injury));
        medicalHistory.setId_3_you_head_injury_comment(id_3_you_head_injury_comment.getText().toString());
        medicalHistory.setId_3_you_neurological(Constants.GetRadioText(this, id_3_you_neurological));
        medicalHistory.setId_3_you_neurological_comment(id_3_you_neurological_comment.getText().toString());
        medicalHistory.setId_3_you_car_accident(Constants.GetRadioText(this, id_3_you_car_accident));
        medicalHistory.setId_3_you_car_accident_comment(id_3_you_car_accident_comment.getText().toString());
        medicalHistory.setId_3_you_any_allergies(Constants.GetRadioText(this, id_3_you_any_allergies));
        medicalHistory.setId_3_you_any_allergies_comment(id_3_you_any_allergies_comment.getText().toString());
        medicalHistory.setId_3_you_asthma(Constants.GetRadioText(this, id_3_you_asthma));
        medicalHistory.setId_3_you_asthma_comment(id_3_you_asthma_comment.getText().toString());
        medicalHistory.setId_3_you_tb(Constants.GetRadioText(this, id_3_you_tb));
        medicalHistory.setId_3_you_tb_comment(id_3_you_tb_comment.getText().toString());
        medicalHistory.setId_3_you_pneumonia(Constants.GetRadioText(this, id_3_you_pneumonia));
        medicalHistory.setId_3_you_pneumonia_comment(id_3_you_pneumonia_comment.getText().toString());
        medicalHistory.setId_3_you_chest_discomfort(Constants.GetRadioText(this, id_3_you_chest_discomfort));
        medicalHistory.setId_3_you_chest_discomfort_comment(id_3_you_chest_discomfort_comment.getText().toString());
        medicalHistory.setId_3_you_heartburn(Constants.GetRadioText(this, id_3_you_heartburn));
        medicalHistory.setId_3_you_heartburn_comment(id_3_you_heartburn_comment.getText().toString());
        medicalHistory.setId_3_you_ulcer(Constants.GetRadioText(this, id_3_you_ulcer));
        medicalHistory.setId_3_you_ulcer_comment(id_3_you_ulcer_comment.getText().toString());
        medicalHistory.setId_3_you_rectal_bleading(Constants.GetRadioText(this, id_3_you_rectal_bleading));
        medicalHistory.setId_3_you_rectal_bleading_comment(id_3_you_rectal_bleading_comment.getText().toString());
        medicalHistory.setId_3_you_weightloss(Constants.GetRadioText(this, id_3_you_weightloss));
        medicalHistory.setId_3_you_weightloss_comment(id_3_you_weightloss_comment.getText().toString());
        medicalHistory.setId_3_you_depression(Constants.GetRadioText(this, id_3_you_depression));
        medicalHistory.setId_3_you_depression_comment(id_3_you_depression_comment.getText().toString());
        medicalHistory.setId_3_you_height_fear(Constants.GetRadioText(this, id_3_you_height_fear));
        medicalHistory.setId_3_you_height_fear_comment(id_3_you_height_fear_comment.getText().toString());
        medicalHistory.setId_3_you_play_sports(Constants.GetRadioText(this, id_3_you_play_sports));
        medicalHistory.setId_3_you_play_sports_comment(id_3_you_play_sports_comment.getText().toString());
        medicalHistory.setId_3_you_gp_address(Constants.GetRadioText(this, id_3_you_gp_address));
        medicalHistory.setId_3_you_gp_address_comment(id_3_you_gp_address_comment.getText().toString());
        ParseQuery<MedicalHistory> query=ParseQuery.getQuery("MedicalHistory");
        query= query.whereEqualTo("user",ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<MedicalHistory>() {
            @Override
            public void done(List<MedicalHistory> objects, ParseException e) {
                if (e != null) {
                    if (objects.size() > 0) {
                        medicalHistory.setObjectId(objects.get(0).getObjectId());

                    }
                    medicalHistory.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            SaveData5();
                        }
                    });
                }
            }
        });

    }
    private void SaveData2() {
        final OccupationalHealthRecord occupationalHealthRecord=new OccupationalHealthRecord(ParseUser.getCurrentUser());
        occupationalHealthRecord.setId_2_years4(id_2_years4.getText().toString());
        occupationalHealthRecord.setId_2_years3(id_2_years3.getText().toString());
        occupationalHealthRecord.setId_2_years2(id_2_years2.getText().toString());
        occupationalHealthRecord.setId_2_years1(id_2_years1.getText().toString());
        occupationalHealthRecord.setId_2_town(id_2_town.getText().toString());
        occupationalHealthRecord.setId_2_company(id_2_company.getText().toString());
        occupationalHealthRecord.setId_2_contact(id_2_contact.getText().toString());
        occupationalHealthRecord.setId_2_company1(id_2_company1.getText().toString());
        occupationalHealthRecord.setId_2_company2(id_2_company2.getText().toString());
        occupationalHealthRecord.setId_2_company3(id_2_company3.getText().toString());
        occupationalHealthRecord.setId_2_company4(id_2_company4.getText().toString());
        occupationalHealthRecord.setId_2_consent(id_2_consent.getText().toString());
        occupationalHealthRecord.setId_2_contact_office(id_2_contact_office.getText().toString());
        occupationalHealthRecord.setId_2_contractbase(id_2_contractbase.getText().toString());
        occupationalHealthRecord.setId_2_datesign(id_2_datesign.getText().toString());
        occupationalHealthRecord.setId_2_department(id_2_department.getText().toString());
        occupationalHealthRecord.setId_2_employee_no(id_2_employee_no.getText().toString());
        occupationalHealthRecord.setId_2_gender(id_2_gender.getText().toString());
        occupationalHealthRecord.setId_2_hazard1(id_2_hazard1.getText().toString());
        occupationalHealthRecord.setId_2_hazard2(id_2_hazard2.getText().toString());
        occupationalHealthRecord.setId_2_hazard3(id_2_hazard3.getText().toString());
        occupationalHealthRecord.setId_2_hazard4(id_2_hazard4.getText().toString());
        occupationalHealthRecord.setId_2_home_address(id_2_home_address.getText().toString());
        occupationalHealthRecord.setId_2_id_number(id_2_id_number.getText().toString());
        occupationalHealthRecord.setId_2_job_title(id_2_job_title.getText().toString());
        occupationalHealthRecord.setId_2_jobtitle1(id_2_jobtitle1.getText().toString());
        occupationalHealthRecord.setId_2_jobtitle2(id_2_jobtitle2.getText().toString());
        occupationalHealthRecord.setId_2_jobtitle3(id_2_jobtitle3.getText().toString());
        occupationalHealthRecord.setId_2_jobtitle4(id_2_jobtitle4.getText().toString());
        occupationalHealthRecord.setId_2_marital_status(id_2_marital_status.getText().toString());
        occupationalHealthRecord.setId_2_medical(id_2_medical.getText().toString());
        occupationalHealthRecord.setId_2_name_surname(id_2_name_surname.getText().toString());
        occupationalHealthRecord.setId_2_province(id_2_province.getText().toString());
        occupationalHealthRecord.setId_2_shift_worker(id_2_shift_worker.getText().toString());
        occupationalHealthRecord.setId_2_starting_date(id_2_starting_date.getText().toString());
        occupationalHealthRecord.setId_2_supervisor(id_2_supervisor.getText().toString());
        occupationalHealthRecord.setEmployee2(employee2);
        ParseQuery<OccupationalHealthRecord> query=ParseQuery.getQuery("OccupationalHealthRecord");
        query= query.whereEqualTo("user",ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<OccupationalHealthRecord>() {
            @Override
            public void done(List<OccupationalHealthRecord> objects, ParseException e) {
                if (e != null) {
                    if (objects.size() > 0) {
                        occupationalHealthRecord.setObjectId(objects.get(0).getObjectId());
                    }
                    occupationalHealthRecord.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            SaveData3();
                        }
                    });
                }
            }
        });
    }
    private void SaveData1(){
        final MedicalFitnessCertificate medicalFitnessCertificate=new MedicalFitnessCertificate(ParseUser.getCurrentUser());
        medicalFitnessCertificate.setId_1_company_name(id_1_company_name.getText().toString());
        medicalFitnessCertificate.setId_1_acknowledged_name(id_1_acknowledged_name.getText().toString());
        medicalFitnessCertificate.setId_1_address(id_1_address.getText().toString());
        medicalFitnessCertificate.setId_1_date_expiry(id_1_date_expiry.getText().toString());
        medicalFitnessCertificate.setId_1_datesign(id_1_datesign.getText().toString());
        medicalFitnessCertificate.setId_1_datesign_omp(id_1_datesign_omp.getText().toString());
        medicalFitnessCertificate.setId_1_department(id_1_department.getText().toString());
        medicalFitnessCertificate.setId_1_employee_number(id_1_employee_number.getText().toString());
        medicalFitnessCertificate.setId_1_exam_type_defined(id_1_exam_type_defined.getText().toString());
        medicalFitnessCertificate.setId_1_id_number(id_1_id_number.getText().toString());
        medicalFitnessCertificate.setId_1_job_title(id_1_job_title.getText().toString());
        medicalFitnessCertificate.setId_1_name(id_1_name.getText().toString());
        medicalFitnessCertificate.setId_1_name_surname(id_1_name_surname.getText().toString());
        medicalFitnessCertificate.setId_1_sur_name(id_1_sur_name.getText().toString());
        medicalFitnessCertificate.setId_1_town(id_1_town.getText().toString());
        medicalFitnessCertificate.setId_1_region(id_1_region.getText().toString());
        medicalFitnessCertificate.setId_1_exam_type(Constants.GetRadioText(this, id_1_exam_type));
        medicalFitnessCertificate.setId_1_person_record(Constants.GetRadioText(this, id_1_person_record));
        medicalFitnessCertificate.setEmployee1(employee1);
        medicalFitnessCertificate.setExaminer1(examiner1);
        medicalFitnessCertificate.setOmp1(omp1);

        ParseQuery<MedicalFitnessCertificate> query=ParseQuery.getQuery("MedicalFitnessCertificate");
        query= query.whereEqualTo("user",ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<MedicalFitnessCertificate>() {
            @Override
            public void done(List<MedicalFitnessCertificate> objects, ParseException e) {
                if (e != null) {
                    if (objects.size() > 0) {
                        medicalFitnessCertificate.setObjectId(objects.get(0).getObjectId());
                    }
                    medicalFitnessCertificate.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            SaveData2();
                        }
                    });
                }
            }
        });


    }*/

    private void Initialize1Screen() {
        id_1_save=(AppCompatButton)startView.findViewById(R.id.id_1_save);
        id_1_save.setOnClickListener(this);

        id_1_company_name=(AppCompatEditText)startView.findViewById(R.id.id_1_company_name);
        id_1_name=(AppCompatEditText)startView.findViewById(R.id.id_1_name);
        id_1_sur_name=(AppCompatEditText)startView.findViewById(R.id.id_1_sur_name);
        id_1_id_number=(AppCompatEditText)startView.findViewById(R.id.id_1_id_number);
        id_1_employee_number=(AppCompatEditText)startView.findViewById(R.id.id_1_employee_number);
        id_1_job_title=(AppCompatEditText)startView.findViewById(R.id.id_1_job_title);
        id_1_department=(AppCompatEditText)startView.findViewById(R.id.id_1_department);
        id_1_address=(AppCompatEditText)startView.findViewById(R.id.id_1_address);
        id_1_town=(AppCompatEditText)startView.findViewById(R.id.id_1_town);
        id_1_region=(AppCompatEditText)startView.findViewById(R.id.id_1_region);
        id_1_exam_type_defined=(AppCompatEditText)startView.findViewById(R.id.id_1_exam_type_defined);
        id_1_acknowledged_name=(AppCompatEditText)startView.findViewById(R.id.id_1_acknowledged_name);
        id_1_datesign=(AppCompatEditText)startView.findViewById(R.id.id_1_datesign);
        id_1_name_surname=(AppCompatEditText)startView.findViewById(R.id.id_1_name_surname);
        id_1_datesign_omp=(AppCompatEditText)startView.findViewById(R.id.id_1_datesign_omp);
        id_1_date_expiry=(AppCompatEditText)startView.findViewById(R.id.id_1_date_expiry);
        id_1_exam_type=(RadioGroup)startView.findViewById(R.id.id_1_exam_type);
        id_1_person_record=(RadioGroup)startView.findViewById(R.id.id_1_person_record);

        id_1_employee_button=(AppCompatButton)startView.findViewById(R.id.id_1_employee_button);
        id_1_examiner_button=(AppCompatButton)startView.findViewById(R.id.id_1_examiner_button);
        id_1_omp_button=(AppCompatButton)startView.findViewById(R.id.id_1_omp_button);
        id_1_employee_button.setOnClickListener(this);
        id_1_examiner_button.setOnClickListener(this);
        id_1_omp_button.setOnClickListener(this);
        id_1_employee_image=(AppCompatImageView)startView.findViewById(R.id.id_1_employee_image);
        id_1_examiner_image=(AppCompatImageView)startView.findViewById(R.id.id_1_examiner_image);
        id_1_omp_image=(AppCompatImageView)startView.findViewById(R.id.id_1_omp_image);
    }
    private void Initialize2Screen() {
        id_2_save=(AppCompatButton)startView.findViewById(R.id.id_2_save);
        id_2_save.setOnClickListener(this);
        id_2_employee_button=(AppCompatButton)startView.findViewById(R.id.id_2_employee_button);
        id_2_employee_button.setOnClickListener(this);
        id_2_employee_image=(AppCompatImageView)startView.findViewById(R.id.id_2_employee_image);

        id_2_name_surname=(AppCompatEditText)startView.findViewById(R.id.id_2_name_surname);
        id_2_id_number=(AppCompatEditText)startView.findViewById(R.id.id_2_id_number);
        id_2_gender=(AppCompatEditText)startView.findViewById(R.id.id_2_gender);
        id_2_marital_status=(AppCompatEditText)startView.findViewById(R.id.id_2_marital_status);
        id_2_home_address=(AppCompatEditText)startView.findViewById(R.id.id_2_home_address);
        id_2_town=(AppCompatEditText)startView.findViewById(R.id.id_2_town);
        id_2_province=(AppCompatEditText)startView.findViewById(R.id.id_2_province);
        id_2_contact=(AppCompatEditText)startView.findViewById(R.id.id_2_contact);
        id_2_medical=(AppCompatEditText)startView.findViewById(R.id.id_2_medical);
        id_2_company=(AppCompatEditText)startView.findViewById(R.id.id_2_company);
        id_2_job_title=(AppCompatEditText)startView.findViewById(R.id.id_2_job_title);
        id_2_employee_no=(AppCompatEditText)startView.findViewById(R.id.id_2_employee_no);
        id_2_contractbase=(AppCompatEditText)startView.findViewById(R.id.id_2_contractbase);
        id_2_shift_worker=(AppCompatEditText)startView.findViewById(R.id.id_2_shift_worker);
        id_2_starting_date=(AppCompatEditText)startView.findViewById(R.id.id_2_starting_date);
        id_2_department=(AppCompatEditText)startView.findViewById(R.id.id_2_department);
        id_2_supervisor=(AppCompatEditText)startView.findViewById(R.id.id_2_supervisor);
        id_2_contact_office=(AppCompatEditText)startView.findViewById(R.id.id_2_contact_office);
        id_2_company1=(AppCompatEditText)startView.findViewById(R.id.id_2_company1);
        id_2_years1=(AppCompatEditText)startView.findViewById(R.id.id_2_years1);
        id_2_jobtitle1=(AppCompatEditText)startView.findViewById(R.id.id_2_jobtitle1);
        id_2_hazard1=(AppCompatEditText)startView.findViewById(R.id.id_2_hazard1);
        id_2_company2=(AppCompatEditText)startView.findViewById(R.id.id_2_company2);
        id_2_years2=(AppCompatEditText)startView.findViewById(R.id.id_2_years2);
        id_2_jobtitle2=(AppCompatEditText)startView.findViewById(R.id.id_2_jobtitle2);
        id_2_hazard2=(AppCompatEditText)startView.findViewById(R.id.id_2_hazard2);
        id_2_company3=(AppCompatEditText)startView.findViewById(R.id.id_2_company3);
        id_2_years3=(AppCompatEditText)startView.findViewById(R.id.id_2_years3);
        id_2_jobtitle3=(AppCompatEditText)startView.findViewById(R.id.id_2_jobtitle3);
        id_2_hazard3=(AppCompatEditText)startView.findViewById(R.id.id_2_hazard3);
        id_2_company4=(AppCompatEditText)startView.findViewById(R.id.id_2_company4);
        id_2_years4=(AppCompatEditText)startView.findViewById(R.id.id_2_years4);
        id_2_jobtitle4=(AppCompatEditText)startView.findViewById(R.id.id_2_jobtitle4);
        id_2_hazard4=(AppCompatEditText)startView.findViewById(R.id.id_2_hazard4);
        id_2_consent=(AppCompatEditText)startView.findViewById(R.id.id_2_consent);
        id_2_datesign=(AppCompatEditText)startView.findViewById(R.id.id_2_datesign);
    }
    private void Initialize3Screen() {
        id_3_save=(AppCompatButton)startView.findViewById(R.id.id_3_save);
        id_3_save.setOnClickListener(this);

        id_3_fam_heart_disease=(RadioGroup)startView.findViewById(R.id.id_3_fam_heart_disease);
        id_3_fam_blood_pressure=(RadioGroup)startView.findViewById(R.id.id_3_fam_blood_pressure);
        id_3_fam_diabetes=(RadioGroup)startView.findViewById(R.id.id_3_fam_diabetes);
        id_3_fam_epilepsy=(RadioGroup)startView.findViewById(R.id.id_3_fam_epilepsy);
        id_3_fam_cancer=(RadioGroup)startView.findViewById(R.id.id_3_fam_cancer);
        id_3_fam_thyroid=(RadioGroup)startView.findViewById(R.id.id_3_fam_thyroid);
        id_3_fam_hereditary=(RadioGroup)startView.findViewById(R.id.id_3_fam_hereditary);
        id_3_fam_blindness=(RadioGroup)startView.findViewById(R.id.id_3_fam_blindness);
        id_3_fam_heart_disease_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_fam_heart_disease_comment);
        id_3_fam_blood_pressure_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_fam_blood_pressure_comment);
        id_3_fam_diabetes_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_fam_diabetes_comment);
        id_3_fam_epilepsy_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_fam_epilepsy_comment);
        id_3_fam_cancer_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_fam_cancer_comment);
        id_3_fam_thyroid_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_fam_thyroid_comment);
        id_3_fam_hereditary_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_fam_hereditary_comment);
        id_3_fam_blindness_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_fam_blindness_comment);

        id_3_you_heart_disease=(RadioGroup)startView.findViewById(R.id.id_3_you_heart_disease);
        id_3_you_blood_pressure=(RadioGroup)startView.findViewById(R.id.id_3_you_blood_pressure);
        id_3_you_diabetes=(RadioGroup)startView.findViewById(R.id.id_3_you_diabetes);
        id_3_you_epilepsy=(RadioGroup)startView.findViewById(R.id.id_3_you_epilepsy);
        id_3_you_cancer=(RadioGroup)startView.findViewById(R.id.id_3_you_cancer);
        id_3_you_thyroid=(RadioGroup)startView.findViewById(R.id.id_3_you_thyroid);
        id_3_you_smoking=(RadioGroup)startView.findViewById(R.id.id_3_you_smoking);
        id_3_you_ex_smoker=(RadioGroup)startView.findViewById(R.id.id_3_you_ex_smoker);
        id_3_you_skin_disorder=(RadioGroup)startView.findViewById(R.id.id_3_you_skin_disorder);
        id_3_you_admitted_to_hospital=(RadioGroup)startView.findViewById(R.id.id_3_you_admitted_to_hospital);
        id_3_you_muscles_disease=(RadioGroup)startView.findViewById(R.id.id_3_you_muscles_disease);
        id_3_you_other_injury=(RadioGroup)startView.findViewById(R.id.id_3_you_other_injury);
        id_3_you_pregnant=(RadioGroup)startView.findViewById(R.id.id_3_you_pregnant);
        id_3_you_gynae=(RadioGroup)startView.findViewById(R.id.id_3_you_gynae);
        id_3_you_headaches=(RadioGroup)startView.findViewById(R.id.id_3_you_headaches);
        id_3_you_dizziness=(RadioGroup)startView.findViewById(R.id.id_3_you_dizziness);
        id_3_you_hearing=(RadioGroup)startView.findViewById(R.id.id_3_you_hearing);
        id_3_you_vision=(RadioGroup)startView.findViewById(R.id.id_3_you_vision);
        id_3_you_head_injury=(RadioGroup)startView.findViewById(R.id.id_3_you_head_injury);
        id_3_you_neurological=(RadioGroup)startView.findViewById(R.id.id_3_you_neurological);
        id_3_you_car_accident=(RadioGroup)startView.findViewById(R.id.id_3_you_car_accident);
        id_3_you_any_allergies=(RadioGroup)startView.findViewById(R.id.id_3_you_any_allergies);
        id_3_you_asthma=(RadioGroup)startView.findViewById(R.id.id_3_you_asthma);
        id_3_you_tb=(RadioGroup)startView.findViewById(R.id.id_3_you_tb);
        id_3_you_pneumonia=(RadioGroup)startView.findViewById(R.id.id_3_you_pneumonia);
        id_3_you_chest_discomfort=(RadioGroup)startView.findViewById(R.id.id_3_you_chest_discomfort);
        id_3_you_heartburn=(RadioGroup)startView.findViewById(R.id.id_3_you_heartburn);
        id_3_you_ulcer=(RadioGroup)startView.findViewById(R.id.id_3_you_ulcer);
        id_3_you_rectal_bleading=(RadioGroup)startView.findViewById(R.id.id_3_you_rectal_bleading);
        id_3_you_weightloss=(RadioGroup)startView.findViewById(R.id.id_3_you_weightloss);
        id_3_you_depression=(RadioGroup)startView.findViewById(R.id.id_3_you_depression);
        id_3_you_height_fear=(RadioGroup)startView.findViewById(R.id.id_3_you_height_fear);
        id_3_you_play_sports=(RadioGroup)startView.findViewById(R.id.id_3_you_play_sports);
        id_3_you_gp_address=(RadioGroup)startView.findViewById(R.id.id_3_you_gp_address);
        id_3_you_drugs_type=(AppCompatEditText)startView.findViewById(R.id.id_3_you_drugs_type);
        id_3_you_drugs_quantity=(AppCompatEditText)startView.findViewById(R.id.id_3_you_drugs_quantity);
        id_3_you_heart_disease_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_heart_disease_comment);
        id_3_you_blood_pressure_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_blood_pressure_comment);
        id_3_you_diabetes_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_diabetes_comment);
        id_3_you_epilepsy_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_epilepsy_comment);
        id_3_you_cancer_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_cancer_comment);
        id_3_you_thyroid_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_thyroid_comment);
        id_3_you_smoking_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_smoking_comment);
        id_3_you_ex_smoker_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_ex_smoker_comment);
        id_3_you_skin_disorder_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_skin_disorder_comment);
        id_3_you_admitted_to_hospital_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_admitted_to_hospital_comment);
        id_3_you_other_injury_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_other_injury_comment);
        id_3_you_muscles_disease_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_muscles_disease_comment);
        id_3_you_pregnant_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_pregnant_comment);
        id_3_you_gynae_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_gynae_comment);
        id_3_you_headaches_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_headaches_comment);
        id_3_you_dizziness_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_dizziness_comment);
        id_3_you_hearing_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_hearing_comment);
        id_3_you_vision_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_vision_comment);
        id_3_you_head_injury_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_head_injury_comment);
        id_3_you_neurological_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_neurological_comment);
        id_3_you_car_accident_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_car_accident_comment);
        id_3_you_any_allergies_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_any_allergies_comment);
        id_3_you_asthma_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_asthma_comment);
        id_3_you_tb_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_tb_comment);
        id_3_you_pneumonia_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_pneumonia_comment);
        id_3_you_chest_discomfort_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_chest_discomfort_comment);
        id_3_you_heartburn_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_heartburn_comment);
        id_3_you_ulcer_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_ulcer_comment);
        id_3_you_rectal_bleading_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_rectal_bleading_comment);
        id_3_you_weightloss_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_weightloss_comment);
        id_3_you_depression_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_depression_comment);
        id_3_you_height_fear_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_height_fear_comment);
        id_3_you_play_sports_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_play_sports_comment);
        id_3_you_gp_address_comment=(AppCompatEditText)startView.findViewById(R.id.id_3_you_gp_address_comment);
    }
    private void Initialize5Screen() {
        id_5_save=(AppCompatButton)startView.findViewById(R.id.id_5_save);
        id_5_save.setOnClickListener(this);

        id_5_bp=(AppCompatEditText)startView.findViewById(R.id.id_5_bp);
        id_5_pulse=(AppCompatEditText)startView.findViewById(R.id.id_5_pulse);
        id_5_weight=(AppCompatEditText)startView.findViewById(R.id.id_5_weight);
        id_5_bmi=(AppCompatEditText)startView.findViewById(R.id.id_5_bmi);
        id_5_hgt=(AppCompatEditText)startView.findViewById(R.id.id_5_hgt);
        id_5_height=(AppCompatEditText)startView.findViewById(R.id.id_5_height);
        id_5_urine_analysis=(AppCompatEditText)startView.findViewById(R.id.id_5_urine_analysis);
        id_5_ph=(AppCompatEditText)startView.findViewById(R.id.id_5_ph);
        id_5_sg=(AppCompatEditText)startView.findViewById(R.id.id_5_sg);
        id_5_bld=(AppCompatEditText)startView.findViewById(R.id.id_5_bld);
        id_5_gluc=(AppCompatEditText)startView.findViewById(R.id.id_5_gluc);
        id_5_leuk=(AppCompatEditText)startView.findViewById(R.id.id_5_leuk);
        id_5_head_comment=(AppCompatEditText)startView.findViewById(R.id.id_5_head_comment);
        id_5_ear_comment=(AppCompatEditText)startView.findViewById(R.id.id_5_ear_comment);
        id_5_lungs_comment=(AppCompatEditText)startView.findViewById(R.id.id_5_lungs_comment);
        id_5_heart_comment=(AppCompatEditText)startView.findViewById(R.id.id_5_heart_comment);
        id_5_vascular_comment=(AppCompatEditText)startView.findViewById(R.id.id_5_vascular_comment);
        id_5_abdomen_comment=(AppCompatEditText)startView.findViewById(R.id.id_5_abdomen_comment);
        id_5_genito_comment=(AppCompatEditText)startView.findViewById(R.id.id_5_genito_comment);
        id_5_neurological_comment=(AppCompatEditText)startView.findViewById(R.id.id_5_neurological_comment);
        id_5_limbs_comment=(AppCompatEditText)startView.findViewById(R.id.id_5_limbs_comment);
        id_5_spine_comment=(AppCompatEditText)startView.findViewById(R.id.id_5_spine_comment);
        id_5_skin_comment=(AppCompatEditText)startView.findViewById(R.id.id_5_skin_comment);
        id_5_psychological_comment=(AppCompatEditText)startView.findViewById(R.id.id_5_psychological_comment);

        id_5_head=(RadioGroup)startView.findViewById(R.id.id_5_head);
        id_5_ear=(RadioGroup)startView.findViewById(R.id.id_5_ear);
        id_5_lungs=(RadioGroup)startView.findViewById(R.id.id_5_lungs);
        id_5_heart=(RadioGroup)startView.findViewById(R.id.id_5_heart);
        id_5_vascular=(RadioGroup)startView.findViewById(R.id.id_5_vascular);
        id_5_abdomen=(RadioGroup)startView.findViewById(R.id.id_5_abdomen);
        id_5_genito=(RadioGroup)startView.findViewById(R.id.id_5_genito);
        id_5_neurological=(RadioGroup)startView.findViewById(R.id.id_5_neurological);
        id_5_limbs=(RadioGroup)startView.findViewById(R.id.id_5_limbs);
        id_5_spine=(RadioGroup)startView.findViewById(R.id.id_5_spine);
        id_5_skin=(RadioGroup)startView.findViewById(R.id.id_5_skin);
        id_5_psychological=(RadioGroup)startView.findViewById(R.id.id_5_psychological);
    }
    private void Initialize6Screen() {
        id_6_name=(AppCompatEditText)startView.findViewById(R.id.id_6_name);
        id_6_dob=(AppCompatEditText)startView.findViewById(R.id.id_6_dob);
        id_6_jobTitle=(AppCompatEditText)startView.findViewById(R.id.id_6_jobTitle);
        id_6_weight=(AppCompatEditText)startView.findViewById(R.id.id_6_weight);
        id_6_height=(AppCompatEditText)startView.findViewById(R.id.id_6_height);
        id_6_department=(AppCompatEditText)startView.findViewById(R.id.id_6_department);
        id_6_year_exposed=(AppCompatEditText)startView.findViewById(R.id.id_6_year_exposed);
        id_6_company_name=(AppCompatEditText)startView.findViewById(R.id.id_6_company_name);
        id_6_job_title=(AppCompatEditText)startView.findViewById(R.id.id_6_job_title);
        id_6_hazards_exposed_to=(AppCompatEditText)startView.findViewById(R.id.id_6_hazards_exposed_to);
        id_6_chest_problem_comment=(AppCompatEditText)startView.findViewById(R.id.id_6_chest_problem_comment);
        id_6_lungcondition_comment=(AppCompatEditText)startView.findViewById(R.id.id_6_lungcondition_comment);
        id_6_tb_comment=(AppCompatEditText)startView.findViewById(R.id.id_6_tb_comment);
        id_6_cold_comment=(AppCompatEditText)startView.findViewById(R.id.id_6_cold_comment);
        id_6_smoking_comment=(AppCompatEditText)startView.findViewById(R.id.id_6_smoking_comment);
        id_6_ex_smoker_comment=(AppCompatEditText)startView.findViewById(R.id.id_6_ex_smoker_comment);
        id_6_xray_comment=(AppCompatEditText)startView.findViewById(R.id.id_6_xray_comment);
        id_6_ppe_comment=(AppCompatEditText)startView.findViewById(R.id.id_6_ppe_comment);
        id_6_on_treatment_comment=(AppCompatEditText)startView.findViewById(R.id.id_6_on_treatment_comment);

        id_6_chest_problem=(RadioGroup)startView.findViewById(R.id.id_6_chest_problem);
        id_6_lungcondition=(RadioGroup)startView.findViewById(R.id.id_6_lungcondition);
        id_6_tb=(RadioGroup)startView.findViewById(R.id.id_6_tb);
        id_6_cold=(RadioGroup)startView.findViewById(R.id.id_6_cold);
        id_6_smoking=(RadioGroup)startView.findViewById(R.id.id_6_smoking);
        id_6_ex_smoker=(RadioGroup)startView.findViewById(R.id.id_6_ex_smoker);
        id_6_xray=(RadioGroup)startView.findViewById(R.id.id_6_xray);
        id_6_ppe=(RadioGroup)startView.findViewById(R.id.id_6_ppe);
        id_6_on_treatment=(RadioGroup)startView.findViewById(R.id.id_6_on_treatment);




        id_6_employeesign_button=(AppCompatButton)startView.findViewById(R.id.id_6_employeesign_button);
        id_6_techniciansign_button=(AppCompatButton)startView.findViewById(R.id.id_6_techniciansign_button);

        id_6_employeesign_button.setOnClickListener(this);
        id_6_techniciansign_button.setOnClickListener(this);

        id_6_employeesign_image=(AppCompatImageView)startView.findViewById(R.id.id_6_employeesign_image);
        id_6_techniciansign_image=(AppCompatImageView)startView.findViewById(R.id.id_6_techniciansign_image);

        id_6_save=(AppCompatButton)startView.findViewById(R.id.id_6_save);
        id_6_save.setOnClickListener(this);
    }
    private void Initialize7Screen(){
        id_7_audio_cold1=(RadioGroup)startView.findViewById(R.id.id_7_audio_cold1);
        id_7_audio_noise1=(RadioGroup)startView.findViewById(R.id.id_7_audio_noise1);
        id_7_audio_hobbies1=(RadioGroup)startView.findViewById(R.id.id_7_audio_hobbies1);
        id_7_audio_hearing1=(RadioGroup)startView.findViewById(R.id.id_7_audio_hearing1);
        id_7_audio_trauma1=(RadioGroup)startView.findViewById(R.id.id_7_audio_trauma1);
        id_7_audio_tb1=(RadioGroup)startView.findViewById(R.id.id_7_audio_tb1);
        id_7_audio_ears1=(RadioGroup)startView.findViewById(R.id.id_7_audio_ears1);
        id_7_audio_military1=(RadioGroup)startView.findViewById(R.id.id_7_audio_military1);
        id_7_audio_ppe1=(RadioGroup)startView.findViewById(R.id.id_7_audio_ppe1);
        id_7_audio_meds1=(RadioGroup)startView.findViewById(R.id.id_7_audio_meds1);
        id_7_audio_cold2=(RadioGroup)startView.findViewById(R.id.id_7_audio_cold2);
        id_7_audio_noise2=(RadioGroup)startView.findViewById(R.id.id_7_audio_noise2);
        id_7_audio_hobbies2=(RadioGroup)startView.findViewById(R.id.id_7_audio_hobbies2);
        id_7_audio_hearing2=(RadioGroup)startView.findViewById(R.id.id_7_audio_hearing2);
        id_7_audio_trauma2=(RadioGroup)startView.findViewById(R.id.id_7_audio_trauma2);
        id_7_audio_tb2=(RadioGroup)startView.findViewById(R.id.id_7_audio_tb2);
        id_7_audio_ears2=(RadioGroup)startView.findViewById(R.id.id_7_audio_ears2);
        id_7_audio_military2=(RadioGroup)startView.findViewById(R.id.id_7_audio_military2);
        id_7_audio_ppe2=(RadioGroup)startView.findViewById(R.id.id_7_audio_ppe2);
        id_7_audio_meds2=(RadioGroup)startView.findViewById(R.id.id_7_audio_meds2);
        id_7_audio_cold3=(RadioGroup)startView.findViewById(R.id.id_7_audio_cold3);
        id_7_audio_noise3=(RadioGroup)startView.findViewById(R.id.id_7_audio_noise3);
        id_7_audio_hobbies3=(RadioGroup)startView.findViewById(R.id.id_7_audio_hobbies3);
        id_7_audio_hearing3=(RadioGroup)startView.findViewById(R.id.id_7_audio_hearing3);
        id_7_audio_trauma3=(RadioGroup)startView.findViewById(R.id.id_7_audio_trauma3);
        id_7_audio_tb3=(RadioGroup)startView.findViewById(R.id.id_7_audio_tb3);
        id_7_audio_ears3=(RadioGroup)startView.findViewById(R.id.id_7_audio_ears3);
        id_7_audio_military3=(RadioGroup)startView.findViewById(R.id.id_7_audio_military3);
        id_7_audio_ppe3=(RadioGroup)startView.findViewById(R.id.id_7_audio_ppe3);
        id_7_audio_meds3=(RadioGroup)startView.findViewById(R.id.id_7_audio_meds3);
        id_7_audio_date1=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_date1);
        id_7_audio_cold_comments1=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_cold_comments1);
        id_7_audio_noise_comments1=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_noise_comments1);
        id_7_audio_hobbies_comments1=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_hobbies_comments1);
        id_7_audio_hearing_comments1=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_hearing_comments1);
        id_7_audio_trauma_comments1=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_trauma_comments1);
        id_7_audio_tb_comments1=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_tb_comments1);
        id_7_audio_ears_comments1=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_ears_comments1);
        id_7_audio_military_comments1=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_military_comments1);
        id_7_audio_ppe_comments1=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_ppe_comments1);
        id_7_audio_meds_comments1=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_meds_comments1);
        id_7_audio_date2=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_date2);
        id_7_audio_cold_comments2=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_cold_comments2);
        id_7_audio_noise_comments2=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_noise_comments2);
        id_7_audio_hobbies_comments2=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_hobbies_comments2);
        id_7_audio_hearing_comments2=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_hearing_comments2);
        id_7_audio_trauma_comments2=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_trauma_comments2);
        id_7_audio_tb_comments2=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_tb_comments2);
        id_7_audio_ears_comments2=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_ears_comments2);
        id_7_audio_military_comments2=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_military_comments2);
        id_7_audio_ppe_comments2=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_ppe_comments2);
        id_7_audio_meds_comments2=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_meds_comments2);
        id_7_audio_date3=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_date3);
        id_7_audio_cold_comments3=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_cold_comments3);
        id_7_audio_noise_comments3=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_noise_comments3);
        id_7_audio_hobbies_comments3=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_hobbies_comments3);
        id_7_audio_hearing_comments3=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_hearing_comments3);
        id_7_audio_trauma_comments3=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_trauma_comments3);
        id_7_audio_tb_comments3=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_tb_comments3);
        id_7_audio_ears_comments3=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_ears_comments3);
        id_7_audio_military_comments3=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_military_comments3);
        id_7_audio_ppe_comments3=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_ppe_comments3);
        id_7_audio_meds_comments3=(AppCompatEditText)startView.findViewById(R.id.id_7_audio_meds_comments3);


        id_7_oto_date1=(AppCompatEditText)startView.findViewById(R.id.id_7_oto_date1);
        id_7_oto_date2=(AppCompatEditText)startView.findViewById(R.id.id_7_oto_date2);
        id_7_oto_date3=(AppCompatEditText)startView.findViewById(R.id.id_7_oto_date3);
        id_7_oto_date4=(AppCompatEditText)startView.findViewById(R.id.id_7_oto_date4);
        id_7_oto_perforation1=(RadioGroup)startView.findViewById(R.id.id_7_oto_perforation1);
        id_7_oto_malfunction1=(RadioGroup)startView.findViewById(R.id.id_7_oto_malfunction1);
        id_7_oto_otitis1=(RadioGroup)startView.findViewById(R.id.id_7_oto_otitis1);
        id_7_oto_wax1=(RadioGroup)startView.findViewById(R.id.id_7_oto_wax1);
        id_7_oto_treatment1=(RadioGroup)startView.findViewById(R.id.id_7_oto_treatment1);
        id_7_oto_perforation2=(RadioGroup)startView.findViewById(R.id.id_7_oto_perforation2);
        id_7_oto_malfunction2=(RadioGroup)startView.findViewById(R.id.id_7_oto_malfunction2);
        id_7_oto_otitis2=(RadioGroup)startView.findViewById(R.id.id_7_oto_otitis2);
        id_7_oto_wax2=(RadioGroup)startView.findViewById(R.id.id_7_oto_wax2);
        id_7_oto_treatment2=(RadioGroup)startView.findViewById(R.id.id_7_oto_treatment2);
        id_7_oto_perforation3=(RadioGroup)startView.findViewById(R.id.id_7_oto_perforation3);
        id_7_oto_malfunction3=(RadioGroup)startView.findViewById(R.id.id_7_oto_malfunction3);
        id_7_oto_otitis3=(RadioGroup)startView.findViewById(R.id.id_7_oto_otitis3);
        id_7_oto_wax3=(RadioGroup)startView.findViewById(R.id.id_7_oto_wax3);
        id_7_oto_treatment3=(RadioGroup)startView.findViewById(R.id.id_7_oto_treatment3);
        id_7_oto_perforation4=(RadioGroup)startView.findViewById(R.id.id_7_oto_perforation4);
        id_7_oto_malfunction4=(RadioGroup)startView.findViewById(R.id.id_7_oto_malfunction4);
        id_7_oto_otitis4=(RadioGroup)startView.findViewById(R.id.id_7_oto_otitis4);
        id_7_oto_wax4=(RadioGroup)startView.findViewById(R.id.id_7_oto_wax4);
        id_7_oto_treatment4=(RadioGroup)startView.findViewById(R.id.id_7_oto_treatment4);

        id_7_oto_sign1=(AppCompatButton)startView.findViewById(R.id.id_7_oto_sign1);
        id_7_oto_sign2=(AppCompatButton)startView.findViewById(R.id.id_7_oto_sign2);
        id_7_oto_sign3=(AppCompatButton)startView.findViewById(R.id.id_7_oto_sign3);
        id_7_oto_sign4=(AppCompatButton)startView.findViewById(R.id.id_7_oto_sign4);
        id_7_save=(AppCompatButton)startView.findViewById(R.id.id_7_save);
        id_7_oto_save=(AppCompatButton)startView.findViewById(R.id.id_7_oto_save);
        id_7_oto_sign1.setOnClickListener(this);
        id_7_oto_sign2.setOnClickListener(this);
        id_7_oto_sign3.setOnClickListener(this);
        id_7_oto_sign4.setOnClickListener(this);
        id_7_save.setOnClickListener(this);
        id_7_oto_save.setOnClickListener(this);
        id_7_oto_sign_image1=(AppCompatImageView)startView.findViewById(R.id.id_7_oto_sign_image1);
        id_7_oto_sign_image2=(AppCompatImageView)startView.findViewById(R.id.id_7_oto_sign_image2);
        id_7_oto_sign_image3=(AppCompatImageView)startView.findViewById(R.id.id_7_oto_sign_image3);
        id_7_oto_sign_image4=(AppCompatImageView)startView.findViewById(R.id.id_7_oto_sign_image4);
    }
    private void Initialize9Screen() {
        id_9_referred_to=(EditText)startView.findViewById(R.id.id_9_referred_to);
        id_9_employement_date=(EditText)startView.findViewById(R.id.id_9_employement_date);
        id_9_employement_no=(EditText)startView.findViewById(R.id.id_9_employement_no);
        id_9_gender=(EditText)startView.findViewById(R.id.id_9_gender);
        id_9_patientName=(EditText)startView.findViewById(R.id.id_9_patientName);
        id_9_dateReferred=(EditText)startView.findViewById(R.id.id_9_dateReferred);
        id_9_occupation=(EditText)startView.findViewById(R.id.id_9_occupation);
        id_9_id_number=(EditText)startView.findViewById(R.id.id_9_id_number);
        id_9_Company=(EditText)startView.findViewById(R.id.id_9_Company);
        id_9_Comments=(EditText)startView.findViewById(R.id.id_9_Comments);
        id_9_occdate=(EditText)startView.findViewById(R.id.id_9_occdate);
        id_9_occpractiseNumber=(EditText)startView.findViewById(R.id.id_9_occpractiseNumber);
        id_9_seenby=(EditText)startView.findViewById(R.id.id_9_seenby);
        id_9_Patient=(EditText)startView.findViewById(R.id.id_9_Patient);
        id_9_medComments=(EditText)startView.findViewById(R.id.id_9_medComments);
        id_9_meddate=(EditText)startView.findViewById(R.id.id_9_meddate);
        id_9_medpractiseNumber=(EditText)startView.findViewById(R.id.id_9_medpractiseNumber);

        id_9_medical_practioner_button=(Button)startView.findViewById(R.id.id_9_medical_practioner_button);
        id_9_occupational_health_practioner_button=(Button)startView.findViewById(R.id.id_9_occupational_health_practioner_button);
        id_9_save=(Button)startView.findViewById(R.id.id_9_save);
        id_9_save.setOnClickListener(this);
        id_9_medical_practioner_button.setOnClickListener(this);
        id_9_occupational_health_practioner_button.setOnClickListener(this);
        id_9_medical_practioner_image=(ImageView)startView.findViewById(R.id.id_9_medical_practioner_image);
        id_9_occupational_health_practioner_image=(ImageView)startView.findViewById(R.id.id_9_occupational_health_practioner_image);
    }
    private void Initialize8Screen() {
        id_8_vision_comments=(AppCompatEditText) startView.findViewById(R.id.id_8_vision_comments);
        id_8_audio_plh=(AppCompatEditText) startView.findViewById(R.id.id_8_audio_plh);
        id_8_audio_shift=(AppCompatEditText) startView.findViewById(R.id.id_8_audio_shift);
        id_8_lung=(AppCompatEditText) startView.findViewById(R.id.id_8_lung);
        id_8_fvc=(AppCompatEditText) startView.findViewById(R.id.id_8_fvc);
        id_8_fev=(AppCompatEditText) startView.findViewById(R.id.id_8_fev);
        id_8_fevoverfvc=(AppCompatEditText) startView.findViewById(R.id.id_8_fevoverfvc);
        id_8_peakflow=(AppCompatEditText) startView.findViewById(R.id.id_8_peakflow);
        id_8_certified=(AppCompatEditText) startView.findViewById(R.id.id_8_certified);
        id_8_datesign=(AppCompatEditText) startView.findViewById(R.id.id_8_datesign);
        id_8_vision=(RadioGroup)startView.findViewById(R.id.id_8_vision);
        id_8_farvision=(RadioGroup)startView.findViewById(R.id.id_8_farvision);
        id_8_nearvision=(RadioGroup)startView.findViewById(R.id.id_8_nearvision);
        id_8_nightvision=(RadioGroup)startView.findViewById(R.id.id_8_nightvision);
        id_8_othervision=(RadioGroup)startView.findViewById(R.id.id_8_othervision);
        id_8_chestray=(RadioGroup)startView.findViewById(R.id.id_8_chestray);
        id_8_rest_ecg=(RadioGroup)startView.findViewById(R.id.id_8_rest_ecg);
        id_8_cholestrol=(RadioGroup)startView.findViewById(R.id.id_8_cholestrol);
        id_8_mdt=(RadioGroup)startView.findViewById(R.id.id_8_mdt);
        id_8_other=(RadioGroup)startView.findViewById(R.id.id_8_other);

        id_8_employee_image=(ImageView)startView.findViewById(R.id.id_8_employee_image);
        id_8_examiner_image=(ImageView)startView.findViewById(R.id.id_8_examiner_image);

        id_8_examiner_button=(Button)startView.findViewById(R.id.id_8_examiner_button);
        id_8_employee_button=(Button)startView.findViewById(R.id.id_8_employee_button);
        id_8_save=(Button)startView.findViewById(R.id.id_8_save);
        id_8_examiner_button.setOnClickListener(this);
        id_8_employee_button.setOnClickListener(this);
        id_8_save.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_logout:
                ParseUser.logOutInBackground(new LogOutCallback() {
                    @Override
                    public void done(ParseException e) {
                        MainActivity.this.recreate();
                        /*Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();*/
                    }
                });
        }
        return true;
    }
    private void ChangeTitle(int number)
    {
        switch (number)
        {
            case 0:
                getSupportActionBar().setTitle("Medical Fitness Certificate");
                break;
            case 1:
                getSupportActionBar().setTitle("Occupational Health Record");
                break;
            case 2:
                getSupportActionBar().setTitle("Medical History");
                break;
            case 3:
                getSupportActionBar().setTitle("Physical Examination");
                break;
            case 4:
                getSupportActionBar().setTitle("Respiratory Questionnaire");
                break;
            case 5:
                getSupportActionBar().setTitle("Audio Questionnaire");
                break;
            case 6:
                getSupportActionBar().setTitle("Otoscopic Examination");
                break;
            case 7:
                getSupportActionBar().setTitle("Special Medical Investigation");
                break;
            case 8:
                getSupportActionBar().setTitle("Medical Referral");
                break;
            default:
                getSupportActionBar().setTitle("Medical Fitness");
                break;
        }
    }
}
