package com.medical.medicalfitness.objects;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.medical.medicalfitness.params.Constants;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by Johars on 2/18/2016.
 */
@ParseClassName("PhysicalExamination")
public class PhysicalExamination extends ParseObject{
    public PhysicalExamination() {
        super();
    }
    public PhysicalExamination(ParseUser user) {
        super();
        put("user", user);
    }
    public ParseUser getUser() {
        return getParseUser("user");
    }
    public void setUser(ParseUser user) {
        put("user", user);
    }

    public String getId_5_bp() {
        return getString("id_5_bp");
    }
    public String getId_5_pulse() {
        return getString("id_5_pulse");
    }
    public String getId_5_weight() {
        return getString("id_5_weight");
    }
    public String getId_5_bmi() {
        return getString("id_5_bmi");
    }
    public String getId_5_hgt() {
        return getString("id_5_hgt");
    }
    public String getId_5_height() {
        return getString("id_5_height");
    }
    public String getId_5_urine_analysis() {
        return getString("id_5_urine_analysis");
    }
    public String getId_5_ph() {
        return getString("id_5_ph");
    }
    public String getId_5_sg() {
        return getString("id_5_sg");
    }
    public String getId_5_bld() {
        return getString("id_5_bld");
    }
    public String getId_5_gluc() {
        return getString("id_5_gluc");
    }
    public String getId_5_leuk() {
        return getString("id_5_leuk");
    }
    public String getId_5_head_comment() {
        return getString("id_5_head_comment");
    }
    public String getId_5_ear_comment() {
        return getString("id_5_ear_comment");
    }
    public String getId_5_lungs_comment() {
        return getString("id_5_lungs_comment");
    }
    public String getId_5_heart_comment() {
        return getString("id_5_heart_comment");
    }
    public String getId_5_vascular_comment() {
        return getString("id_5_vascular_comment");
    }
    public String getId_5_abdomen_comment() {
        return getString("id_5_abdomen_comment");
    }
    public String getId_5_genito_comment() {
        return getString("id_5_genito_comment");
    }
    public String getId_5_neurological_comment() {
        return getString("id_5_neurological_comment");
    }
    public String getId_5_limbs_comment() {
        return getString("id_5_limbs_comment");
    }
    public String getId_5_spine_comment() {
        return getString("id_5_spine_comment");
    }
    public String getId_5_skin_comment() {
        return getString("id_5_skin_comment");
    }
    public String getId_5_psychological_comment() {
        return getString("id_5_psychological_comment");
    }
    public String getId_5_head() {
        return getString("id_5_head");
    }
    public String getId_5_ear() {
        return getString("id_5_ear");
    }
    public String getId_5_lungs() {
        return getString("id_5_lungs");
    }
    public String getId_5_heart() {
        return getString("id_5_heart");
    }
    public String getId_5_vascular() {
        return getString("id_5_vascular");
    }
    public String getId_5_abdomen() {
        return getString("id_5_abdomen");
    }
    public String getId_5_genito() {
        return getString("id_5_genito");
    }
    public String getId_5_neurological() {
        return getString("id_5_neurological");
    }
    public String getId_5_limbs() {
        return getString("id_5_limbs");
    }
    public String getId_5_spine() {
        return getString("id_5_spine");
    }
    public String getId_5_skin() {
        return getString("id_5_skin");
    }
    public String getId_5_psychological() {
        return getString("id_5_psychological");
    }

    public void setId_5_bp(String id_5_bp) {
        put("id_5_bp",id_5_bp);
    }
    public void setId_5_pulse(String id_5_pulse) {
        put("id_5_pulse",id_5_pulse);
    }
    public void setId_5_weight(String id_5_weight) {
        put("id_5_weight",id_5_weight);
    }
    public void setId_5_bmi(String id_5_bmi) {
        put("id_5_bmi",id_5_bmi);
    }
    public void setId_5_hgt(String id_5_hgt) {
        put("id_5_hgt",id_5_hgt);
    }
    public void setId_5_height(String id_5_height) {
        put("id_5_height",id_5_height);
    }
    public void setId_5_urine_analysis(String id_5_urine_analysis) {
        put("id_5_urine_analysis",id_5_urine_analysis);
    }
    public void setId_5_ph(String id_5_ph) {
        put("id_5_ph",id_5_ph);
    }
    public void setId_5_sg(String id_5_sg) {
        put("id_5_sg",id_5_sg);
    }
    public void setId_5_bld(String id_5_bld) {
        put("id_5_bld",id_5_bld);
    }
    public void setId_5_gluc(String id_5_gluc) {
        put("id_5_gluc",id_5_gluc);
    }
    public void setId_5_leuk(String id_5_leuk) {
        put("id_5_leuk",id_5_leuk);
    }
    public void setId_5_head_comment(String id_5_head_comment) {
        put("id_5_head_comment",id_5_head_comment);
    }
    public void setId_5_ear_comment(String id_5_ear_comment) {
        put("id_5_ear_comment",id_5_ear_comment);
    }
    public void setId_5_lungs_comment(String id_5_lungs_comment) {
        put("id_5_lungs_comment",id_5_lungs_comment);
    }
    public void setId_5_heart_comment(String id_5_heart_comment) {
        put("id_5_heart_comment",id_5_heart_comment);
    }
    public void setId_5_vascular_comment(String id_5_vascular_comment) {
        put("id_5_vascular_comment",id_5_vascular_comment);
    }
    public void setId_5_abdomen_comment(String id_5_abdomen_comment) {
        put("id_5_abdomen_comment",id_5_abdomen_comment);
    }
    public void setId_5_genito_comment(String id_5_genito_comment) {
        put("id_5_genito_comment",id_5_genito_comment);
    }
    public void setId_5_neurological_comment(String id_5_neurological_comment) {
        put("id_5_neurological_comment",id_5_neurological_comment);
    }
    public void setId_5_limbs_comment(String id_5_limbs_comment) {
        put("id_5_limbs_comment",id_5_limbs_comment);
    }
    public void setId_5_spine_comment(String id_5_spine_comment) {
        put("id_5_spine_comment",id_5_spine_comment);
    }
    public void setId_5_skin_comment(String id_5_skin_comment) {
        put("id_5_skin_comment",id_5_skin_comment);
    }
    public void setId_5_psychological_comment(String id_5_psychological_comment) {
        put("id_5_psychological_comment",id_5_psychological_comment);
    }
    public void setId_5_head(String id_5_head) {
        put("id_5_head",id_5_head);
    }
    public void setId_5_ear(String id_5_ear) {
        put("id_5_ear",id_5_ear);
    }
    public void setId_5_lungs(String id_5_lungs) {
        put("id_5_lungs",id_5_lungs);
    }
    public void setId_5_heart(String id_5_heart) {
        put("id_5_heart",id_5_heart);
    }
    public void setId_5_vascular(String id_5_vascular) {
        put("id_5_vascular",id_5_vascular);
    }
    public void setId_5_abdomen(String id_5_abdomen) {
        put("id_5_abdomen",id_5_abdomen);
    }
    public void setId_5_genito(String id_5_genito) {
        put("id_5_genito",id_5_genito);
    }
    public void setId_5_neurological(String id_5_neurological) {
        put("id_5_neurological",id_5_neurological);
    }
    public void setId_5_limbs(String id_5_limbs) {
        put("id_5_limbs",id_5_limbs);
    }
    public void setId_5_spine(String id_5_spine) {
        put("id_5_spine",id_5_spine);
    }
    public void setId_5_skin(String id_5_skin) {
        put("id_5_skin",id_5_skin);
    }
    public void setId_5_psychological(String id_5_psychological) {
        put("id_5_psychological",id_5_psychological);
    }
    public String getTitle()
    {
        return "Physical Examination";
    }
    public Document getinPDF(Document doc) throws DocumentException {
        doc=Constants.AddHeader(doc,getTitle(),4);
        doc.add(new Phrase(Constants.GetStringLine("BP", getId_5_bp())));
        doc.add(new Phrase(Constants.GetStringLine("PULSE", getId_5_pulse())));
        doc.add(new Phrase(Constants.GetStringLine("WEIGHT", getId_5_weight())));
        doc.add(new Phrase(Constants.GetStringLine("BMI", getId_5_bmi())));
        doc.add(new Phrase(Constants.GetStringLine("HGT", getId_5_hgt())));
        doc.add(new Phrase(Constants.GetStringLine("HEIGHT", getId_5_height())));
        doc.add(new Phrase(Constants.GetStringLine("Urine Analysis", getId_5_urine_analysis())));
        doc.add(new Phrase(Constants.GetStringLine("PH", getId_5_ph())));
        doc.add(new Phrase(Constants.GetStringLine("SG", getId_5_sg())));
        doc.add(new Phrase(Constants.GetStringLine("BLD", getId_5_bld())));
        doc.add(new Phrase(Constants.GetStringLine("GLUC", getId_5_gluc())));
        doc.add(new Phrase(Constants.GetStringLine("LEUK",getId_5_leuk())));
        doc.add(new Phrase(Constants.GetStringLine("Head, Face,Scalp and Neck...",getId_5_head())));
        doc.add(new Phrase(Constants.GetStringLine("Comments if (ABN)", getId_5_head_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Ear, Nose and Throat...",getId_5_ear())));
        doc.add(new Phrase(Constants.GetStringLine("Comments if (ABN)", getId_5_ear_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Lungs, Chest and Breast...",getId_5_lungs())));
        doc.add(new Phrase(Constants.GetStringLine("Comments if (ABN)", getId_5_lungs_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Heart - Size and sounds...",getId_5_heart())));
        doc.add(new Phrase(Constants.GetStringLine("Comments if (ABN)", getId_5_heart_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Vascular System (Pulses and glands)",getId_5_vascular())));
        doc.add(new Phrase(Constants.GetStringLine("Comments if (ABN)", getId_5_vascular_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Abdomen (Viscera and hernia)",getId_5_abdomen())));
        doc.add(new Phrase(Constants.GetStringLine("Comments if (ABN)", getId_5_abdomen_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Genito - Urinary (If applicable)",getId_5_genito())));
        doc.add(new Phrase(Constants.GetStringLine("Comments if (ABN)", getId_5_genito_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Neurological System: cranial nurves, sensory",getId_5_neurological())));
        doc.add(new Phrase(Constants.GetStringLine("Comments if (ABN)",getId_5_neurological_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Upper and Lower limbs (Strength, motion)",getId_5_limbs())));
        doc.add(new Phrase(Constants.GetStringLine("Comments if (ABN)", getId_5_limbs_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Spine and muscoskeletal: cervical, lumber",getId_5_spine())));
        doc.add(new Phrase(Constants.GetStringLine("Comments if (ABN)", getId_5_spine_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Skin and appendages",getId_5_skin())));
        doc.add(new Phrase(Constants.GetStringLine("Comments if (ABN)", getId_5_skin_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Psychological evaluation",getId_5_psychological())));
        doc.add(new Phrase(Constants.GetStringLine("Comments if (ABN)",getId_5_psychological_comment())));
        return doc;
    }
}
