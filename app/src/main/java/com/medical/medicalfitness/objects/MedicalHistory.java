package com.medical.medicalfitness.objects;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.medical.medicalfitness.params.Constants;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by Johars on 2/18/2016.
 */
@ParseClassName("MedicalHistory")
public class MedicalHistory extends ParseObject {
    public MedicalHistory() {
        super();
    }
    public MedicalHistory(ParseUser user) {
        super();
        put("user", user);
    }
    public ParseUser getUser() {
        return getParseUser("user");
    }
    public void setUser(ParseUser user) {
        put("user", user);
    }
    public String getId_3_fam_heart_disease_comment() {
        return getString("id_3_fam_heart_disease_comment");
    }

    public String getId_3_fam_blood_pressure_comment() {
        return getString("id_3_fam_blood_pressure_comment");
    }

    public String getId_3_fam_diabetes_comment() {
        return getString("id_3_fam_diabetes_comment");
    }

    public String getId_3_fam_epilepsy_comment() {
        return getString("id_3_fam_epilepsy_comment");
    }

    public String getId_3_fam_cancer_comment() {
        return getString("id_3_fam_cancer_comment");
    }

    public String getId_3_fam_thyroid_comment() {
        return getString("id_3_fam_thyroid_comment");
    }

    public String getId_3_fam_hereditary_comment() {
        return getString("id_3_fam_hereditary_comment");
    }

    public String getId_3_fam_blindness_comment() {
        return getString("id_3_fam_blindness_comment");
    }

    public String getId_3_fam_heart_disease() {
        return getString("id_3_fam_heart_disease");
    }

    public String getId_3_fam_blood_pressure() {
        return getString("id_3_fam_blood_pressure");
    }

    public String getId_3_fam_diabetes() {
        return getString("id_3_fam_diabetes");
    }

    public String getId_3_fam_epilepsy() {
        return getString("id_3_fam_epilepsy");
    }

    public String getId_3_fam_cancer() {
        return getString("id_3_fam_cancer");
    }

    public String getId_3_fam_thyroid() {
        return getString("id_3_fam_thyroid");
    }

    public String getId_3_fam_hereditary() {
        return getString("id_3_fam_hereditary");
    }

    public String getId_3_fam_blindness() {
        return getString("id_3_fam_blindness");
    }

    public String getId_3_you_heart_disease() {
        return getString("id_3_you_heart_disease");
    }

    public String getId_3_you_blood_pressure() {
        return getString("id_3_you_blood_pressure");
    }

    public String getId_3_you_diabetes() {
        return getString("id_3_you_diabetes");
    }

    public String getId_3_you_epilepsy() {
        return getString("id_3_you_epilepsy");
    }

    public String getId_3_you_cancer() {
        return getString("id_3_you_cancer");
    }

    public String getId_3_you_thyroid() {
        return getString("id_3_you_thyroid");
    }

    public String getId_3_you_smoking() {
        return getString("id_3_you_smoking");
    }

    public String getId_3_you_ex_smoker() {
        return getString("id_3_you_ex_smoker");
    }

    public String getId_3_you_skin_disorder() {
        return getString("id_3_you_skin_disorder");
    }

    public String getId_3_you_admitted_to_hospital() {
        return getString("id_3_you_admitted_to_hospital");
    }

    public String getId_3_you_muscles_disease() {
        return getString("id_3_you_muscles_disease");
    }

    public String getId_3_you_other_injury() {
        return getString("id_3_you_other_injury");
    }

    public String getId_3_you_pregnant() {
        return getString("id_3_you_pregnant");
    }

    public String getId_3_you_gynae() {
        return getString("id_3_you_gynae");
    }

    public String getId_3_you_headaches() {
        return getString("id_3_you_headaches");
    }

    public String getId_3_you_dizziness() {
        return getString("id_3_you_dizziness");
    }

    public String getId_3_you_hearing() {
        return getString("id_3_you_hearing");
    }

    public String getId_3_you_vision() {
        return getString("id_3_you_vision");
    }

    public String getId_3_you_head_injury() {
        return getString("id_3_you_head_injury");
    }

    public String getId_3_you_neurological() {
        return getString("id_3_you_neurological");
    }

    public String getId_3_you_car_accident() {
        return getString("id_3_you_car_accident");
    }

    public String getId_3_you_any_allergies() {
        return getString("id_3_you_any_allergies");
    }

    public String getId_3_you_asthma() {
        return getString("id_3_you_asthma");
    }

    public String getId_3_you_tb() {
        return getString("id_3_you_tb");
    }

    public String getId_3_you_pneumonia() {
        return getString("id_3_you_pneumonia");
    }

    public String getId_3_you_chest_discomfort() {
        return getString("id_3_you_chest_discomfort");
    }

    public String getId_3_you_heartburn() {
        return getString("id_3_you_heartburn");
    }

    public String getId_3_you_ulcer() {
        return getString("id_3_you_ulcer");
    }

    public String getId_3_you_rectal_bleading() {
        return getString("id_3_you_rectal_bleading");
    }

    public String getId_3_you_weightloss() {
        return getString("id_3_you_weightloss");
    }

    public String getId_3_you_depression() {
        return getString("id_3_you_depression");
    }

    public String getId_3_you_height_fear() {
        return getString("id_3_you_height_fear");
    }

    public String getId_3_you_play_sports() {
        return getString("id_3_you_play_sports");
    }

    public String getId_3_you_gp_address() {
        return getString("id_3_you_gp_address");
    }

    public String getId_3_you_drugs_type() {
        return getString("id_3_you_drugs_type");
    }

    public String getId_3_you_drugs_quantity() {
        return getString("id_3_you_drugs_quantity");
    }

    public String getId_3_you_heart_disease_comment() {
        return getString("id_3_you_heart_disease_comment");
    }

    public String getId_3_you_blood_pressure_comment() {
        return getString("id_3_you_blood_pressure_comment");
    }

    public String getId_3_you_diabetes_comment() {
        return getString("id_3_you_diabetes_comment");
    }

    public String getId_3_you_epilepsy_comment() {
        return getString("id_3_you_epilepsy_comment");
    }

    public String getId_3_you_cancer_comment() {
        return getString("id_3_you_cancer_comment");
    }

    public String getId_3_you_thyroid_comment() {
        return getString("id_3_you_thyroid_comment");
    }

    public String getId_3_you_smoking_comment() {
        return getString("id_3_you_smoking_comment");
    }

    public String getId_3_you_ex_smoker_comment() {
        return getString("id_3_you_ex_smoker_comment");
    }

    public String getId_3_you_skin_disorder_comment() {
        return getString("id_3_you_skin_disorder_comment");
    }

    public String getId_3_you_admitted_to_hospital_comment() {
        return getString("id_3_you_admitted_to_hospital_comment");
    }

    public String getId_3_you_other_injury_comment() {
        return getString("id_3_you_other_injury_comment");
    }

    public String getId_3_you_muscles_disease_comment() {
        return getString("id_3_you_muscles_disease_comment");
    }

    public String getId_3_you_pregnant_comment() {
        return getString("id_3_you_pregnant_comment");
    }

    public String getId_3_you_gynae_comment() {
        return getString("id_3_you_gynae_comment");
    }

    public String getId_3_you_headaches_comment() {
        return getString("id_3_you_headaches_comment");
    }

    public String getId_3_you_dizziness_comment() {
        return getString("id_3_you_dizziness_comment");
    }

    public String getId_3_you_hearing_comment() {
        return getString("id_3_you_hearing_comment");
    }

    public String getId_3_you_vision_comment() {
        return getString("id_3_you_vision_comment");
    }

    public String getId_3_you_head_injury_comment() {
        return getString("id_3_you_head_injury_comment");
    }

    public String getId_3_you_neurological_comment() {
        return getString("id_3_you_neurological_comment");
    }

    public String getId_3_you_car_accident_comment() {
        return getString("id_3_you_car_accident_comment");
    }

    public String getId_3_you_any_allergies_comment() {
        return getString("id_3_you_any_allergies_comment");
    }

    public String getId_3_you_asthma_comment() {
        return getString("id_3_you_asthma_comment");
    }

    public String getId_3_you_tb_comment() {
        return getString("id_3_you_tb_comment");
    }

    public String getId_3_you_pneumonia_comment() {
        return getString("id_3_you_pneumonia_comment");
    }

    public String getId_3_you_chest_discomfort_comment() {
        return getString("id_3_you_chest_discomfort_comment");
    }

    public String getId_3_you_heartburn_comment() {
        return getString("id_3_you_heartburn_comment");
    }

    public String getId_3_you_ulcer_comment() {
        return getString("id_3_you_ulcer_comment");
    }

    public String getId_3_you_rectal_bleading_comment() {
        return getString("id_3_you_rectal_bleading_comment");
    }

    public String getId_3_you_weightloss_comment() {
        return getString("id_3_you_weightloss_comment");
    }

    public String getId_3_you_depression_comment() {
        return getString("id_3_you_depression_comment");
    }

    public String getId_3_you_height_fear_comment() {
        return getString("id_3_you_height_fear_comment");
    }

    public String getId_3_you_play_sports_comment() {
        return getString("id_3_you_play_sports_comment");
    }

    public String getId_3_you_gp_address_comment() {
        return getString("id_3_you_gp_address_comment");
    }


    public void setId_3_fam_heart_disease_comment(String id_3_fam_heart_disease_comment) {
        put("id_3_fam_heart_disease_comment",id_3_fam_heart_disease_comment);
    }

    public void setId_3_fam_blood_pressure_comment(String id_3_fam_blood_pressure_comment) {
        put("id_3_fam_blood_pressure_comment",id_3_fam_blood_pressure_comment);
    }

    public void setId_3_fam_diabetes_comment(String id_3_fam_diabetes_comment) {
        put("id_3_fam_diabetes_comment",id_3_fam_diabetes_comment);
    }

    public void setId_3_fam_epilepsy_comment(String id_3_fam_epilepsy_comment) {
        put("id_3_fam_epilepsy_comment",id_3_fam_epilepsy_comment);
    }

    public void setId_3_fam_cancer_comment(String id_3_fam_cancer_comment) {
        put("id_3_fam_cancer_comment",id_3_fam_cancer_comment);
    }

    public void setId_3_fam_thyroid_comment(String id_3_fam_thyroid_comment) {
        put("id_3_fam_thyroid_comment",id_3_fam_thyroid_comment);
    }

    public void setId_3_fam_hereditary_comment(String id_3_fam_hereditary_comment) {
        put("id_3_fam_hereditary_comment",id_3_fam_hereditary_comment);
    }

    public void setId_3_fam_blindness_comment(String id_3_fam_blindness_comment) {
        put("id_3_fam_blindness_comment",id_3_fam_blindness_comment);
    }

    public void setId_3_fam_heart_disease(String id_3_fam_heart_disease) {
        put("id_3_fam_heart_disease",id_3_fam_heart_disease);
    }

    public void setId_3_fam_blood_pressure(String id_3_fam_blood_pressure) {
        put("id_3_fam_blood_pressure",id_3_fam_blood_pressure);
    }

    public void setId_3_fam_diabetes(String id_3_fam_diabetes) {
        put("id_3_fam_diabetes",id_3_fam_diabetes);
    }

    public void setId_3_fam_epilepsy(String id_3_fam_epilepsy) {
        put("id_3_fam_epilepsy",id_3_fam_epilepsy);
    }

    public void setId_3_fam_cancer(String id_3_fam_cancer) {
        put("id_3_fam_cancer",id_3_fam_cancer);
    }

    public void setId_3_fam_thyroid(String id_3_fam_thyroid) {
        put("id_3_fam_thyroid",id_3_fam_thyroid);
    }

    public void setId_3_fam_hereditary(String id_3_fam_hereditary) {
        put("id_3_fam_hereditary",id_3_fam_hereditary);
    }

    public void setId_3_fam_blindness(String id_3_fam_blindness) {
        put("id_3_fam_blindness",id_3_fam_blindness);
    }
    public void setId_3_you_heart_disease(String id_3_you_heart_disease) {
        put("id_3_you_heart_disease",id_3_you_heart_disease);
    }

    public void setId_3_you_blood_pressure(String id_3_you_blood_pressure) {
        put("id_3_you_blood_pressure",id_3_you_blood_pressure);
    }

    public void setId_3_you_diabetes(String id_3_you_diabetes) {
        put("id_3_you_diabetes",id_3_you_diabetes);
    }

    public void setId_3_you_epilepsy(String id_3_you_epilepsy) {
        put("id_3_you_epilepsy",id_3_you_epilepsy);
    }

    public void setId_3_you_cancer(String id_3_you_cancer) {
        put("id_3_you_cancer",id_3_you_cancer);
    }

    public void setId_3_you_thyroid(String id_3_you_thyroid) {
        put("id_3_you_thyroid",id_3_you_thyroid);
    }

    public void setId_3_you_smoking(String id_3_you_smoking) {
        put("id_3_you_smoking",id_3_you_smoking);
    }

    public void setId_3_you_ex_smoker(String id_3_you_ex_smoker) {
        put("id_3_you_ex_smoker",id_3_you_ex_smoker);
    }

    public void setId_3_you_skin_disorder(String id_3_you_skin_disorder) {
        put("id_3_you_skin_disorder",id_3_you_skin_disorder);
    }

    public void setId_3_you_admitted_to_hospital(String id_3_you_admitted_to_hospital) {
        put("id_3_you_admitted_to_hospital",id_3_you_admitted_to_hospital);
    }

    public void setId_3_you_muscles_disease(String id_3_you_muscles_disease) {
        put("id_3_you_muscles_disease",id_3_you_muscles_disease);
    }

    public void setId_3_you_other_injury(String id_3_you_other_injury) {
        put("id_3_you_other_injury",id_3_you_other_injury);
    }

    public void setId_3_you_pregnant(String id_3_you_pregnant) {
        put("id_3_you_pregnant",id_3_you_pregnant);
    }

    public void setId_3_you_gynae(String id_3_you_gynae) {
        put("id_3_you_gynae",id_3_you_gynae);
    }

    public void setId_3_you_headaches(String id_3_you_headaches) {
        put("id_3_you_headaches",id_3_you_headaches);
    }

    public void setId_3_you_dizziness(String id_3_you_dizziness) {
        put("id_3_you_dizziness",id_3_you_dizziness);
    }

    public void setId_3_you_hearing(String id_3_you_hearing) {
        put("id_3_you_hearing",id_3_you_hearing);
    }

    public void setId_3_you_vision(String id_3_you_vision) {
        put("id_3_you_vision",id_3_you_vision);
    }

    public void setId_3_you_head_injury(String id_3_you_head_injury) {
        put("id_3_you_head_injury",id_3_you_head_injury);
    }

    public void setId_3_you_neurological(String id_3_you_neurological) {
        put("id_3_you_neurological",id_3_you_neurological);
    }

    public void setId_3_you_car_accident(String id_3_you_car_accident) {
        put("id_3_you_car_accident",id_3_you_car_accident);
    }

    public void setId_3_you_any_allergies(String id_3_you_any_allergies) {
        put("id_3_you_any_allergies",id_3_you_any_allergies);
    }

    public void setId_3_you_asthma(String id_3_you_asthma) {
        put("id_3_you_asthma",id_3_you_asthma);
    }

    public void setId_3_you_tb(String id_3_you_tb) {
        put("id_3_you_tb",id_3_you_tb);
    }

    public void setId_3_you_pneumonia(String id_3_you_pneumonia) {
        put("id_3_you_pneumonia",id_3_you_pneumonia);
    }

    public void setId_3_you_chest_discomfort(String id_3_you_chest_discomfort) {
        put("id_3_you_chest_discomfort",id_3_you_chest_discomfort);
    }

    public void setId_3_you_heartburn(String id_3_you_heartburn) {
        put("id_3_you_heartburn",id_3_you_heartburn);
    }

    public void setId_3_you_ulcer(String id_3_you_ulcer) {
        put("id_3_you_ulcer",id_3_you_ulcer);
    }

    public void setId_3_you_rectal_bleading(String id_3_you_rectal_bleading) {
        put("id_3_you_rectal_bleading",id_3_you_rectal_bleading);
    }

    public void setId_3_you_weightloss(String id_3_you_weightloss) {
        put("id_3_you_weightloss",id_3_you_weightloss);
    }

    public void setId_3_you_depression(String id_3_you_depression) {
        put("id_3_you_depression",id_3_you_depression);
    }

    public void setId_3_you_height_fear(String id_3_you_height_fear) {
        put("id_3_you_height_fear",id_3_you_height_fear);
    }

    public void setId_3_you_play_sports(String id_3_you_play_sports) {
        put("id_3_you_play_sports",id_3_you_play_sports);
    }

    public void setId_3_you_gp_address(String id_3_you_gp_address) {
        put("id_3_you_gp_address",id_3_you_gp_address);
    }

    public void setId_3_you_drugs_type(String id_3_you_drugs_type) {
        put("id_3_you_drugs_type",id_3_you_drugs_type);
    }

    public void setId_3_you_drugs_quantity(String id_3_you_drugs_quantity) {
        put("id_3_you_drugs_quantity",id_3_you_drugs_quantity);
    }

    public void setId_3_you_heart_disease_comment(String id_3_you_heart_disease_comment) {
        put("id_3_you_heart_disease_comment",id_3_you_heart_disease_comment);
    }

    public void setId_3_you_blood_pressure_comment(String id_3_you_blood_pressure_comment) {
        put("id_3_you_blood_pressure_comment",id_3_you_blood_pressure_comment);
    }

    public void setId_3_you_diabetes_comment(String id_3_you_diabetes_comment) {
        put("id_3_you_diabetes_comment",id_3_you_diabetes_comment);
    }

    public void setId_3_you_epilepsy_comment(String id_3_you_epilepsy_comment) {
        put("id_3_you_epilepsy_comment",id_3_you_epilepsy_comment);
    }

    public void setId_3_you_cancer_comment(String id_3_you_cancer_comment) {
        put("id_3_you_cancer_comment",id_3_you_cancer_comment);
    }

    public void setId_3_you_thyroid_comment(String id_3_you_thyroid_comment) {
        put("id_3_you_thyroid_comment",id_3_you_thyroid_comment);
    }

    public void setId_3_you_smoking_comment(String id_3_you_smoking_comment) {
        put("id_3_you_smoking_comment",id_3_you_smoking_comment);
    }

    public void setId_3_you_ex_smoker_comment(String id_3_you_ex_smoker_comment) {
        put("id_3_you_ex_smoker_comment",id_3_you_ex_smoker_comment);
    }

    public void setId_3_you_skin_disorder_comment(String id_3_you_skin_disorder_comment) {
        put("id_3_you_skin_disorder_comment",id_3_you_skin_disorder_comment);
    }

    public void setId_3_you_admitted_to_hospital_comment(String id_3_you_admitted_to_hospital_comment) {
        put("id_3_you_admitted_to_hospital_comment",id_3_you_admitted_to_hospital_comment);
    }

    public void setId_3_you_other_injury_comment(String id_3_you_other_injury_comment) {
        put("id_3_you_other_injury_comment",id_3_you_other_injury_comment);
    }

    public void setId_3_you_muscles_disease_comment(String id_3_you_muscles_disease_comment) {
        put("id_3_you_muscles_disease_comment",id_3_you_muscles_disease_comment);
    }

    public void setId_3_you_pregnant_comment(String id_3_you_pregnant_comment) {
        put("id_3_you_pregnant_comment",id_3_you_pregnant_comment);
    }

    public void setId_3_you_gynae_comment(String id_3_you_gynae_comment) {
        put("id_3_you_gynae_comment",id_3_you_gynae_comment);
    }

    public void setId_3_you_headaches_comment(String id_3_you_headaches_comment) {
        put("id_3_you_headaches_comment",id_3_you_headaches_comment);
    }

    public void setId_3_you_dizziness_comment(String id_3_you_dizziness_comment) {
        put("id_3_you_dizziness_comment",id_3_you_dizziness_comment);
    }

    public void setId_3_you_hearing_comment(String id_3_you_hearing_comment) {
        put("id_3_you_hearing_comment",id_3_you_hearing_comment);
    }

    public void setId_3_you_vision_comment(String id_3_you_vision_comment) {
        put("id_3_you_vision_comment",id_3_you_vision_comment);
    }

    public void setId_3_you_head_injury_comment(String id_3_you_head_injury_comment) {
        put("id_3_you_head_injury_comment",id_3_you_head_injury_comment);
    }

    public void setId_3_you_neurological_comment(String id_3_you_neurological_comment) {
        put("id_3_you_neurological_comment",id_3_you_neurological_comment);
    }

    public void setId_3_you_car_accident_comment(String id_3_you_car_accident_comment) {
        put("id_3_you_car_accident_comment",id_3_you_car_accident_comment);
    }

    public void setId_3_you_any_allergies_comment(String id_3_you_any_allergies_comment) {
        put("id_3_you_any_allergies_comment",id_3_you_any_allergies_comment);
    }

    public void setId_3_you_asthma_comment(String id_3_you_asthma_comment) {
        put("id_3_you_asthma_comment",id_3_you_asthma_comment);
    }

    public void setId_3_you_tb_comment(String id_3_you_tb_comment) {
        put("id_3_you_tb_comment",id_3_you_tb_comment);
    }

    public void setId_3_you_pneumonia_comment(String id_3_you_pneumonia_comment) {
        put("id_3_you_pneumonia_comment",id_3_you_pneumonia_comment);
    }

    public void setId_3_you_chest_discomfort_comment(String id_3_you_chest_discomfort_comment) {
        put("id_3_you_chest_discomfort_comment",id_3_you_chest_discomfort_comment);
    }

    public void setId_3_you_heartburn_comment(String id_3_you_heartburn_comment) {
        put("id_3_you_heartburn_comment",id_3_you_heartburn_comment);
    }

    public void setId_3_you_ulcer_comment(String id_3_you_ulcer_comment) {
        put("id_3_you_ulcer_comment",id_3_you_ulcer_comment);
    }

    public void setId_3_you_rectal_bleading_comment(String id_3_you_rectal_bleading_comment) {
        put("id_3_you_rectal_bleading_comment",id_3_you_rectal_bleading_comment);
    }

    public void setId_3_you_weightloss_comment(String id_3_you_weightloss_comment) {
        put("id_3_you_weightloss_comment",id_3_you_weightloss_comment);
    }

    public void setId_3_you_depression_comment(String id_3_you_depression_comment) {
        put("id_3_you_depression_comment",id_3_you_depression_comment);
    }

    public void setId_3_you_height_fear_comment(String id_3_you_height_fear_comment) {
        put("id_3_you_height_fear_comment",id_3_you_height_fear_comment);
    }

    public void setId_3_you_play_sports_comment(String id_3_you_play_sports_comment) {
        put("id_3_you_play_sports_comment",id_3_you_play_sports_comment);
    }

    public void setId_3_you_gp_address_comment(String id_3_you_gp_address_comment) {
        put("id_3_you_gp_address_comment",id_3_you_gp_address_comment);
    }
    public String getTitle()
    {
        return "Medical History";
    }
    public Document getinPDF(Document doc) throws DocumentException {
        doc=Constants.AddHeader(doc,getTitle(),3);
        doc.add(new Phrase("Do a family member have the following?"));
        doc.add(new Phrase(Constants.GetStringLine("Heart Disease - valve, murmur, angina etc.",getId_3_fam_heart_disease())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_fam_heart_disease_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Blood pressure (High/ Low) and treatment...",getId_3_fam_blood_pressure())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_fam_blood_pressure_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Diabetes Melitus (sugar) on insulin, tabs etc.",getId_3_fam_diabetes())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_fam_diabetes_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Epilepsy/ Fits/ Convulsions",getId_3_fam_epilepsy())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_fam_epilepsy_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Cancer, tumours, blood disease",getId_3_fam_cancer())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_fam_cancer_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Thyroid disorder (Hyper/ Hypo)",getId_3_fam_thyroid())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_fam_thyroid_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Hereditary disease",getId_3_fam_hereditary())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_fam_hereditary_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Blindness/glaucoma",getId_3_fam_blindness())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_fam_blindness_comment())));
        doc.add(new Phrase("Have you ever had or currently have the following:"));
        doc.add(new Phrase(Constants.GetStringLine("Headaches - frequently or severe, migrains...",getId_3_you_headaches())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_headaches_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Dizziness or unsteadiness?",getId_3_you_dizziness())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_dizziness_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Head Injury - concussion, blackouts, unconscious...",getId_3_you_head_injury())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_head_injury_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Epilepsy/ Fits/ Convulsions",getId_3_you_epilepsy())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_epilepsy_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Any Neurological disorder?",getId_3_you_neurological())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_neurological_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Vision, eye problems...",getId_3_you_vision())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_vision_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Hearing and speech problems...",getId_3_you_hearing())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_hearing_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Any allergies? Bee - sting, meds...",getId_3_you_any_allergies())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_any_allergies_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Asthma, coughing, lung disease chronic etc...",getId_3_you_asthma())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_asthma_comment())));
        doc.add(new Phrase(Constants.GetStringLine("TB, Weight loss, night sweats, blood in sputum...",getId_3_you_tb())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_tb_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Pneumonia",getId_3_you_pneumonia())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_pneumonia_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Heart Disease - valve, murmur, angina etc.",getId_3_you_heart_disease())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_heart_disease_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Blood pressure (High/ Low) and treatment...",getId_3_you_blood_pressure())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_blood_pressure_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Chest discomfort, palpitations, tightness...",getId_3_you_chest_discomfort())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_chest_discomfort_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Frequent heartburn/ Indigestion...",getId_3_you_heartburn())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_heartburn_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Stomach, Ulcer, liver, Intestinal problems...",getId_3_you_ulcer())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_ulcer_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Diabetes Melitus (sugar) on insulin, tabs etc.",getId_3_you_diabetes())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_diabetes_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Gynae or prostate trouble...",getId_3_you_gynae())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_gynae_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Pregnant or planning to be?",getId_3_you_pregnant())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_pregnant_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Cancer, tumours, blood disease",getId_3_you_cancer())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_cancer_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Any skin disorders? Eczema, rash, psoriasis etc.",getId_3_you_skin_disorder())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_skin_disorder_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Disorders/ diseases of muscles, joints, bones",getId_3_you_muscles_disease())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_muscles_disease_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Back problems",getId_3_you_car_accident())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_car_accident_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Ever Admitted to hospital?",getId_3_you_admitted_to_hospital())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_admitted_to_hospital_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Any other Injury, illness, infection currently?",getId_3_you_other_injury())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_other_injury_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Any mental/psychological disorder",getId_3_you_rectal_bleading())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_rectal_bleading_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Depression, Anxiety, suicidal...",getId_3_you_depression())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_depression_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Afraid of heights or confined spaces?",getId_3_you_height_fear())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_height_fear_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Are you currently using alcohol or drugs?",getId_3_you_weightloss())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_weightloss_comment())));
        doc.add(new Phrase("Have you been treated for alcohol or drug addiction? Habit forming drugs..."));
        doc.add(new Phrase(Constants.GetStringLine("Type",getId_3_you_drugs_type())));
        doc.add(new Phrase(Constants.GetStringLine("Quantity",getId_3_you_drugs_quantity())));
        doc.add(new Phrase(Constants.GetStringLine("Are you smoking? How many per day? Year?",getId_3_you_smoking())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_smoking_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Are you an Ex smoker? Year when stopped?",getId_3_you_ex_smoker())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_ex_smoker_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Play sports - hobbies? hunting, choir, music etc...",getId_3_you_play_sports())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_play_sports_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Are you currently on any medication?",getId_3_you_thyroid())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_thyroid_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Name and address of your usual GP/ Clinic...",getId_3_you_gp_address())));
        doc.add(new Phrase(Constants.GetStringLine("Please Explain if - Yes",getId_3_you_gp_address_comment())));
        return doc;
    }
}
