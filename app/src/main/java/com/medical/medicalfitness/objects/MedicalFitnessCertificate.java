package com.medical.medicalfitness.objects;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.medical.medicalfitness.params.Constants;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.InputStream;

/**
 * Created by Johars on 2/18/2016.
 */
@ParseClassName("MedicalFitnessCertificate")
public class MedicalFitnessCertificate extends ParseObject {

    public MedicalFitnessCertificate() {
        super();
    }
    public MedicalFitnessCertificate(ParseUser user) {
        super();
        put("user", user);
    }
    public ParseUser getUser() {
        return getParseUser("user");
    }
    public void setUser(ParseUser user) {
        put("user", user);
    }
    public String getId_1_company_name() {
        return getString("id_1_company_name");
    }
    public String getId_1_name() {
        return getString("id_1_name");
    }
    public String getId_1_sur_name() {
        return getString("id_1_sur_name");
    }
    public String getId_1_id_number() {
        return getString("id_1_id_number");
    }
    public String getId_1_employee_number() {
        return getString("id_1_employee_number");
    }
    public String getId_1_job_title() {
        return getString("id_1_job_title");
    }
    public String getId_1_department() {
        return getString("id_1_department");
    }
    public String getId_1_address() {
        return getString("id_1_address");
    }
    public String getId_1_town() {
        return getString("id_1_town");
    }
    public String getId_1_region() {
        return getString("id_1_region");
    }
    public String getId_1_exam_type_defined() {
        return getString("id_1_exam_type_defined");
    }
    public String getId_1_acknowledged_name() {
        return getString("id_1_acknowledged_name");
    }
    public String getId_1_datesign() {
        return getString("id_1_datesign");
    }
    public String getId_1_name_surname() {
        return getString("id_1_name_surname");
    }
    public String getId_1_datesign_omp() {
        return getString("id_1_datesign_omp");
    }
    public String getId_1_date_expiry() {
        return getString("id_1_date_expiry");
    }
    public String getId_1_exam_type() {
        return getString("id_1_exam_type");
    }
    public String getId_1_person_record() {
        return getString("id_1_person_record");
    }




    public void setId_1_datesign_omp(String id_1_datesign_omp) {
        put("id_1_datesign_omp",id_1_datesign_omp);
    }
    public void setId_1_name_surname(String id_1_name_surname) {
        put("id_1_name_surname",id_1_name_surname);
    }
    public void setId_1_company_name(String id_1_company_name) {
        put("id_1_company_name", id_1_company_name);
    }
    public void setId_1_name(String id_1_name) {
        put("id_1_name",id_1_name);
    }
    public void setId_1_sur_name(String id_1_sur_name) {
        put("id_1_sur_name",id_1_sur_name);
    }
    public void setId_1_id_number(String id_1_id_number) {
        put("id_1_id_number",id_1_id_number);
    }
    public void setId_1_employee_number(String id_1_employee_number) {
        put("id_1_employee_number",id_1_employee_number);
    }
    public void setId_1_job_title(String id_1_job_title) {
        put("id_1_job_title",id_1_job_title);
    }
    public void setId_1_department(String id_1_department) {
        put("id_1_department",id_1_department);
    }
    public void setId_1_address(String id_1_address) {
        put("id_1_address",id_1_address);
    }
    public void setId_1_town(String id_1_town) {
        put("id_1_town",id_1_town);
    }
    public void setId_1_region(String id_1_region) {
        put("id_1_region",id_1_region);
    }
    public void setId_1_exam_type_defined(String id_1_exam_type_defined) {
        put("id_1_exam_type_defined",id_1_exam_type_defined);
    }
    public void setId_1_acknowledged_name(String id_1_acknowledged_name) {
        put("id_1_acknowledged_name",id_1_acknowledged_name);
    }
    public void setId_1_datesign(String id_1_datesign) {
        put("id_1_datesign",id_1_datesign);
    }
    public void setId_1_date_expiry(String id_1_date_expiry) {
        put("id_1_date_expiry",id_1_date_expiry);
    }
    public void setId_1_exam_type(String id_1_exam_type) {
        put("id_1_exam_type",id_1_exam_type);
    }
    public void setId_1_person_record(String id_1_person_record) {
        put("id_1_person_record",id_1_person_record);
    }
    public ParseFile getEmployee1() {
        return getParseFile("employee1");
    }
    public ParseFile getExaminer1() {
        return getParseFile("examiner1");
    }
    public ParseFile getOmp1() {
        return getParseFile("omp1");
    }
    public void setEmployee1(ParseFile employee1) {
        if(employee1!=null) {
            put("employee1", employee1);
        }
    }
    public void setExaminer1(ParseFile examiner1) {
        if(examiner1!=null) {
            put("examiner1", examiner1);
        }
    }
    public void setOmp1(ParseFile omp1) {
        if(omp1!=null) {
            put("omp1", omp1);
        }
    }


    private InputStream bemployee1;
    private InputStream bexaminer1,bomp1;
    public String getTitle()
    {
        return "Medical Fitness Certificate";
    }





    public Document getinPDF(Document doc) throws DocumentException {
        doc=Constants.AddHeader(doc,getTitle(),1);
        doc.add(new Phrase(Constants.GetStringLine("Organization/ Company Name",getId_1_company_name())));
        doc.add(new Phrase(Constants.GetStringLine("Name",getId_1_name())));
        doc.add(new Phrase(Constants.GetStringLine("Surname",getId_1_sur_name())));
        doc.add(new Phrase(Constants.GetStringLine("ID / Passport Number",getId_1_id_number())));
        doc.add(new Phrase(Constants.GetStringLine("Employee/ Clock Number",getId_1_employee_number())));
        doc.add(new Phrase(Constants.GetStringLine("Job Title",getId_1_job_title())));
        doc.add(new Phrase(Constants.GetStringLine("Department",getId_1_department())));
        doc.add(new Phrase(Constants.GetStringLine("Address",getId_1_address())));
        doc.add(new Phrase(Constants.GetStringLine("Town/ City",getId_1_town())));
        doc.add(new Phrase(Constants.GetStringLine("Region",getId_1_region())));
        doc.add(new Phrase(Constants.GetStringLine("Type of Examination",getId_1_exam_type())));
        doc.add(new Phrase(Constants.GetStringLine("Define",getId_1_exam_type_defined())));
        doc.add(new Phrase(Constants.GetStringLine("According to the Generic job category we find the person",getId_1_person_record())));
        doc.add(new Phrase("I " + getId_1_acknowledged_name() + " acknowledge I have been informed of the outcome of the above assessment. AnyIndication of alteration of the information here in will render this record invalid."));
        doc=Constants.AddImage(doc,getBemployee1(),"Signature of Employee");
        doc=Constants.AddImage(doc,getBexaminer1(),"Signature of Examiner");
        doc.add(new Phrase(Constants.GetStringLine("Date Signed (DD/MM/YYYY)",getId_1_datesign())));
        doc.add(new Phrase("Finess Certification By"));
        doc=Constants.AddImage(doc,getBomp1(),"Signature of OMP/OHN");
        doc.add(new Phrase(Constants.GetStringLine("Name and Surname",getId_1_name_surname())));
        doc.add(new Phrase(Constants.GetStringLine("Date Signed (DD/MM/YYYY)",getId_1_datesign_omp())));
        doc.add(new Phrase(Constants.GetStringLine("Certificate Expiry date",getId_1_date_expiry())));
        return doc;
    }

    public InputStream getBemployee1() {
        return bemployee1;
    }

    public void setBemployee1(InputStream bemployee1) {
        if(bemployee1!=null) {
            this.bemployee1 = bemployee1;
        }
    }

    public InputStream getBexaminer1() {
        return bexaminer1;
    }

    public void setBexaminer1(InputStream bexaminer1) {
        if(bexaminer1!=null) {
            this.bexaminer1 = bexaminer1;
        }
    }

    public InputStream getBomp1() {
        return bomp1;
    }

    public void setBomp1(InputStream bomp1) {
        if(bomp1!=null) {
            this.bomp1 = bomp1;
        }
    }
}