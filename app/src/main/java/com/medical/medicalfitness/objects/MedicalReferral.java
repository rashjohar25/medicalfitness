package com.medical.medicalfitness.objects;

import android.widget.EditText;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.medical.medicalfitness.R;
import com.medical.medicalfitness.params.Constants;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Johars on 2/18/2016.
 */
@ParseClassName("MedicalReferral")
public class MedicalReferral extends ParseObject {
    public MedicalReferral() {
        super();
    }
    public MedicalReferral(ParseUser user) {
        super();
        put("user", user);
    }
    public ParseUser getUser() {
        return getParseUser("user");
    }
    public void setUser(ParseUser user) {
        put("user", user);
    }

    public void setId_9_referred_to(String id_9_referred_to) {
        put("id_9_referred_to",id_9_referred_to);
    }

    public void setId_9_employement_date(String id_9_employement_date) {
        put("id_9_employement_date",id_9_employement_date);
    }

    public void setId_9_employement_no(String id_9_employement_no) {
        put("id_9_employement_no",id_9_employement_no);
    }

    public void setId_9_gender(String id_9_gender) {
        put("id_9_gender",id_9_gender);
    }

    public void setId_9_patientName(String id_9_patientName) {
        put("id_9_patientName",id_9_patientName);
    }

    public void setId_9_dateReferred(String id_9_dateReferred) {
        put("id_9_dateReferred",id_9_dateReferred);
    }

    public void setId_9_occupation(String id_9_occupation) {
        put("id_9_occupation",id_9_occupation);
    }

    public void setId_9_id_number(String id_9_id_number) {
        put("id_9_id_number",id_9_id_number);
    }

    public void setId_9_Company(String id_9_Company) {
        put("id_9_Company",id_9_Company);
    }

    public void setId_9_Comments(String id_9_Comments) {
        put("id_9_Comments",id_9_Comments);
    }

    public void setId_9_occdate(String id_9_occdate) {
        put("id_9_occdate",id_9_occdate);
    }

    public void setId_9_occpractiseNumber(String id_9_occpractiseNumber) {
        put("id_9_occpractiseNumber",id_9_occpractiseNumber);
    }

    public void setId_9_seenby(String id_9_seenby) {
        put("id_9_seenby",id_9_seenby);
    }

    public void setId_9_Patient(String id_9_Patient) {
        put("id_9_Patient",id_9_Patient);
    }

    public void setId_9_medComments(String id_9_medComments) {
        put("id_9_medComments",id_9_medComments);
    }

    public void setId_9_meddate(String id_9_meddate) {
        put("id_9_meddate",id_9_meddate);
    }

    public void setId_9_medpractiseNumber(String id_9_medpractiseNumber) {
        put("id_9_medpractiseNumber",id_9_medpractiseNumber);
    }

    public void setOccfileDate9(ParseFile occfileDate9) {
        if(occfileDate9!=null) {
            put("occfileDate9", occfileDate9);
        }
    }

    public void setMedfileDate9(ParseFile medfileDate9) {
        if(medfileDate9!=null) {
            put("medfileDate9", medfileDate9);
        }
    }

    public String getId_9_referred_to() {
        return getString("id_9_referred_to");
    }
    public String getId_9_employement_date() {
        return getString("id_9_employement_date");
    }
    public String getId_9_employement_no() {
        return getString("id_9_employement_no");
    }
    public String getId_9_gender() {
        return getString("id_9_gender");
    }
    public String getId_9_patientName() {
        return getString("id_9_patientName");
    }
    public String getId_9_dateReferred() {
        return getString("id_9_dateReferred");
    }
    public String getId_9_occupation() {
        return getString("id_9_occupation");
    }
    public String getId_9_id_number() {
        return getString("id_9_id_number");
    }
    public String getId_9_Company() {
        return getString("id_9_Company");
    }
    public String getId_9_Comments() {
        return getString("id_9_Comments");
    }
    public String getId_9_occdate() {
        return getString("id_9_occdate");
    }
    public String getId_9_occpractiseNumber() {
        return getString("id_9_occpractiseNumber");
    }
    public String getId_9_seenby() {
        return getString("id_9_seenby");
    }
    public String getId_9_Patient() {
        return getString("id_9_Patient");
    }
    public String getId_9_medComments() {
        return getString("id_9_medComments");
    }
    public String getId_9_meddate() {
        return getString("id_9_meddate");
    }
    public String getId_9_medpractiseNumber() {
        return getString("id_9_medpractiseNumber");
    }
    public ParseFile getOccfileDate9() {
        return getParseFile("occfileDate9");
    }
    public ParseFile getMedfileDate9() {
        return getParseFile("medfileDate9");
    }

    public String getTitle()
    {
        return "Medical Referral";
    }


    private InputStream bmedfileDate9,boccfileDate9;
    public Document getinPDF(Document doc) throws DocumentException {
        doc=Constants.AddHeader(doc,getTitle(),9);
        doc.add(new Phrase(Constants.GetStringLine("Referred To",getId_9_referred_to())));
        doc.add(new Phrase(Constants.GetStringLine("Employment Date",getId_9_employement_date())));
        doc.add(new Phrase(Constants.GetStringLine("Employee / Clock No.",getId_9_employement_no())));
        doc.add(new Phrase(Constants.GetStringLine("Gender (M/F)",getId_9_gender())));
        doc.add(new Phrase(Constants.GetStringLine("Patient Name and Surname",getId_9_patientName())));
        doc.add(new Phrase(Constants.GetStringLine("Date Referred",getId_9_dateReferred())));
        doc.add(new Phrase(Constants.GetStringLine("Occupation",getId_9_occupation())));
        doc.add(new Phrase(Constants.GetStringLine("ID Number",getId_9_id_number())));
        doc.add(new Phrase(Constants.GetStringLine("Company",getId_9_Company())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_9_Comments())));
        doc=Constants.AddImage(doc,getBoccfileDate9(),"Occupational Health Practitioner");
        doc.add(new Phrase(Constants.GetStringLine("Date (DD/MM/YYYY)",getId_9_occdate())));
        doc.add(new Phrase(Constants.GetStringLine("Practise Number",getId_9_occpractiseNumber())));
        doc.add(new Phrase(Constants.GetStringLine("Seen By",getId_9_seenby())));
        doc.add(new Phrase(Constants.GetStringLine("Patient",getId_9_Patient())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_9_medComments())));
        doc=Constants.AddImage(doc,getBmedfileDate9(),"Medical Practitioner Signature");
        doc.add(new Phrase(Constants.GetStringLine("Date (DD/MM/YYYY)",getId_9_meddate())));
        doc.add(new Phrase(Constants.GetStringLine("Practise Number",getId_9_medpractiseNumber())));
        return doc;
    }

    public InputStream getBmedfileDate9() {
        return bmedfileDate9;
    }

    public void setBmedfileDate9(InputStream bmedfileDate9) {
        if(bmedfileDate9!=null) {
            this.bmedfileDate9 = bmedfileDate9;
        }
    }

    public InputStream getBoccfileDate9() {
        return boccfileDate9;
    }

    public void setBoccfileDate9(InputStream boccfileDate9) {
        if(boccfileDate9!=null) {
            this.boccfileDate9 = boccfileDate9;
        }
    }
}
