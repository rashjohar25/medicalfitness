package com.medical.medicalfitness.objects;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.medical.medicalfitness.params.Constants;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.InputStream;

/**
 * Created by Johars on 2/18/2016.
 */
@ParseClassName("RespiratoryQuestionnaire")
public class RespiratoryQuestionnaire extends ParseObject {
    public RespiratoryQuestionnaire() {
        super();
    }
    public RespiratoryQuestionnaire(ParseUser user) {
        super();
        put("user", user);
    }
    public ParseUser getUser() {
        return getParseUser("user");
    }
    public void setUser(ParseUser user) {
        put("user", user);
    }


    public void setId_6_name(String id_6_name) {
        put("id_6_name",id_6_name);
    }

    public void setId_6_dob(String id_6_dob) {
        put("id_6_dob",id_6_dob);
    }

    public void setId_6_jobTitle(String id_6_jobTitle) {
        put("id_6_jobTitle",id_6_jobTitle);
    }

    public void setId_6_weight(String id_6_weight) {
        put("id_6_weight",id_6_weight);
    }

    public void setId_6_height(String id_6_height) {
        put("id_6_height",id_6_height);
    }

    public void setId_6_department(String id_6_department) {
        put("id_6_department",id_6_department);
    }

    public void setId_6_year_exposed(String id_6_year_exposed) {
        put("id_6_year_exposed",id_6_year_exposed);
    }

    public void setId_6_company_name(String id_6_company_name) {
        put("id_6_company_name",id_6_company_name);
    }

    public void setId_6_job_title(String id_6_job_title) {
        put("id_6_job_title",id_6_job_title);
    }

    public void setId_6_hazards_exposed_to(String id_6_hazards_exposed_to) {
        put("id_6_hazards_exposed_to",id_6_hazards_exposed_to);
    }

    public void setId_6_chest_problem_comment(String id_6_chest_problem_comment) {
        put("id_6_chest_problem_comment",id_6_chest_problem_comment);
    }

    public void setId_6_lungcondition_comment(String id_6_lungcondition_comment) {
        put("id_6_lungcondition_comment",id_6_lungcondition_comment);
    }

    public void setId_6_tb_comment(String id_6_tb_comment) {
        put("id_6_tb_comment",id_6_tb_comment);
    }

    public void setId_6_cold_comment(String id_6_cold_comment) {
        put("id_6_cold_comment",id_6_cold_comment);
    }

    public void setId_6_smoking_comment(String id_6_smoking_comment) {
        put("id_6_smoking_comment",id_6_smoking_comment);
    }

    public void setId_6_ex_smoker_comment(String id_6_ex_smoker_comment) {
        put("id_6_ex_smoker_comment",id_6_ex_smoker_comment);
    }

    public void setId_6_xray_comment(String id_6_xray_comment) {
        put("id_6_xray_comment",id_6_xray_comment);
    }

    public void setId_6_ppe_comment(String id_6_ppe_comment) {
        put("id_6_ppe_comment",id_6_ppe_comment);
    }

    public void setId_6_on_treatment_comment(String id_6_on_treatment_comment) {
        put("id_6_on_treatment_comment",id_6_on_treatment_comment);
    }

    public void setId_6_chest_problem(String id_6_chest_problem) {
        put("id_6_chest_problem",id_6_chest_problem);
    }

    public void setId_6_lungcondition(String id_6_lungcondition) {
        put("id_6_lungcondition",id_6_lungcondition);
    }

    public void setId_6_tb(String id_6_tb) {
        put("id_6_tb",id_6_tb);
    }

    public void setId_6_cold(String id_6_cold) {
        put("id_6_cold",id_6_cold);
    }

    public void setId_6_smoking(String id_6_smoking) {
        put("id_6_smoking",id_6_smoking);
    }

    public void setId_6_ex_smoker(String id_6_ex_smoker) {
        put("id_6_ex_smoker",id_6_ex_smoker);
    }

    public void setId_6_xray(String id_6_xray) {
        put("id_6_xray",id_6_xray);
    }

    public void setId_6_ppe(String id_6_ppe) {
        put("id_6_ppe",id_6_ppe);
    }

    public void setId_6_on_treatment(String id_6_on_treatment) {
        put("id_6_on_treatment",id_6_on_treatment);
    }
    public String getId_6_name() {
        return getString("id_6_name");
    }
    public String getId_6_dob() {
        return getString("id_6_dob");
    }
    public String getId_6_jobTitle() {
        return getString("id_6_jobTitle");
    }
    public String getId_6_weight() {
        return getString("id_6_weight");
    }
    public String getId_6_height() {
        return getString("id_6_height");
    }
    public String getId_6_department() {
        return getString("id_6_department");
    }
    public String getId_6_year_exposed() {
        return getString("id_6_year_exposed");
    }
    public String getId_6_company_name() {
        return getString("id_6_company_name");
    }
    public String getId_6_job_title() {
        return getString("id_6_job_title");
    }
    public String getId_6_hazards_exposed_to() {
        return getString("id_6_hazards_exposed_to");
    }
    public String getId_6_chest_problem_comment() {
        return getString("id_6_chest_problem_comment");
    }
    public String getId_6_lungcondition_comment() {
        return getString("id_6_lungcondition_comment");
    }
    public String getId_6_tb_comment() {
        return getString("id_6_tb_comment");
    }
    public String getId_6_cold_comment() {
        return getString("id_6_cold_comment");
    }
    public String getId_6_smoking_comment() {
        return getString("id_6_smoking_comment");
    }
    public String getId_6_ex_smoker_comment() {
        return getString("id_6_ex_smoker_comment");
    }
    public String getId_6_xray_comment() {
        return getString("id_6_xray_comment");
    }
    public String getId_6_ppe_comment() {
        return getString("id_6_ppe_comment");
    }
    public String getId_6_on_treatment_comment() {
        return getString("id_6_on_treatment_comment");
    }
    public String getId_6_chest_problem() {
        return getString("id_6_chest_problem");
    }
    public String getId_6_lungcondition() {
        return getString("id_6_lungcondition");
    }
    public String getId_6_tb() {
        return getString("id_6_tb");
    }
    public String getId_6_cold() {
        return getString("id_6_cold");
    }
    public String getId_6_smoking() {
        return getString("id_6_smoking");
    }
    public String getId_6_ex_smoker() {
        return getString("id_6_ex_smoker");
    }
    public String getId_6_xray() {
        return getString("id_6_xray");
    }
    public String getId_6_ppe() {
        return getString("id_6_ppe");
    }
    public String getId_6_on_treatment() {
        return getString("id_6_on_treatment");
    }
    public ParseFile getEmployee6() {
        return getParseFile("employee6");
    }
    public ParseFile getTechnician6() {
        return getParseFile("technician6");
    }
    public void setEmployee6(ParseFile employee6) {
        if(employee6!=null) {
            put("employee6", employee6);
        }
    }
    public void setTechnician6(ParseFile technician6) {
        if(technician6!=null) {
            put("technician6", technician6);
        }
    }
    public String getTitle()
    {
        return "Respiratory Questionnaire";
    }
    private InputStream bemployee6,btechnician6;
    public Document getinPDF(Document doc) throws DocumentException {
        doc=Constants.AddHeader(doc,getTitle(),5);
        doc.add(new Phrase(Constants.GetStringLine("Name",getId_6_name())));
        doc.add(new Phrase(Constants.GetStringLine("DOB (DD/MM/YYYY)",getId_6_dob())));
        doc.add(new Phrase(Constants.GetStringLine("Job Title",getId_6_jobTitle())));
        doc.add(new Phrase(Constants.GetStringLine("Weight",getId_6_weight())));
        doc.add(new Phrase(Constants.GetStringLine("Height",getId_6_height())));
        doc.add(new Phrase(Constants.GetStringLine("Department",getId_6_department())));
        doc.add(new Phrase("Please complete below if history of hazard exposure e.g. asbestos, fumes, radiation, dust etc..."));
        doc.add(new Phrase(Constants.GetStringLine("Years Exposed",getId_6_year_exposed())));
        doc.add(new Phrase(Constants.GetStringLine("Company Name",getId_6_company_name())));
        doc.add(new Phrase(Constants.GetStringLine("Job Title/ Occupation",getId_6_job_title())));
        doc.add(new Phrase(Constants.GetStringLine("Hazards Exposed to",getId_6_hazards_exposed_to())));
        doc.add(new Phrase("Have you had the following?"));
        doc.add(new Phrase(Constants.GetStringLine("Chest Problems, e.g. short of breath, coughing, wheeze...",getId_6_chest_problem())));
        doc.add(new Phrase(Constants.GetStringLine("Explain if Yes",getId_6_chest_problem_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Do your Natural parents have a chronic lung conditon?",getId_6_lungcondition())));
        doc.add(new Phrase(Constants.GetStringLine("Explain if Yes",getId_6_lungcondition_comment())));
        doc.add(new Phrase(Constants.GetStringLine("TB, emphysema, asthma, pneumonia, chronic bronchitis...",getId_6_tb())));
        doc.add(new Phrase(Constants.GetStringLine("Explain if Yes",getId_6_tb_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Currently have a cold, flu, sinus or runny nose",getId_6_cold())));
        doc.add(new Phrase(Constants.GetStringLine("Explain if Yes",getId_6_cold_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Are you smoking? Cigarette, pipe, cigars? Amount/day?",getId_6_smoking())));
        doc.add(new Phrase(Constants.GetStringLine("Explain if Yes",getId_6_smoking_comment())));
        doc.add(new Phrase(Constants.GetStringLine("An Ex-Smoker? State date when stopped",getId_6_ex_smoker())));
        doc.add(new Phrase(Constants.GetStringLine("Explain if Yes",getId_6_ex_smoker_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Previous chest X-rays done?",getId_6_xray())));
        doc.add(new Phrase(Constants.GetStringLine("Explain if Yes",getId_6_xray_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Do you use PPE's at work?",getId_6_ppe())));
        doc.add(new Phrase(Constants.GetStringLine("Explain if Yes",getId_6_ppe_comment())));
        doc.add(new Phrase(Constants.GetStringLine("Are you currently on Treatment?",getId_6_on_treatment())));
        doc.add(new Phrase(Constants.GetStringLine("Explain if Yes",getId_6_on_treatment_comment())));
        doc=Constants.AddImage(doc,getBemployee6(),"Employee Signature");
        doc=Constants.AddImage(doc,getBtechnician6(),"Technician Signature");
        return doc;
    }

    public InputStream getBemployee6() {
        return bemployee6;
    }

    public void setBemployee6(InputStream bemployee6) {
        if(bemployee6!=null) {
            this.bemployee6 = bemployee6;
        }
    }

    public InputStream getBtechnician6() {
        return btechnician6;
    }

    public void setBtechnician6(InputStream btechnician6) {
        if(btechnician6!=null) {
            this.btechnician6 = btechnician6;
        }
    }
}
