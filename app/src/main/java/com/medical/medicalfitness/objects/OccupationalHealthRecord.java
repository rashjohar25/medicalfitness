package com.medical.medicalfitness.objects;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.medical.medicalfitness.params.Constants;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.InputStream;

/**
 * Created by Johars on 2/18/2016.
 */
@ParseClassName("OccupationalHealthRecord")
public class OccupationalHealthRecord extends ParseObject {
    public OccupationalHealthRecord() {
        super();
    }

    public OccupationalHealthRecord(ParseUser user) {
        super();
        put("user", user);
    }

    public ParseUser getUser() {
        return getParseUser("user");
    }

    public void setUser(ParseUser user) {
        put("user", user);
    }

    public String getId_2_name_surname() {
        return getString("id_2_name_surname");
    }
    public String getId_2_id_number() {
        return getString("id_2_id_number");
    }
    public String getId_2_gender() {
        return getString("id_2_gender");
    }
    public String getId_2_marital_status() {
        return getString("id_2_marital_status");
    }
    public String getId_2_home_address() {
        return getString("id_2_home_address");
    }
    public String getId_2_town() {
        return getString("id_2_town");
    }
    public String getId_2_province() {
        return getString("id_2_province");
    }
    public String getId_2_contact() {
        return getString("id_2_contact");
    }
    public String getId_2_medical() {
        return getString("id_2_medical");
    }
    public String getId_2_company() {
        return getString("id_2_company");
    }
    public String getId_2_job_title() {
        return getString("id_2_job_title");
    }
    public String getId_2_employee_no() {
        return getString("id_2_employee_no");
    }
    public String getId_2_contractbase() {
        return getString("id_2_contractbase");
    }
    public String getId_2_shift_worker() {
        return getString("id_2_shift_worker");
    }
    public String getId_2_starting_date() {
        return getString("id_2_starting_date");
    }
    public String getId_2_department() {
        return getString("id_2_department");
    }
    public String getId_2_supervisor() {
        return getString("id_2_supervisor");
    }
    public String getId_2_contact_office() {
        return getString("id_2_contact_office");
    }
    public String getId_2_company1() {
        return getString("id_2_company1");
    }
    public String getId_2_years1() {
        return getString("id_2_years1");
    }
    public String getId_2_jobtitle1() {
        return getString("id_2_jobtitle1");
    }
    public String getId_2_hazard1() {
        return getString("id_2_hazard1");
    }
    public String getId_2_company2() {
        return getString("id_2_company2");
    }
    public String getId_2_years2() {
        return getString("id_2_years2");
    }
    public String getId_2_jobtitle2() {
        return getString("id_2_jobtitle2");
    }
    public String getId_2_hazard2() {
        return getString("id_2_hazard2");
    }
    public String getId_2_company3() {
        return getString("id_2_company3");
    }
    public String getId_2_years3() {
        return getString("id_2_years3");
    }
    public String getId_2_jobtitle3() {
        return getString("id_2_jobtitle3");
    }
    public String getId_2_hazard3() {
        return getString("id_2_hazard3");
    }
    public String getId_2_company4() {
        return getString("id_2_company4");
    }
    public String getId_2_years4() {
        return getString("id_2_years4");
    }
    public String getId_2_jobtitle4() {
        return getString("id_2_jobtitle4");
    }
    public String getId_2_hazard4() {
        return getString("id_2_hazard4");
    }
    public String getId_2_consent() {
        return getString("id_2_consent");
    }
    public String getId_2_datesign() {
        return getString("id_2_datesign");
    }

    public void setId_2_name_surname(String id_2_name_surname) {
        put("id_2_name_surname", id_2_name_surname);
    }

    public void setId_2_id_number(String id_2_id_number) {
        put("id_2_id_number", id_2_id_number);
    }

    public void setId_2_gender(String id_2_gender) {
        put("id_2_gender", id_2_gender);
    }

    public void setId_2_marital_status(String id_2_marital_status) {
        put("id_2_marital_status", id_2_marital_status);
    }

    public void setId_2_home_address(String id_2_home_address) {
        put("id_2_home_address", id_2_home_address);
    }

    public void setId_2_town(String id_2_town) {
        put("id_2_town", id_2_town);
    }

    public void setId_2_province(String id_2_province) {
        put("id_2_province", id_2_province);
    }

    public void setId_2_contact(String id_2_contact) {
        put("id_2_contact", id_2_contact);
    }

    public void setId_2_medical(String id_2_medical) {
        put("id_2_medical", id_2_medical);
    }

    public void setId_2_company(String id_2_company) {
        put("id_2_company", id_2_company);
    }

    public void setId_2_job_title(String id_2_job_title) {
        put("id_2_job_title", id_2_job_title);
    }

    public void setId_2_employee_no(String id_2_employee_no) {
        put("id_2_employee_no", id_2_employee_no);
    }

    public void setId_2_contractbase(String id_2_contractbase) {
        put("id_2_contractbase", id_2_contractbase);
    }

    public void setId_2_shift_worker(String id_2_shift_worker) {
        put("id_2_shift_worker", id_2_shift_worker);
    }

    public void setId_2_starting_date(String id_2_starting_date) {
        put("id_2_starting_date", id_2_starting_date);
    }

    public void setId_2_department(String id_2_department) {
        put("id_2_department", id_2_department);
    }

    public void setId_2_supervisor(String id_2_supervisor) {
        put("id_2_supervisor", id_2_supervisor);
    }

    public void setId_2_contact_office(String id_2_contact_office) {
        put("id_2_contact_office", id_2_contact_office);
    }

    public void setId_2_company1(String id_2_company1) {
        put("id_2_company1", id_2_company1);
    }

    public void setId_2_years1(String id_2_years1) {
        put("id_2_years1", id_2_years1);
    }

    public void setId_2_jobtitle1(String id_2_jobtitle1) {
        put("id_2_jobtitle1", id_2_jobtitle1);
    }

    public void setId_2_hazard1(String id_2_hazard1) {
        put("id_2_hazard1", id_2_hazard1);
    }

    public void setId_2_company2(String id_2_company2) {
        put("id_2_company2", id_2_company2);
    }

    public void setId_2_years2(String id_2_years2) {
        put("id_2_years2", id_2_years2);
    }

    public void setId_2_jobtitle2(String id_2_jobtitle2) {
        put("id_2_jobtitle2", id_2_jobtitle2);
    }

    public void setId_2_hazard2(String id_2_hazard2) {
        put("id_2_hazard2", id_2_hazard2);
    }

    public void setId_2_company3(String id_2_company3) {
        put("id_2_company3", id_2_company3);
    }

    public void setId_2_years3(String id_2_years3) {
        put("id_2_years3", id_2_years3);
    }

    public void setId_2_jobtitle3(String id_2_jobtitle3) {
        put("id_2_jobtitle3", id_2_jobtitle3);
    }

    public void setId_2_hazard3(String id_2_hazard3) {
        put("id_2_hazard3", id_2_hazard3);
    }

    public void setId_2_company4(String id_2_company4) {
        put("id_2_company4", id_2_company4);
    }

    public void setId_2_years4(String id_2_years4) {
        put("id_2_years4", id_2_years4);
    }

    public void setId_2_jobtitle4(String id_2_jobtitle4) {
        put("id_2_jobtitle4", id_2_jobtitle4);
    }

    public void setId_2_hazard4(String id_2_hazard4) {
        put("id_2_hazard4", id_2_hazard4);
    }

    public void setId_2_consent(String id_2_consent) {
        put("id_2_consent", id_2_consent);
    }

    public void setId_2_datesign(String id_2_datesign) {
        put("id_2_datesign", id_2_datesign);
    }
    public ParseFile getEmployee2() {
        return getParseFile("employee2");
    }
    public void setEmployee2(ParseFile file) {
        if(file!=null) {
            put("employee2", file);
        }
    }
    public String getTitle()
    {
        return "Occupational Health Record";
    }


    private InputStream bemployee2;
    public Document getinPDF(Document doc) throws DocumentException {
        doc=Constants.AddHeader(doc,getTitle(),2);
        doc.add(new Phrase(Constants.GetStringLine("Name and Surname",getId_2_name_surname())));
        doc.add(new Phrase(Constants.GetStringLine("I.D./ Passport No",getId_2_id_number())));
        doc.add(new Phrase(Constants.GetStringLine("Gender (Male/ Female)",getId_2_gender())));
        doc.add(new Phrase(Constants.GetStringLine("Marital Status",getId_2_marital_status())));
        doc.add(new Phrase(Constants.GetStringLine("Home Address",getId_2_home_address())));
        doc.add(new Phrase(Constants.GetStringLine("Town",getId_2_town())));
        doc.add(new Phrase(Constants.GetStringLine("Province",getId_2_province())));
        doc.add(new Phrase(Constants.GetStringLine("Contact No.",getId_2_contact())));
        doc.add(new Phrase(Constants.GetStringLine("Medical (Yes/ No)",getId_2_medical())));
        doc.add(new Phrase(Constants.GetStringLine("Organisation/ Company",getId_2_company())));
        doc.add(new Phrase(Constants.GetStringLine("Job Title",getId_2_job_title())));
        doc.add(new Phrase(Constants.GetStringLine("Employee/ Clock No.",getId_2_employee_no())));
        doc.add(new Phrase(Constants.GetStringLine("Contract or permanent",getId_2_contractbase())));
        doc.add(new Phrase(Constants.GetStringLine("Shift Worker (Yes/No)",getId_2_shift_worker())));
        doc.add(new Phrase(Constants.GetStringLine("Starting/ Engagement date",getId_2_starting_date())));
        doc.add(new Phrase(Constants.GetStringLine("Department",getId_2_department())));
        doc.add(new Phrase(Constants.GetStringLine("Supervisor",getId_2_supervisor())));
        doc.add(new Phrase(Constants.GetStringLine("Supervisor Contact No.",getId_2_contact_office())));
        doc.add(new Phrase("Previous work history"));
        doc.add(new Phrase("Work 1"));
        doc.add(new Phrase(Constants.GetStringLine("Company Name",getId_2_company1())));
        doc.add(new Phrase(Constants.GetStringLine("Years Worked",getId_2_years1())));
        doc.add(new Phrase(Constants.GetStringLine("Job Title/ Occupation",getId_2_jobtitle1())));
        doc.add(new Phrase(Constants.GetStringLine("Hazard Exposure (Noise/ Chemical etc.)",getId_2_hazard1())));
        doc.add(new Phrase("Work 2"));
        doc.add(new Phrase(Constants.GetStringLine("Company Name",getId_2_company2())));
        doc.add(new Phrase(Constants.GetStringLine("Years Worked",getId_2_years2())));
        doc.add(new Phrase(Constants.GetStringLine("Job Title/ Occupation",getId_2_jobtitle2())));
        doc.add(new Phrase(Constants.GetStringLine("Hazard Exposure (Noise/ Chemical etc.)",getId_2_hazard2())));
        doc.add(new Phrase("Work 3"));
        doc.add(new Phrase(Constants.GetStringLine("Company Name",getId_2_company3())));
        doc.add(new Phrase(Constants.GetStringLine("Years Worked",getId_2_years3())));
        doc.add(new Phrase(Constants.GetStringLine("Job Title/ Occupation",getId_2_jobtitle3())));
        doc.add(new Phrase(Constants.GetStringLine("Hazard Exposure (Noise/ Chemical etc.)",getId_2_hazard3())));
        doc.add(new Phrase("Work 4"));
        doc.add(new Phrase(Constants.GetStringLine("Company Name",getId_2_company4())));
        doc.add(new Phrase(Constants.GetStringLine("Years Worked",getId_2_years4())));
        doc.add(new Phrase(Constants.GetStringLine("Job Title/ Occupation",getId_2_jobtitle4())));
        doc.add(new Phrase(Constants.GetStringLine("Hazard Exposure (Noise/ Chemical etc.)",getId_2_hazard4())));
        doc.add(new Phrase("Consent"));
        doc.add(new Phrase("I, "+getId_2_consent()+" hereby consent to examinations of medical surveillance and other required tests, for monitoring occupational exposure and or assessment for fitness of work. I declare that the answer to the question asked in this document are to the best of knowledge true. and correct and that any false information may invalidate the terms of my employment. I consent that, if required, a doctor acting for the organisation/ Company may approach my medical attendent for further information. My Signature below gives my consent to this on the understanding that any information so obtained will be treated as confidential. This is a confidential document."));
        doc.add(new Phrase("I agree to medical surveillance results and fitness for work to be divulged to management and other appropriate authorities where applicable and neccessary."));
        doc=Constants.AddImage(doc,getBemployee2(),"Employee Signature");
        doc.add(new Phrase(Constants.GetStringLine("Date Signed (DD/MM/YYYY)",getId_2_datesign())));
        return doc;
    }
    public InputStream getBemployee2() {
        return bemployee2;
    }
    public void setBemployee2(InputStream bemployee2) {
        if(bemployee2!=null) {
            this.bemployee2 = bemployee2;
        }
    }
}
