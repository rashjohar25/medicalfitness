package com.medical.medicalfitness.objects;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.medical.medicalfitness.params.Constants;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.InputStream;

/**
 * Created by Johars on 2/18/2016.
 */
@ParseClassName("SpecialMedicalInvestigation")
public class SpecialMedicalInvestigation extends ParseObject {
    public SpecialMedicalInvestigation() {
        super();
    }
    public SpecialMedicalInvestigation(ParseUser user) {
        super();
        put("user", user);
    }
    public ParseUser getUser() {
        return getParseUser("user");
    }
    public void setUser(ParseUser user) {
        put("user", user);
    }
    public void setId_8_other(String id_8_other) {
        put("id_8_other",id_8_other);
    }

    public void setId_8_vision_comments(String id_8_vision_comments) {
        put("id_8_vision_comments",id_8_vision_comments);
    }

    public void setId_8_audio_plh(String id_8_audio_plh) {
        put("id_8_audio_plh",id_8_audio_plh);
    }

    public void setId_8_audio_shift(String id_8_audio_shift) {
        put("id_8_audio_shift",id_8_audio_shift);
    }

    public void setId_8_lung(String id_8_lung) {
        put("id_8_lung",id_8_lung);
    }

    public void setId_8_fvc(String id_8_fvc) {
        put("id_8_fvc",id_8_fvc);
    }

    public void setId_8_fev(String id_8_fev) {
        put("id_8_fev",id_8_fev);
    }

    public void setId_8_fevoverfvc(String id_8_fevoverfvc) {
        put("id_8_fevoverfvc",id_8_fevoverfvc);
    }

    public void setId_8_peakflow(String id_8_peakflow) {
        put("id_8_peakflow",id_8_peakflow);
    }

    public void setId_8_certified(String id_8_certified) {
        put("id_8_certified",id_8_certified);
    }

    public void setId_8_datesign(String id_8_datesign) {
        put("id_8_datesign",id_8_datesign);
    }

    public void setId_8_vision(String id_8_vision) {
        put("id_8_vision",id_8_vision);
    }

    public void setId_8_farvision(String id_8_farvision) {
        put("id_8_farvision",id_8_farvision);
    }

    public void setId_8_nearvision(String id_8_nearvision) {
        put("id_8_nearvision",id_8_nearvision);
    }

    public void setId_8_nightvision(String id_8_nightvision) {
        put("id_8_nightvision",id_8_nightvision);
    }

    public void setId_8_othervision(String id_8_othervision) {
        put("id_8_othervision",id_8_othervision);
    }

    public void setId_8_chestray(String id_8_chestray) {
        put("id_8_chestray",id_8_chestray);
    }

    public void setId_8_rest_ecg(String id_8_rest_ecg) {
        put("id_8_rest_ecg",id_8_rest_ecg);
    }

    public void setId_8_cholestrol(String id_8_cholestrol) {
        put("id_8_cholestrol",id_8_cholestrol);
    }
    public void setId_8_mdt(String id_8_mdt) {
        put("id_8_mdt",id_8_mdt);
    }
    public String getId_8_vision_comments() {
        return getString("id_8_vision_comments");
    }
    public String getId_8_audio_plh() {
        return getString("id_8_audio_plh");
    }
    public String getId_8_audio_shift() {
        return getString("id_8_audio_shift");
    }
    public String getId_8_lung() {
        return getString("id_8_lung");
    }
    public String getId_8_fvc() {
        return getString("id_8_fvc");
    }
    public String getId_8_fev() {
        return getString("id_8_fev");
    }
    public String getId_8_fevoverfvc() {
        return getString("id_8_fevoverfvc");
    }
    public String getId_8_peakflow() {
        return getString("id_8_peakflow");
    }
    public String getId_8_certified() {
        return getString("id_8_certified");
    }
    public String getId_8_datesign() {
        return getString("id_8_datesign");
    }
    public String getId_8_vision() {
        return getString("id_8_vision");
    }
    public String getId_8_farvision() {
        return getString("id_8_farvision");
    }
    public String getId_8_nearvision() {
        return getString("id_8_nearvision");
    }
    public String getId_8_nightvision() {
        return getString("id_8_nightvision");
    }
    public String getId_8_othervision() {
        return getString("id_8_othervision");
    }
    public String getId_8_chestray() {
        return getString("id_8_chestray");
    }
    public String getId_8_rest_ecg() {
        return getString("id_8_rest_ecg");
    }
    public String getId_8_cholestrol() {
        return getString("id_8_cholestrol");
    }
    public String getId_8_mdt() {
        return getString("id_8_mdt");
    }
    public String getId_8_other() {
        return getString("id_8_other");
    }
    public ParseFile getEmployee8() {
        return getParseFile("employee8");
    }
    public void setEmployee8(ParseFile employee8) {
        if(employee8!=null) {
            put("employee8", employee8);
        }
    }
    public ParseFile getExaminer8() {
        return getParseFile("examiner8");
    }
    public void setExaminer8(ParseFile examiner8) {
        if(examiner8!=null) {
            put("examiner8", examiner8);
        }
    }
    public String getTitle()
    {
        return "Special Medical Investigation";
    }

    private InputStream bexaminer8,bemployee8;
    public Document getinPDF(Document doc) throws DocumentException {
        doc=Constants.AddHeader(doc,getTitle(),8);
        doc.add(new Phrase(Constants.GetStringLine("Vision Test(Keystone / Snellen)",getId_8_vision())));
        doc.add(new Phrase(Constants.GetStringLine("Far Vision",getId_8_farvision())));
        doc.add(new Phrase(Constants.GetStringLine("Near Vision",getId_8_nearvision())));
        doc.add(new Phrase(Constants.GetStringLine("Night Vision",getId_8_nightvision())));
        doc.add(new Phrase(Constants.GetStringLine("Others",getId_8_othervision())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_8_vision_comments())));
        doc.add(new Phrase("Audiometry Screening"));
        doc.add(new Phrase(Constants.GetStringLine("PLH B/L % (PLH)",getId_8_audio_plh())));
        doc.add(new Phrase(Constants.GetStringLine("% Shift from Previous",getId_8_audio_shift())));
        doc.add(new Phrase(Constants.GetStringLine("Lung Function Test",getId_8_lung())));
        doc.add(new Phrase("Spirometry Result"));
        doc.add(new Phrase(Constants.GetStringLine("Normal FVC %",getId_8_fvc())));
        doc.add(new Phrase(Constants.GetStringLine("Obstruct FEV1 %",getId_8_fev())));
        doc.add(new Phrase(Constants.GetStringLine("Restrict FEV1 / FVC",getId_8_fevoverfvc())));
        doc.add(new Phrase("Special Examinations"));
        doc.add(new Phrase(Constants.GetStringLine("Peak Flow (L/Min)",getId_8_peakflow())));
        doc.add(new Phrase(Constants.GetStringLine("Chest X-ray",getId_8_chestray())));
        doc.add(new Phrase(Constants.GetStringLine("Rest ECG",getId_8_rest_ecg())));
        doc.add(new Phrase("Blood Test"));
        doc.add(new Phrase(Constants.GetStringLine("Cholestrol",getId_8_cholestrol())));
        doc.add(new Phrase(Constants.GetStringLine("MDT",getId_8_mdt())));
        doc.add(new Phrase(Constants.GetStringLine("Other",getId_8_other())));
        doc.add(new Phrase("I "+getId_8_certified()+" certify that the above information is complete and true. I understand that inaccurate, false or missing information may invalidate this examination."));
        doc=Constants.AddImage(doc,getBemployee8(),"Employee Signature");
        doc=Constants.AddImage(doc,getBexaminer8(),"Examiner / OHN signature");
        doc.add(new Phrase(Constants.GetStringLine("Date Signed (DD/MM/YYYY)",getId_8_datesign())));
        return doc;
    }

    public InputStream getBexaminer8() {
        return bexaminer8;
    }

    public void setBexaminer8(InputStream bexaminer8) {
        if(bexaminer8!=null) {
            this.bexaminer8 = bexaminer8;
        }
    }

    public InputStream getBemployee8() {
        return bemployee8;
    }

    public void setBemployee8(InputStream bemployee8) {
        if(bemployee8!=null) {
            this.bemployee8 = bemployee8;
        }
    }
}
