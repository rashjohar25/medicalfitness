package com.medical.medicalfitness.objects;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.medical.medicalfitness.params.Constants;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by Johars on 2/18/2016.
 */
@ParseClassName("AudioQuestionnaire")
public class AudioQuestionnaire extends ParseObject {
    public AudioQuestionnaire() {
        super();
    }
    public AudioQuestionnaire(ParseUser user) {
        super();
        put("user", user);
    }
    public ParseUser getUser() {
        return getParseUser("user");
    }
    public void setUser(ParseUser user) { put("user", user); }
    public String getId_7_audio_cold1() {
        return getString("id_7_audio_cold1");
    }

    public String getId_7_audio_noise1() {
        return getString("id_7_audio_noise1");
    }

    public String getId_7_audio_hobbies1() {
        return getString("id_7_audio_hobbies1");
    }

    public String getId_7_audio_hearing1() {
        return getString("id_7_audio_hearing1");
    }

    public String getId_7_audio_trauma1() {
        return getString("id_7_audio_trauma1");
    }

    public String getId_7_audio_tb1() {
        return getString("id_7_audio_tb1");
    }

    public String getId_7_audio_ears1() {
        return getString("id_7_audio_ears1");
    }

    public String getId_7_audio_military1() {
        return getString("id_7_audio_military1");
    }

    public String getId_7_audio_ppe1() {
        return getString("id_7_audio_ppe1");
    }

    public String getId_7_audio_meds1() {
        return getString("id_7_audio_meds1");
    }

    public String getId_7_audio_cold2() {
        return getString("id_7_audio_cold2");
    }

    public String getId_7_audio_noise2() {
        return getString("id_7_audio_noise2");
    }

    public String getId_7_audio_hobbies2() {
        return getString("id_7_audio_hobbies2");
    }

    public String getId_7_audio_hearing2() {
        return getString("id_7_audio_hearing2");
    }

    public String getId_7_audio_trauma2() {
        return getString("id_7_audio_trauma2");
    }

    public String getId_7_audio_tb2() {
        return getString("id_7_audio_tb2");
    }

    public String getId_7_audio_ears2() {
        return getString("id_7_audio_ears2");
    }

    public String getId_7_audio_military2() {
        return getString("id_7_audio_military2");
    }

    public String getId_7_audio_ppe2() {
        return getString("id_7_audio_ppe2");
    }

    public String getId_7_audio_meds2() {
        return getString("id_7_audio_meds2");
    }

    public String getId_7_audio_cold3() {
        return getString("id_7_audio_cold3");
    }

    public String getId_7_audio_noise3() {
        return getString("id_7_audio_noise3");
    }

    public String getId_7_audio_hobbies3() {
        return getString("id_7_audio_hobbies3");
    }

    public String getId_7_audio_hearing3() {
        return getString("id_7_audio_hearing3");
    }

    public String getId_7_audio_trauma3() {
        return getString("id_7_audio_trauma3");
    }

    public String getId_7_audio_tb3() {
        return getString("id_7_audio_tb3");
    }

    public String getId_7_audio_ears3() {
        return getString("id_7_audio_ears3");
    }

    public String getId_7_audio_military3() {
        return getString("id_7_audio_military3");
    }

    public String getId_7_audio_ppe3() {
        return getString("id_7_audio_ppe3");
    }

    public String getId_7_audio_meds3() {
        return getString("id_7_audio_meds3");
    }

    public String getId_7_audio_date1() {
        return getString("id_7_audio_date1");
    }

    public String getId_7_audio_cold_comments1() {
        return getString("id_7_audio_cold_comments1");
    }

    public String getId_7_audio_noise_comments1() {
        return getString("id_7_audio_noise_comments1");
    }

    public String getId_7_audio_hobbies_comments1() {
        return getString("id_7_audio_hobbies_comments1");
    }

    public String getId_7_audio_hearing_comments1() {
        return getString("id_7_audio_hearing_comments1");
    }

    public String getId_7_audio_trauma_comments1() {
        return getString("id_7_audio_trauma_comments1");
    }

    public String getId_7_audio_tb_comments1() {
        return getString("id_7_audio_tb_comments1");
    }

    public String getId_7_audio_ears_comments1() {
        return getString("id_7_audio_ears_comments1");
    }

    public String getId_7_audio_military_comments1() {
        return getString("id_7_audio_military_comments1");
    }

    public String getId_7_audio_ppe_comments1() {
        return getString("id_7_audio_ppe_comments1");
    }

    public String getId_7_audio_meds_comments1() {
        return getString("id_7_audio_meds_comments1");
    }

    public String getId_7_audio_date2() {
        return getString("id_7_audio_date2");
    }

    public String getId_7_audio_cold_comments2() {
        return getString("id_7_audio_cold_comments2");
    }

    public String getId_7_audio_noise_comments2() {
        return getString("id_7_audio_noise_comments2");
    }

    public String getId_7_audio_hobbies_comments2() {
        return getString("id_7_audio_hobbies_comments2");
    }

    public String getId_7_audio_hearing_comments2() {
        return getString("id_7_audio_hearing_comments2");
    }

    public String getId_7_audio_trauma_comments2() {
        return getString("id_7_audio_trauma_comments2");
    }

    public String getId_7_audio_tb_comments2() {
        return getString("id_7_audio_tb_comments2");
    }

    public String getId_7_audio_ears_comments2() {
        return getString("id_7_audio_ears_comments2");
    }

    public String getId_7_audio_military_comments2() {
        return getString("id_7_audio_military_comments2");
    }

    public String getId_7_audio_ppe_comments2() {
        return getString("id_7_audio_ppe_comments2");
    }

    public String getId_7_audio_meds_comments2() {
        return getString("id_7_audio_meds_comments2");
    }

    public String getId_7_audio_date3() {
        return getString("id_7_audio_date3");
    }

    public String getId_7_audio_cold_comments3() {
        return getString("id_7_audio_cold_comments3");
    }

    public String getId_7_audio_noise_comments3() {
        return getString("id_7_audio_noise_comments3");
    }

    public String getId_7_audio_hobbies_comments3() {
        return getString("id_7_audio_hobbies_comments3");
    }

    public String getId_7_audio_hearing_comments3() {
        return getString("id_7_audio_hearing_comments3");
    }

    public String getId_7_audio_trauma_comments3() {
        return getString("id_7_audio_trauma_comments3");
    }

    public String getId_7_audio_tb_comments3() {
        return getString("id_7_audio_tb_comments3");
    }

    public String getId_7_audio_ears_comments3() {
        return getString("id_7_audio_ears_comments3");
    }

    public String getId_7_audio_military_comments3() {
        return getString("id_7_audio_military_comments3");
    }

    public String getId_7_audio_ppe_comments3() {
        return getString("id_7_audio_ppe_comments3");
    }

    public String getId_7_audio_meds_comments3() {
        return getString("id_7_audio_meds_comments3");
    }


    public void setId_7_audio_cold1(String id_7_audio_cold1) {
        put("id_7_audio_cold1",id_7_audio_cold1);
    }

    public void setId_7_audio_noise1(String id_7_audio_noise1) {
        put("id_7_audio_noise1",id_7_audio_noise1);
    }

    public void setId_7_audio_hobbies1(String id_7_audio_hobbies1) {
        put("id_7_audio_hobbies1",id_7_audio_hobbies1);
    }

    public void setId_7_audio_hearing1(String id_7_audio_hearing1) {
        put("id_7_audio_hearing1",id_7_audio_hearing1);
    }

    public void setId_7_audio_trauma1(String id_7_audio_trauma1) {
        put("id_7_audio_trauma1",id_7_audio_trauma1);
    }

    public void setId_7_audio_tb1(String id_7_audio_tb1) {
        put("id_7_audio_tb1",id_7_audio_tb1);
    }

    public void setId_7_audio_ears1(String id_7_audio_ears1) {
        put("id_7_audio_ears1",id_7_audio_ears1);
    }

    public void setId_7_audio_military1(String id_7_audio_military1) {
        put("id_7_audio_military1",id_7_audio_military1);
    }

    public void setId_7_audio_ppe1(String id_7_audio_ppe1) {
        put("id_7_audio_ppe1",id_7_audio_ppe1);
    }

    public void setId_7_audio_meds1(String id_7_audio_meds1) {
        put("id_7_audio_meds1",id_7_audio_meds1);
    }

    public void setId_7_audio_cold2(String id_7_audio_cold2) {
        put("id_7_audio_cold2",id_7_audio_cold2);
    }

    public void setId_7_audio_noise2(String id_7_audio_noise2) {
        put("id_7_audio_noise2",id_7_audio_noise2);
    }

    public void setId_7_audio_hobbies2(String id_7_audio_hobbies2) {
        put("id_7_audio_hobbies2",id_7_audio_hobbies2);
    }

    public void setId_7_audio_hearing2(String id_7_audio_hearing2) {
        put("id_7_audio_hearing2",id_7_audio_hearing2);
    }

    public void setId_7_audio_trauma2(String id_7_audio_trauma2) {
        put("id_7_audio_trauma2",id_7_audio_trauma2);
    }

    public void setId_7_audio_tb2(String id_7_audio_tb2) {
        put("id_7_audio_tb2",id_7_audio_tb2);
    }

    public void setId_7_audio_ears2(String id_7_audio_ears2) {
        put("id_7_audio_ears2",id_7_audio_ears2);
    }

    public void setId_7_audio_military2(String id_7_audio_military2) {
        put("id_7_audio_military2",id_7_audio_military2);
    }

    public void setId_7_audio_ppe2(String id_7_audio_ppe2) {
        put("id_7_audio_ppe2",id_7_audio_ppe2);
    }

    public void setId_7_audio_meds2(String id_7_audio_meds2) {
        put("id_7_audio_meds2",id_7_audio_meds2);
    }

    public void setId_7_audio_cold3(String id_7_audio_cold3) {
        put("id_7_audio_cold3",id_7_audio_cold3);
    }

    public void setId_7_audio_noise3(String id_7_audio_noise3) {
        put("id_7_audio_noise3",id_7_audio_noise3);
    }

    public void setId_7_audio_hobbies3(String id_7_audio_hobbies3) {
        put("id_7_audio_hobbies3",id_7_audio_hobbies3);
    }

    public void setId_7_audio_hearing3(String id_7_audio_hearing3) {
        put("id_7_audio_hearing3",id_7_audio_hearing3);
    }

    public void setId_7_audio_trauma3(String id_7_audio_trauma3) {
        put("id_7_audio_trauma3",id_7_audio_trauma3);
    }

    public void setId_7_audio_tb3(String id_7_audio_tb3) {
        put("id_7_audio_tb3",id_7_audio_tb3);
    }

    public void setId_7_audio_ears3(String id_7_audio_ears3) {
        put("id_7_audio_ears3",id_7_audio_ears3);
    }

    public void setId_7_audio_military3(String id_7_audio_military3) {
        put("id_7_audio_military3",id_7_audio_military3);
    }

    public void setId_7_audio_ppe3(String id_7_audio_ppe3) {
        put("id_7_audio_ppe3",id_7_audio_ppe3);
    }

    public void setId_7_audio_meds3(String id_7_audio_meds3) {
        put("id_7_audio_meds3",id_7_audio_meds3);
    }

    public void setId_7_audio_date1(String id_7_audio_date1) {
        put("id_7_audio_date1",id_7_audio_date1);
    }

    public void setId_7_audio_cold_comments1(String id_7_audio_cold_comments1) {
        put("id_7_audio_cold_comments1",id_7_audio_cold_comments1);
    }

    public void setId_7_audio_noise_comments1(String id_7_audio_noise_comments1) {
        put("id_7_audio_noise_comments1",id_7_audio_noise_comments1);
    }

    public void setId_7_audio_hobbies_comments1(String id_7_audio_hobbies_comments1) {
        put("id_7_audio_hobbies_comments1",id_7_audio_hobbies_comments1);
    }

    public void setId_7_audio_hearing_comments1(String id_7_audio_hearing_comments1) {
        put("id_7_audio_hearing_comments1",id_7_audio_hearing_comments1);
    }

    public void setId_7_audio_trauma_comments1(String id_7_audio_trauma_comments1) {
        put("id_7_audio_trauma_comments1",id_7_audio_trauma_comments1);
    }

    public void setId_7_audio_tb_comments1(String id_7_audio_tb_comments1) {
        put("id_7_audio_tb_comments1",id_7_audio_tb_comments1);
    }

    public void setId_7_audio_ears_comments1(String id_7_audio_ears_comments1) {
        put("id_7_audio_ears_comments1",id_7_audio_ears_comments1);
    }

    public void setId_7_audio_military_comments1(String id_7_audio_military_comments1) {
        put("id_7_audio_military_comments1",id_7_audio_military_comments1);
    }

    public void setId_7_audio_ppe_comments1(String id_7_audio_ppe_comments1) {
        put("id_7_audio_ppe_comments1",id_7_audio_ppe_comments1);
    }

    public void setId_7_audio_meds_comments1(String id_7_audio_meds_comments1) {
        put("id_7_audio_meds_comments1",id_7_audio_meds_comments1);
    }

    public void setId_7_audio_date2(String id_7_audio_date2) {
        put("id_7_audio_date2",id_7_audio_date2);
    }

    public void setId_7_audio_cold_comments2(String id_7_audio_cold_comments2) {
        put("id_7_audio_cold_comments2",id_7_audio_cold_comments2);
    }

    public void setId_7_audio_noise_comments2(String id_7_audio_noise_comments2) {
        put("id_7_audio_noise_comments2",id_7_audio_noise_comments2);
    }

    public void setId_7_audio_hobbies_comments2(String id_7_audio_hobbies_comments2) {
        put("id_7_audio_hobbies_comments2",id_7_audio_hobbies_comments2);
    }

    public void setId_7_audio_hearing_comments2(String id_7_audio_hearing_comments2) {
        put("id_7_audio_hearing_comments2",id_7_audio_hearing_comments2);
    }

    public void setId_7_audio_trauma_comments2(String id_7_audio_trauma_comments2) {
        put("id_7_audio_trauma_comments2",id_7_audio_trauma_comments2);
    }

    public void setId_7_audio_tb_comments2(String id_7_audio_tb_comments2) {
        put("id_7_audio_tb_comments2",id_7_audio_tb_comments2);
    }

    public void setId_7_audio_ears_comments2(String id_7_audio_ears_comments2) {
        put("id_7_audio_ears_comments2",id_7_audio_ears_comments2);
    }

    public void setId_7_audio_military_comments2(String id_7_audio_military_comments2) {
        put("id_7_audio_military_comments2",id_7_audio_military_comments2);
    }

    public void setId_7_audio_ppe_comments2(String id_7_audio_ppe_comments2) {
        put("id_7_audio_ppe_comments2",id_7_audio_ppe_comments2);
    }

    public void setId_7_audio_meds_comments2(String id_7_audio_meds_comments2) {
        put("id_7_audio_meds_comments2",id_7_audio_meds_comments2);
    }

    public void setId_7_audio_date3(String id_7_audio_date3) {
        put("id_7_audio_date3",id_7_audio_date3);
    }

    public void setId_7_audio_cold_comments3(String id_7_audio_cold_comments3) {
        put("id_7_audio_cold_comments3",id_7_audio_cold_comments3);
    }

    public void setId_7_audio_noise_comments3(String id_7_audio_noise_comments3) {
        put("id_7_audio_noise_comments3",id_7_audio_noise_comments3);
    }

    public void setId_7_audio_hobbies_comments3(String id_7_audio_hobbies_comments3) {
        put("id_7_audio_hobbies_comments3",id_7_audio_hobbies_comments3);
    }

    public void setId_7_audio_hearing_comments3(String id_7_audio_hearing_comments3) {
        put("id_7_audio_hearing_comments3",id_7_audio_hearing_comments3);
    }

    public void setId_7_audio_trauma_comments3(String id_7_audio_trauma_comments3) {
        put("id_7_audio_trauma_comments3",id_7_audio_trauma_comments3);
    }

    public void setId_7_audio_tb_comments3(String id_7_audio_tb_comments3) {
        put("id_7_audio_tb_comments3",id_7_audio_tb_comments3);
    }

    public void setId_7_audio_ears_comments3(String id_7_audio_ears_comments3) {
        put("id_7_audio_ears_comments3",id_7_audio_ears_comments3);
    }

    public void setId_7_audio_military_comments3(String id_7_audio_military_comments3) {
        put("id_7_audio_military_comments3",id_7_audio_military_comments3);
    }

    public void setId_7_audio_ppe_comments3(String id_7_audio_ppe_comments3) {
        put("id_7_audio_ppe_comments3",id_7_audio_ppe_comments3);
    }

    public void setId_7_audio_meds_comments3(String id_7_audio_meds_comments3) {
        put("id_7_audio_meds_comments3",id_7_audio_meds_comments3);
    }
    public String getTitle() {
        return "Audio Questionnaire";
    }
    public Document getinPDF(Document doc) throws DocumentException {
        doc=Constants.AddHeader(doc,getTitle(),6);
        doc.add(new Phrase("Test 1"));
        doc.add(new Phrase(Constants.GetStringLine("Date (DD/MM/YYYY)",getId_7_audio_date1())));
        doc.add(new Phrase(Constants.GetStringLine("Cold, Flu, Sinus, Hayfever etc...",getId_7_audio_cold1())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_cold_comments1())));
        doc.add(new Phrase(Constants.GetStringLine("Noise Exposure at work or previously...",getId_7_audio_noise1())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_noise_comments1())));
        doc.add(new Phrase(Constants.GetStringLine("Any hobbies exposing to you to noise?",getId_7_audio_hobbies1())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_hobbies_comments1())));
        doc.add(new Phrase(Constants.GetStringLine("Hearing problems or in  the family - hereditary...",getId_7_audio_hearing1())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_hearing_comments1())));
        doc.add(new Phrase(Constants.GetStringLine("Any head injury, trauma to ear, infection, surgery etc,",getId_7_audio_trauma1())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_trauma_comments1())));
        doc.add(new Phrase(Constants.GetStringLine("Ever had TB, meningitis or malaria before?",getId_7_audio_tb1())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_tb_comments1())));
        doc.add(new Phrase(Constants.GetStringLine("Ringing in the ears sometimes - tinnitus...",getId_7_audio_ears1())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_ears_comments1())));
        doc.add(new Phrase(Constants.GetStringLine("Military Service - exposure to gunfire / explosion",getId_7_audio_military1())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_military_comments1())));
        doc.add(new Phrase(Constants.GetStringLine("Do you PPE's at work e.g. earplug, muffs etc.",getId_7_audio_ppe1())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_ppe_comments1())));
        doc.add(new Phrase(Constants.GetStringLine("Have you used any meds that can cause deafness?",getId_7_audio_meds1())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_meds_comments1())));
        doc.add(new Phrase("Test 2"));
        doc.add(new Phrase(Constants.GetStringLine("Date (DD/MM/YYYY)",getId_7_audio_date2())));
        doc.add(new Phrase(Constants.GetStringLine("Cold, Flu, Sinus, Hayfever etc...",getId_7_audio_cold2())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_cold_comments2())));
        doc.add(new Phrase(Constants.GetStringLine("Noise Exposure at work or previously...",getId_7_audio_noise2())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_noise_comments2())));
        doc.add(new Phrase(Constants.GetStringLine("Any hobbies exposing to you to noise?",getId_7_audio_hobbies2())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_hobbies_comments2())));
        doc.add(new Phrase(Constants.GetStringLine("Hearing problems or in  the family - hereditary...",getId_7_audio_hearing2())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_hearing_comments2())));
        doc.add(new Phrase(Constants.GetStringLine("Any head injury, trauma to ear, infection, surgery etc,",getId_7_audio_trauma2())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_trauma_comments2())));
        doc.add(new Phrase(Constants.GetStringLine("Ever had TB, meningitis or malaria before?",getId_7_audio_tb2())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_tb_comments2())));
        doc.add(new Phrase(Constants.GetStringLine("Ringing in the ears sometimes - tinnitus...",getId_7_audio_ears2())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_ears_comments2())));
        doc.add(new Phrase(Constants.GetStringLine("Military Service - exposure to gunfire / explosion",getId_7_audio_military2())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_military_comments2())));
        doc.add(new Phrase(Constants.GetStringLine("Do you PPE's at work e.g. earplug, muffs etc.",getId_7_audio_ppe2())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_ppe_comments2())));
        doc.add(new Phrase(Constants.GetStringLine("Have you used any meds that can cause deafness?",getId_7_audio_meds2())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_meds_comments2())));
        doc.add(new Phrase("Test 3"));
        doc.add(new Phrase(Constants.GetStringLine("Date (DD/MM/YYYY)",getId_7_audio_date3())));
        doc.add(new Phrase(Constants.GetStringLine("Cold, Flu, Sinus, Hayfever etc...",getId_7_audio_cold3())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_cold_comments3())));
        doc.add(new Phrase(Constants.GetStringLine("Noise Exposure at work or previously...",getId_7_audio_noise3())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_noise_comments3())));
        doc.add(new Phrase(Constants.GetStringLine("Any hobbies exposing to you to noise?",getId_7_audio_hobbies3())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_hobbies_comments3())));
        doc.add(new Phrase(Constants.GetStringLine("Hearing problems or in  the family - hereditary...",getId_7_audio_hearing3())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_hearing_comments3())));
        doc.add(new Phrase(Constants.GetStringLine("Any head injury, trauma to ear, infection, surgery etc,",getId_7_audio_trauma3())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_trauma_comments3())));
        doc.add(new Phrase(Constants.GetStringLine("Ever had TB, meningitis or malaria before?",getId_7_audio_tb3())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_tb_comments3())));
        doc.add(new Phrase(Constants.GetStringLine("Ringing in the ears sometimes - tinnitus...",getId_7_audio_ears3())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_ears_comments3())));
        doc.add(new Phrase(Constants.GetStringLine("Military Service - exposure to gunfire / explosion",getId_7_audio_military3())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_military_comments3())));
        doc.add(new Phrase(Constants.GetStringLine("Do you PPE's at work e.g. earplug, muffs etc.",getId_7_audio_ppe3())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_ppe_comments3())));
        doc.add(new Phrase(Constants.GetStringLine("Have you used any meds that can cause deafness?",getId_7_audio_meds3())));
        doc.add(new Phrase(Constants.GetStringLine("Comments",getId_7_audio_meds_comments3())));
        return doc;
    }
}
