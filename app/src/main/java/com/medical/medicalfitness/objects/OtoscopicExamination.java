package com.medical.medicalfitness.objects;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.medical.medicalfitness.params.Constants;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.InputStream;

/**
 * Created by Johars on 2/18/2016.
 */
@ParseClassName("OtoscopicExamination")
public class OtoscopicExamination extends ParseObject{
    public OtoscopicExamination() {
        super();
    }
    public OtoscopicExamination(ParseUser user) {
        super();
        put("user", user);
    }
    public ParseUser getUser() {
        return getParseUser("user");
    }
    public void setUser(ParseUser user) {
        put("user", user);
    }

    public void setId_7_oto_date1(String id_7_oto_date1) {
        put("id_7_oto_date1",id_7_oto_date1);
    }

    public void setId_7_oto_date2(String id_7_oto_date2) {
        put("id_7_oto_date2",id_7_oto_date2);
    }

    public void setId_7_oto_date3(String id_7_oto_date3) {
        put("id_7_oto_date3",id_7_oto_date3);
    }

    public void setId_7_oto_date4(String id_7_oto_date4) {
        put("id_7_oto_date4",id_7_oto_date4);
    }

    public void setId_7_oto_perforation1(String id_7_oto_perforation1) {
        put("id_7_oto_perforation1",id_7_oto_perforation1);
    }

    public void setId_7_oto_malfunction1(String id_7_oto_malfunction1) {
        put("id_7_oto_malfunction1",id_7_oto_malfunction1);
    }

    public void setId_7_oto_otitis1(String id_7_oto_otitis1) {
        put("id_7_oto_otitis1",id_7_oto_otitis1);
    }

    public void setId_7_oto_wax1(String id_7_oto_wax1) {
        put("id_7_oto_wax1",id_7_oto_wax1);
    }

    public void setId_7_oto_treatment1(String id_7_oto_treatment1) {
        put("id_7_oto_treatment1",id_7_oto_treatment1);
    }

    public void setId_7_oto_perforation2(String id_7_oto_perforation2) {
        put("id_7_oto_perforation2",id_7_oto_perforation2);
    }

    public void setId_7_oto_malfunction2(String id_7_oto_malfunction2) {
        put("id_7_oto_malfunction2",id_7_oto_malfunction2);
    }

    public void setId_7_oto_otitis2(String id_7_oto_otitis2) {
        put("id_7_oto_otitis2",id_7_oto_otitis2);
    }

    public void setId_7_oto_wax2(String id_7_oto_wax2) {
        put("id_7_oto_wax2",id_7_oto_wax2);
    }

    public void setId_7_oto_treatment2(String id_7_oto_treatment2) {
        put("id_7_oto_treatment2",id_7_oto_treatment2);
    }

    public void setId_7_oto_perforation3(String id_7_oto_perforation3) {
        put("id_7_oto_perforation3",id_7_oto_perforation3);
    }

    public void setId_7_oto_malfunction3(String id_7_oto_malfunction3) {
        put("id_7_oto_malfunction3",id_7_oto_malfunction3);
    }

    public void setId_7_oto_otitis3(String id_7_oto_otitis3) {
        put("id_7_oto_otitis3",id_7_oto_otitis3);
    }

    public void setId_7_oto_wax3(String id_7_oto_wax3) {
        put("id_7_oto_wax3",id_7_oto_wax3);
    }

    public void setId_7_oto_treatment3(String id_7_oto_treatment3) {
        put("id_7_oto_treatment3",id_7_oto_treatment3);
    }

    public void setId_7_oto_perforation4(String id_7_oto_perforation4) {
        put("id_7_oto_perforation4",id_7_oto_perforation4);
    }

    public void setId_7_oto_malfunction4(String id_7_oto_malfunction4) {
        put("id_7_oto_malfunction4",id_7_oto_malfunction4);
    }

    public void setId_7_oto_otitis4(String id_7_oto_otitis4) {
        put("id_7_oto_otitis4",id_7_oto_otitis4);
    }

    public void setId_7_oto_wax4(String id_7_oto_wax4) {
        put("id_7_oto_wax4",id_7_oto_wax4);
    }

    public void setId_7_oto_treatment4(String id_7_oto_treatment4) {
        put("id_7_oto_treatment4",id_7_oto_treatment4);
    }
    public String getId_7_oto_date1() {
        return getString("id_7_oto_date1");
    }
    public String getId_7_oto_date2() {
        return getString("id_7_oto_date2");
    }
    public String getId_7_oto_date3() {
        return getString("id_7_oto_date3");
    }
    public String getId_7_oto_date4() {
        return getString("id_7_oto_date4");
    }
    public String getId_7_oto_perforation1() {
        return getString("id_7_oto_perforation1");
    }
    public String getId_7_oto_malfunction1() {
        return getString("id_7_oto_malfunction1");
    }
    public String getId_7_oto_otitis1() {
        return getString("id_7_oto_otitis1");
    }
    public String getId_7_oto_wax1() {
        return getString("id_7_oto_wax1");
    }
    public String getId_7_oto_treatment1() {
        return getString("id_7_oto_treatment1");
    }
    public String getId_7_oto_perforation2() {
        return getString("id_7_oto_perforation2");
    }
    public String getId_7_oto_malfunction2() {
        return getString("id_7_oto_malfunction2");
    }
    public String getId_7_oto_otitis2() {
        return getString("id_7_oto_otitis2");
    }
    public String getId_7_oto_wax2() {
        return getString("id_7_oto_wax2");
    }
    public String getId_7_oto_treatment2() {
        return getString("id_7_oto_treatment2");
    }
    public String getId_7_oto_perforation3() {
        return getString("id_7_oto_perforation3");
    }
    public String getId_7_oto_malfunction3() {
        return getString("id_7_oto_malfunction3");
    }
    public String getId_7_oto_otitis3() {
        return getString("id_7_oto_otitis3");
    }
    public String getId_7_oto_wax3() {
        return getString("id_7_oto_wax3");
    }
    public String getId_7_oto_treatment3() {
        return getString("id_7_oto_treatment3");
    }
    public String getId_7_oto_perforation4() {
        return getString("id_7_oto_perforation4");
    }
    public String getId_7_oto_malfunction4() {
        return getString("id_7_oto_malfunction4");
    }
    public String getId_7_oto_otitis4() {
        return getString("id_7_oto_otitis4");
    }
    public String getId_7_oto_wax4() {
        return getString("id_7_oto_wax4");
    }
    public String getId_7_oto_treatment4() {
        return getString("id_7_oto_treatment4");
    }

    public ParseFile getExamsign_7_4()
    {
        return getParseFile("examsign_7_4");
    }
    public void setExamsign_7_4(ParseFile examsign_7_4) {
        if(examsign_7_4!=null) {
            put("examsign_7_4", examsign_7_4);
        }
    }
    public ParseFile getExamsign_7_1()
    {
        return getParseFile("examsign_7_1");
    }
    public void setExamsign_7_1(ParseFile examsign_7_1) {

        if(examsign_7_1!=null) {
            put("examsign_7_1", examsign_7_1);
        }
    }
    public ParseFile getExamsign_7_2()
    {
        return getParseFile("examsign_7_2");
    }
    public void setExamsign_7_2(ParseFile examsign_7_2) {
        if(examsign_7_2!=null) {
            put("examsign_7_2", examsign_7_2);
        }
    }
    public ParseFile getExamsign_7_3()
    {
        return getParseFile("examsign_7_3");
    }
    public void setExamsign_7_3(ParseFile examsign_7_3) {

        if(examsign_7_3!=null) {
            put("examsign_7_3", examsign_7_3);
        }
    }
    public String getTitle() {
        return "Otoscopic Examination";
    }


    private InputStream bexamsign_7_1, bexamsign_7_2,bexamsign_7_3,bexamsign_7_4;
    public Document getinPDF(Document doc) throws DocumentException {
        doc = Constants.AddHeader(doc, getTitle(), 7);
        doc.add(new Phrase("Test 1"));
        doc.add(new Phrase(Constants.GetStringLine("Date (DD/MM/YYYY)", getId_7_oto_date1())));
        doc = Constants.AddImage(doc, getBexamsign_7_1(),"Examiner Signature");
        doc.add(new Phrase(Constants.GetStringLine("Perforation of the eardrums", getId_7_oto_perforation1())));
        doc.add(new Phrase(Constants.GetStringLine("Malfunction of inside / outside", getId_7_oto_malfunction1())));
        doc.add(new Phrase(Constants.GetStringLine("Otitis media or externa", getId_7_oto_otitis1())));
        doc.add(new Phrase(Constants.GetStringLine("Wax Present", getId_7_oto_wax1())));
        doc.add(new Phrase(Constants.GetStringLine("On treatment currently / previously", getId_7_oto_treatment1())));
        doc.add(new Phrase("Test 2"));
        doc.add(new Phrase(Constants.GetStringLine("Date (DD/MM/YYYY)", getId_7_oto_date2())));
        doc = Constants.AddImage(doc, getBexamsign_7_2(),"Examiner Signature");
        doc.add(new Phrase(Constants.GetStringLine("Perforation of the eardrums", getId_7_oto_perforation2())));
        doc.add(new Phrase(Constants.GetStringLine("Malfunction of inside / outside", getId_7_oto_malfunction2())));
        doc.add(new Phrase(Constants.GetStringLine("Otitis media or externa", getId_7_oto_otitis2())));
        doc.add(new Phrase(Constants.GetStringLine("Wax Present", getId_7_oto_wax2())));
        doc.add(new Phrase(Constants.GetStringLine("On treatment currently / previously", getId_7_oto_treatment2())));
        doc.add(new Phrase("Test 3"));
        doc.add(new Phrase(Constants.GetStringLine("Date (DD/MM/YYYY)", getId_7_oto_date3())));
        doc = Constants.AddImage(doc, getBexamsign_7_3(),"Examiner Signature");
        doc.add(new Phrase(Constants.GetStringLine("Perforation of the eardrums", getId_7_oto_perforation3())));
        doc.add(new Phrase(Constants.GetStringLine("Malfunction of inside / outside", getId_7_oto_malfunction3())));
        doc.add(new Phrase(Constants.GetStringLine("Otitis media or externa", getId_7_oto_otitis3())));
        doc.add(new Phrase(Constants.GetStringLine("Wax Present", getId_7_oto_wax3())));
        doc.add(new Phrase(Constants.GetStringLine("On treatment currently / previously", getId_7_oto_treatment3())));
        doc.add(new Phrase("Test 4"));
        doc.add(new Phrase(Constants.GetStringLine("Date (DD/MM/YYYY)", getId_7_oto_date4())));
        doc = Constants.AddImage(doc, getBexamsign_7_4(),"Examiner Signature");
        doc.add(new Phrase(Constants.GetStringLine("Perforation of the eardrums", getId_7_oto_perforation4())));
        doc.add(new Phrase(Constants.GetStringLine("Malfunction of inside / outside", getId_7_oto_malfunction4())));
        doc.add(new Phrase(Constants.GetStringLine("Otitis media or externa", getId_7_oto_otitis4())));
        doc.add(new Phrase(Constants.GetStringLine("Wax Present", getId_7_oto_wax4())));
        doc.add(new Phrase(Constants.GetStringLine("On treatment currently / previously", getId_7_oto_treatment4())));
        return doc;
    }


    public InputStream getBexamsign_7_1() {
        return bexamsign_7_1;
    }

    public void setBexamsign_7_1(InputStream bexamsign_7_1) {
        if(bexamsign_7_1!=null) {
            this.bexamsign_7_1 = bexamsign_7_1;
        }
    }

    public InputStream getBexamsign_7_2() {
        return bexamsign_7_2;
    }

    public void setBexamsign_7_2(InputStream bexamsign_7_2) {
        if(bexamsign_7_2!=null) {
            this.bexamsign_7_2 = bexamsign_7_2;
        }
    }

    public InputStream getBexamsign_7_3() {
        return bexamsign_7_3;
    }

    public void setBexamsign_7_3(InputStream bexamsign_7_3) {
        if(bexamsign_7_3!=null) {
            this.bexamsign_7_3 = bexamsign_7_3;
        }
    }

    public InputStream getBexamsign_7_4() {
        return bexamsign_7_4;
    }

    public void setBexamsign_7_4(InputStream bexamsign_7_4) {
        if(bexamsign_7_4!=null) {
            this.bexamsign_7_4 = bexamsign_7_4;
        }
    }
}
