package com.medical.medicalfitness.params;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Header;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.parse.ParseException;
import com.parse.ParseFile;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Constants {
    public static final int SIGNATURE_MEDICAL9 = 901;
    public static final int SIGNATURE_OCCUPATION9= 902;
    public static final int SIGNATURE_EXAMINER8= 801;
    public static final int SIGNATURE_EMPLOYEE8= 802;
    public static final int SIGNATURE_EXAMINER_7_1= 701;
    public static final int SIGNATURE_EXAMINER_7_2= 702;
    public static final int SIGNATURE_EXAMINER_7_3= 703;
    public static final int SIGNATURE_EXAMINER_7_4= 704;
    public static final int SIGNATURE_EMPLOYEE6= 601;
    public static final int SIGNATURE_TECHNICIAN6= 602;
    public static final int SIGNATURE_EMPLOYEE1= 101;
    public static final int SIGNATURE_EXAMINER1= 102;
    public static final int SIGNATURE_OMP1= 103;
    public static final int SIGNATURE_EMPLOYEE2= 201;
    public static final String TAG="Medical Certificate App";
    public static final String STATUS="status";
    public static final String FILE="file";
    public static final String FILEDATA="filedata";
    public static final String DONE="done";
    public static final String TITLE="title";
    public static final String CANCEL="cancel";
    public static final String DIRECTORY1="CHSTEMP";
    public static final String DIRECTORY2="GetSignature";
    public static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
    public static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
    public static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
    public static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

    public static String getTodaysDate() {
        final Calendar c = Calendar.getInstance();
        int todaysDate =     (c.get(Calendar.YEAR) * 10000) +
                ((c.get(Calendar.MONTH) + 1) * 100) +
                (c.get(Calendar.DAY_OF_MONTH));
        return(String.valueOf(todaysDate));
    }

    public static String getCurrentTime() {
        final Calendar c = Calendar.getInstance();
        int currentTime =     (c.get(Calendar.HOUR_OF_DAY) * 10000) +
                (c.get(Calendar.MINUTE) * 100) +
                (c.get(Calendar.SECOND));
        return(String.valueOf(currentTime));
    }
    public static boolean prepareDirectory(Context context,String loc)
    {
        try {
            if (Constants.makedirs(loc)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Could not initiate File System.. Is Sdcard mounted properly?", Toast.LENGTH_LONG).show();
            return false;
        }
    }
    public static String GetUniqueID()
    {
        return Constants.getTodaysDate() + "_" + Constants.getCurrentTime() + "_" + Math.random();
    }
    public static boolean makedirs(String loc)
    {
        File tempdir = new File(loc);
        if (!tempdir.exists())
            tempdir.mkdirs();

        if (tempdir.isDirectory())
        {
            File[] files = tempdir.listFiles();
            for (File file : files)
            {
                /*if (!file.delete())
                {
                    System.out.println("Failed to delete " + file);
                }*/
            }
        }
        return (tempdir.isDirectory());
    }
    public static String getRealPathFromURI(Context context,Uri contentUri)
    {
        Cursor cursor=null;
        try{
            String[] proj={MediaStore.Images.Media.DATA};
            cursor=context.getContentResolver().query(contentUri,proj,null,null,null);
            int index=cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(index);
        }
        finally {
            if(cursor!=null)
            {
                cursor.close();
            }
        }
    }
    public static ProgressDialog getDialog(Context context)
    {
        ProgressDialog builder=new ProgressDialog(context);
        builder.setCancelable(false);
        builder.setMessage("Saving...");
        builder.setIndeterminate(true);
        builder.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        return builder;
    }

    public static String ConvertToString(Bitmap bitmap)
    {
        ByteArrayOutputStream os=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
        byte[] b=os.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);
    }
    public static ParseFile ConvertToParseFile(String str) {
        byte[] decodedString= Base64.decode(str, Base64.DEFAULT);
        ParseFile ff=new ParseFile("file.png",decodedString);
        ff.saveInBackground();
        return ff;
    }
    public static Bitmap ConvertToBitmap(String str)
    {
        byte[] decodedString= ConvertToByteArray(str);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }
    public static byte[] ConvertToByteArray(String str)
    {
        byte[] decodedString= Base64.decode(str, Base64.DEFAULT);
        return decodedString;
    }
    public static void SendEmail(Context context,String subject,String content)
    {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, content);
        AlertDialog dialog= SendReport(context,emailIntent);
        dialog.show();
    }
    public static void SendEmail(Context context,String subject,File file)
    {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        /*ArrayList<Uri> uris = new ArrayList<Uri>();
        uris.add(Uri.fromFile(new File("/path/to/first/file")));
        uris.add(Uri.fromFile(new File("/path/to/second/file")));
        emailIntent.putParcelableArrayListExtra(Intent.ACTION_SEND_MULTIPLE, uris);*/
        AlertDialog dialog= SendReport(context,emailIntent);
        dialog.show();
    }
    public static Document AddHeader(Document document,String header,int number) throws DocumentException {
        document.newPage();
        Anchor anchor=new Anchor(header, Constants.catFont);
        anchor.setName(header);
        Chapter catPart = new Chapter(new Paragraph(anchor), number);
        document.add(catPart);
        return document;
    }
    public static Document AddImage(Document doc,InputStream file,String title){
        if(file!=null) {
            try {
                Image occfile = null;
                Bitmap bmp = BitmapFactory.decodeStream(file);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                occfile = Image.getInstance(stream.toByteArray());
                occfile.setAlt(title);
                occfile.scaleAbsolute(200.0f, 140.0f);
                doc.add(new Phrase(title + ":"));
                doc.add(occfile);
            } catch (BadElementException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return doc;
    }
    public static Document AddImage(Document doc,byte[] file,String title)
    {
        if(file!=null)
        {
            Image occfile= null;
            try {
                doc.add(new Phrase(title+":"));
                occfile = Image.getInstance(file);
                occfile.setAlt(title);
                occfile.scaleAbsolute(200.0f, 140.0f);
                doc.add(occfile);
            } catch (BadElementException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }
        return doc;
    }
    public static String GetStringLine(String value1,String value2)
    {
        return value1+" : "+value2+"\n";
    }
    public static String GetRadioText(AppCompatActivity activity,RadioGroup group)
    {
        try {
            AppCompatRadioButton rad = (AppCompatRadioButton) activity.findViewById(group.getCheckedRadioButtonId());
            return rad.getText().toString();
        }
        catch (NullPointerException ex)
        {
            return "";
        }
    }
    private static AlertDialog SendReport(final Context context,final Intent intent)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle("Send Mail to...");
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final EditText text=new EditText(context);
        builder.setView(text);
        builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{text.getText().toString()});
                try {
                    context.startActivity(Intent.createChooser(intent, "Send mail..."));
                    Toast.makeText(context, "Sending the Email.", Toast.LENGTH_SHORT).show();
                }
                catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(context, "There is no email client installed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return builder.create();
    }
    private static Boolean IsEmail(String value)
    {
        Pattern pattern=Patterns.EMAIL_ADDRESS;
        Matcher matcher=pattern.matcher(value);
        if(matcher.matches())
        {
            return true;
        }else {
            return false;
        }
    }


}
