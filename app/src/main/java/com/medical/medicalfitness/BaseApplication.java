package com.medical.medicalfitness;

import android.app.Application;

import com.medical.medicalfitness.objects.AudioQuestionnaire;
import com.medical.medicalfitness.objects.MedicalFitnessCertificate;
import com.medical.medicalfitness.objects.MedicalHistory;
import com.medical.medicalfitness.objects.MedicalReferral;
import com.medical.medicalfitness.objects.OccupationalHealthRecord;
import com.medical.medicalfitness.objects.OtoscopicExamination;
import com.medical.medicalfitness.objects.PhysicalExamination;
import com.medical.medicalfitness.objects.RespiratoryQuestionnaire;
import com.medical.medicalfitness.objects.SpecialMedicalInvestigation;
import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseObject;


/**
 * Created by Johars on 2/15/2016.
 */
public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        ParseObject.registerSubclass(MedicalFitnessCertificate.class);
        ParseObject.registerSubclass(OccupationalHealthRecord.class);
        ParseObject.registerSubclass(MedicalHistory.class);
        ParseObject.registerSubclass(PhysicalExamination.class);
        ParseObject.registerSubclass(RespiratoryQuestionnaire.class);
        ParseObject.registerSubclass(AudioQuestionnaire.class);
        ParseObject.registerSubclass(OtoscopicExamination.class);
        ParseObject.registerSubclass(SpecialMedicalInvestigation.class);
        ParseObject.registerSubclass(MedicalReferral.class);
        Parse.initialize(this);
    }
}
