package com.medical.medicalfitness.datatypes;

import android.util.Patterns;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Johars on 11/25/2015.
 */
public class Email implements Serializable {
    private static Pattern emailpatt= Patterns.EMAIL_ADDRESS;
    private String email;
    public Email(String email)
    {
        this.email=email;
    }
    @Override
    public String toString() {
        return email;
    }
    public Boolean IsMatch()
    {
        return emailpatt.matcher(email).matches();
    }
    public Boolean IsFound()
    {
        return emailpatt.matcher(email).find();
    }
}
