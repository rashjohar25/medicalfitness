package com.medical.medicalfitness.datatypes;

import java.io.Serializable;

/**
 * Created by Johars on 11/25/2015.
 */
public class Password implements Serializable {
    private static final String[] invalidchars={" ","\'","\"","{","}","[","]","\\","|",";",":","/","?",">",".",",","<","+","=","-",")","(","*","&","^","%","$","!","~","`"};
    private String password;
    public Password(String password)
    {
        this.password=password;
    }
    public Boolean IsValid()
    {
        if(password.length()<7)
            return false;
        for (String c:invalidchars) {
            if(password.contains(c))
                return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return password;
    }
}
