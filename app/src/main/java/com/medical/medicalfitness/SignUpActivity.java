package com.medical.medicalfitness;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.medical.medicalfitness.datatypes.Email;
import com.medical.medicalfitness.datatypes.Password;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SignUpActivity extends AppCompatActivity {
    private AppCompatAutoCompleteTextView username,email;
    private AppCompatEditText password;
    private AppCompatButton email_sign_up_button;
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        username = (AppCompatAutoCompleteTextView) findViewById(R.id.username);
        email = (AppCompatAutoCompleteTextView) findViewById(R.id.email);
        password = (AppCompatEditText) findViewById(R.id.password);
        email_sign_up_button = (AppCompatButton) findViewById(R.id.email_sign_up_button);
        dialog=CreateInfiniteProgress(this);
        email_sign_up_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AttemptSignUp(v);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    private void AttemptSignUp(View v)throws ParseException {
        dialog.show();
        if(IsInfoValid(v)) {
            Password pwd = new Password(password.getText().toString());
            Email ema = new Email(email.getText().toString());
            final ParseUser user = new ParseUser();
            user.setUsername(username.getText().toString());
            user.setPassword(pwd.toString());
            user.setEmail(ema.toString());
            user.signUpInBackground(new SignUpCallback() {
                @Override
                public void done(ParseException e) {
                    if(e==null) {
                        Toast.makeText(getApplicationContext(),"User Created Successfully.",Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }else {
                        Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        else {
            dialog.dismiss();
        }
    }
    private Boolean IsRequiredFieldEmpty()
    {
        if(email.getText().toString().isEmpty())
            return true;
        if(password.getText().toString().isEmpty())
            return true;
        if(username.getText().toString().isEmpty())
            return true;
        return false;
    }
    private Boolean IsInfoValid(View v) throws ParseException {
        Password pwd=new Password(password.getText().toString());
        Email mail=new Email(email.getText().toString());
        if(IsRequiredFieldEmpty()) {
            Toast.makeText(getApplicationContext(),"All Fields are Required.",Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!pwd.IsValid()) {
            Toast.makeText(this, "Password must be of minimum 7 character long with valid character only.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(!mail.IsFound()) {
            Toast.makeText(this, "Please enter a valid email address.", Toast.LENGTH_LONG).show();
            return false;
        }
        if(username.getText().toString().contains(" ")) {
            Toast.makeText(this, "Username can not contain empty characters.", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    private ProgressDialog CreateInfiniteProgress(Context context)
    {
        ProgressDialog builder=new ProgressDialog(context);
        builder.setMessage("Please Wait...");
        builder.setCancelable(false);
        builder.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        builder.setIndeterminate(true);
        return builder;
    }
}
